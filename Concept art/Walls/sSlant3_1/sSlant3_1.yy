{
    "id": "32db3b83-caab-492d-a72b-3b0d558b6051",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlant3_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cdfced9a-6032-4204-9a85-ab75e8767af1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32db3b83-caab-492d-a72b-3b0d558b6051",
            "compositeImage": {
                "id": "05b138ec-c421-47c2-bc93-244d409879c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdfced9a-6032-4204-9a85-ab75e8767af1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc61a393-3de1-4ea7-8347-6b46a6b79555",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdfced9a-6032-4204-9a85-ab75e8767af1",
                    "LayerId": "3f9e1bcb-83c5-4d47-a01c-3fbbe3846477"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3f9e1bcb-83c5-4d47-a01c-3fbbe3846477",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "32db3b83-caab-492d-a72b-3b0d558b6051",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}