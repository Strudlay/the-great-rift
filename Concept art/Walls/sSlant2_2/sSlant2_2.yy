{
    "id": "aaf3c2c3-707d-4c0d-811d-e10d948e03a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlant2_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "711b879c-8144-4124-a101-7c4337a4452c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aaf3c2c3-707d-4c0d-811d-e10d948e03a8",
            "compositeImage": {
                "id": "b20d7c2d-385a-4491-bf72-747f610de930",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "711b879c-8144-4124-a101-7c4337a4452c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1389f58a-5800-4a4c-a565-db90cf4609d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "711b879c-8144-4124-a101-7c4337a4452c",
                    "LayerId": "5ed985b0-0e68-4996-ab1a-83b2b1adc0c7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5ed985b0-0e68-4996-ab1a-83b2b1adc0c7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aaf3c2c3-707d-4c0d-811d-e10d948e03a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}