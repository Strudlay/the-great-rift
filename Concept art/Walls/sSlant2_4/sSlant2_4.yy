{
    "id": "2921c769-de07-4fb7-90cd-3235994248bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlant2_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e5377e8b-b71e-40e5-82a3-93866b233f85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2921c769-de07-4fb7-90cd-3235994248bc",
            "compositeImage": {
                "id": "99b39ac5-0bb2-412e-a983-b6a926a55b3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5377e8b-b71e-40e5-82a3-93866b233f85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c85658d0-ba0c-4b7b-b3c0-215b7dacc6ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5377e8b-b71e-40e5-82a3-93866b233f85",
                    "LayerId": "9d7cb018-36d4-4e88-8d3d-40e59a000242"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9d7cb018-36d4-4e88-8d3d-40e59a000242",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2921c769-de07-4fb7-90cd-3235994248bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}