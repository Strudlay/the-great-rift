{
    "id": "e685fb2d-630f-4562-9833-a97cc392f0d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlant1_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "956d0025-fb95-490e-9e5a-ff585843cc57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e685fb2d-630f-4562-9833-a97cc392f0d0",
            "compositeImage": {
                "id": "e74f509c-ac30-4e06-a532-45f44c703b35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "956d0025-fb95-490e-9e5a-ff585843cc57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81e628b7-9bfc-48ec-813a-c12ba74c9a8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "956d0025-fb95-490e-9e5a-ff585843cc57",
                    "LayerId": "7b4cdd72-1e27-42f4-bb1b-17b8bedbf6ed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7b4cdd72-1e27-42f4-bb1b-17b8bedbf6ed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e685fb2d-630f-4562-9833-a97cc392f0d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}