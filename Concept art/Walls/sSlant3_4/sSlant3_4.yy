{
    "id": "001beec4-b3ce-42a2-83c9-1e2f4a9215b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlant3_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1bdc864-3c71-4cf5-bad6-f731ac97f3ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "001beec4-b3ce-42a2-83c9-1e2f4a9215b1",
            "compositeImage": {
                "id": "fc6fab81-a6d1-4ba8-a781-96f083204600",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1bdc864-3c71-4cf5-bad6-f731ac97f3ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "329bfdaf-c3d1-47a0-b094-c2b74bd379c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1bdc864-3c71-4cf5-bad6-f731ac97f3ba",
                    "LayerId": "c64ad363-2f65-4cc1-adf2-7f413e16b4e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c64ad363-2f65-4cc1-adf2-7f413e16b4e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "001beec4-b3ce-42a2-83c9-1e2f4a9215b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}