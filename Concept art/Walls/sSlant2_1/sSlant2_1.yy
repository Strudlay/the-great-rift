{
    "id": "d9d98376-7422-4795-a8c8-e1dd582298b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlant2_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bca31311-3400-47aa-a601-c357732d378f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9d98376-7422-4795-a8c8-e1dd582298b6",
            "compositeImage": {
                "id": "60b6a79a-d5f8-443d-90b6-b76c4ef19d2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bca31311-3400-47aa-a601-c357732d378f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d3fe92f-a761-4ab3-8190-447b6cb77db8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bca31311-3400-47aa-a601-c357732d378f",
                    "LayerId": "9b7ad0cf-cf87-454d-8472-f71a5edeefde"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9b7ad0cf-cf87-454d-8472-f71a5edeefde",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d9d98376-7422-4795-a8c8-e1dd582298b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}