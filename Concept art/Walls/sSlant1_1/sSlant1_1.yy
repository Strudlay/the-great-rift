{
    "id": "f4d7d8eb-70b3-4e06-8a08-9c5cad4e15a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlant1_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76733d42-d777-4d17-9764-b932c77c20eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d7d8eb-70b3-4e06-8a08-9c5cad4e15a8",
            "compositeImage": {
                "id": "a590d35d-13de-4d50-ab3f-b305707b2af3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76733d42-d777-4d17-9764-b932c77c20eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1514e19-fe81-4f49-a6a2-9fb2e9303927",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76733d42-d777-4d17-9764-b932c77c20eb",
                    "LayerId": "54c2d1d5-3dba-4cfb-99df-d264da830be4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "54c2d1d5-3dba-4cfb-99df-d264da830be4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4d7d8eb-70b3-4e06-8a08-9c5cad4e15a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}