{
    "id": "0677acc3-ff83-4cc5-8551-34b0cee31821",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlant1_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "048aea14-b1f4-4b6c-a82a-f7fe251dfd4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0677acc3-ff83-4cc5-8551-34b0cee31821",
            "compositeImage": {
                "id": "fb8f3ae0-fca7-42de-82f9-518001fa5f62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "048aea14-b1f4-4b6c-a82a-f7fe251dfd4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d1fcdf2-f85f-4615-b3a3-f85e4a3da139",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "048aea14-b1f4-4b6c-a82a-f7fe251dfd4c",
                    "LayerId": "a3b055b7-0c5a-4975-b50c-e5a00d86d8e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a3b055b7-0c5a-4975-b50c-e5a00d86d8e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0677acc3-ff83-4cc5-8551-34b0cee31821",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}