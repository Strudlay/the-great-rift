{
    "id": "dbfa132a-b6e3-4a84-8c15-f003aa66b77a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlant3_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6cb440e9-448e-417c-a1c3-b2bde4cc1249",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbfa132a-b6e3-4a84-8c15-f003aa66b77a",
            "compositeImage": {
                "id": "81f70348-a491-4519-8ad1-17abd3dce0bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cb440e9-448e-417c-a1c3-b2bde4cc1249",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "239873e7-11ab-4432-9e33-e3d78c01c279",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cb440e9-448e-417c-a1c3-b2bde4cc1249",
                    "LayerId": "9d30e049-77b0-48ff-8d44-86b0b52326bd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9d30e049-77b0-48ff-8d44-86b0b52326bd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dbfa132a-b6e3-4a84-8c15-f003aa66b77a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}