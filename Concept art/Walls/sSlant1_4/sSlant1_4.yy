{
    "id": "12527b7b-bfd9-4692-be21-4a946c71a47c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlant1_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "40c7a522-7b22-4aed-98e3-ea0059f8514a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12527b7b-bfd9-4692-be21-4a946c71a47c",
            "compositeImage": {
                "id": "95e9f768-dd49-4f5f-8c32-507e8408654c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40c7a522-7b22-4aed-98e3-ea0059f8514a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08620194-15ec-4479-8309-df7a820c0b6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40c7a522-7b22-4aed-98e3-ea0059f8514a",
                    "LayerId": "14411c7e-6fea-4059-af9d-edde6e9b61ae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "14411c7e-6fea-4059-af9d-edde6e9b61ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "12527b7b-bfd9-4692-be21-4a946c71a47c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}