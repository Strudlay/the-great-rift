{
    "id": "74f25d2f-2ba3-428f-ac55-a75e7e9834f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlant3_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "47a4fe2d-0559-4f26-ba7e-2a57d8d81fac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74f25d2f-2ba3-428f-ac55-a75e7e9834f5",
            "compositeImage": {
                "id": "865226cb-494f-4e57-b6b7-4a3fd099c90e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47a4fe2d-0559-4f26-ba7e-2a57d8d81fac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0606d908-49a9-4893-bd2e-936ad4310503",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47a4fe2d-0559-4f26-ba7e-2a57d8d81fac",
                    "LayerId": "b82bd6b2-602d-4028-99a1-b59ca0c037e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b82bd6b2-602d-4028-99a1-b59ca0c037e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "74f25d2f-2ba3-428f-ac55-a75e7e9834f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}