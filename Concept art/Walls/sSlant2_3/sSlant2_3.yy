{
    "id": "08c65d59-7b23-46ff-a2f8-64c49cb6389b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlant2_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05a908de-2da1-46e0-ad08-c79dd3ff96f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08c65d59-7b23-46ff-a2f8-64c49cb6389b",
            "compositeImage": {
                "id": "51d86c00-b791-4d18-bd9b-45305fa0c051",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05a908de-2da1-46e0-ad08-c79dd3ff96f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72010d02-2a4d-489a-b182-578f5bf98439",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05a908de-2da1-46e0-ad08-c79dd3ff96f9",
                    "LayerId": "e1a11aed-1aaf-4584-bbba-2506f7226ccd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e1a11aed-1aaf-4584-bbba-2506f7226ccd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "08c65d59-7b23-46ff-a2f8-64c49cb6389b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}