{
    "id": "aec4b71e-d79e-435b-887b-e040feb8adbf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCorner3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a32f01eb-e22b-4806-8eda-b64d6cb32000",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aec4b71e-d79e-435b-887b-e040feb8adbf",
            "compositeImage": {
                "id": "05d6c9a0-edc0-4f4d-af8f-1ebbc3c50c39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a32f01eb-e22b-4806-8eda-b64d6cb32000",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdd9a1f6-4123-4df5-9d1b-6d170956ea7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a32f01eb-e22b-4806-8eda-b64d6cb32000",
                    "LayerId": "539b1c88-536e-453a-b569-54300f76c2d0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "539b1c88-536e-453a-b569-54300f76c2d0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aec4b71e-d79e-435b-887b-e040feb8adbf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}