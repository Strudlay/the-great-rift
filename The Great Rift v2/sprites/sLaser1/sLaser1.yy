{
    "id": "a5f3bb3b-eb92-4e02-817c-00ef1b1b9032",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLaser1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "33e2065d-370f-47be-8ea4-c5ec333f11d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5f3bb3b-eb92-4e02-817c-00ef1b1b9032",
            "compositeImage": {
                "id": "23394839-f33c-4e36-a2f9-6e27f67f8ce4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33e2065d-370f-47be-8ea4-c5ec333f11d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c513bdb-ad27-4d9f-a6d5-4b21d39eed6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33e2065d-370f-47be-8ea4-c5ec333f11d7",
                    "LayerId": "fbf4ea68-8699-481a-b0d5-9349a301c4dd"
                }
            ]
        },
        {
            "id": "fee4d90e-ccba-43e4-a64c-a9510a30f3e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5f3bb3b-eb92-4e02-817c-00ef1b1b9032",
            "compositeImage": {
                "id": "5384cd5c-3a33-41d3-9ac2-78d42e568289",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fee4d90e-ccba-43e4-a64c-a9510a30f3e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47cac1a9-75ec-4aa4-b73b-f143e98823e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fee4d90e-ccba-43e4-a64c-a9510a30f3e8",
                    "LayerId": "fbf4ea68-8699-481a-b0d5-9349a301c4dd"
                }
            ]
        },
        {
            "id": "a1c1cd52-351d-4fff-a1fe-18c48cfde821",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5f3bb3b-eb92-4e02-817c-00ef1b1b9032",
            "compositeImage": {
                "id": "d5626200-192b-4e6d-8404-eee18740a479",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1c1cd52-351d-4fff-a1fe-18c48cfde821",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c365372-7cf2-4bd2-9cd4-d6ce1e4755ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1c1cd52-351d-4fff-a1fe-18c48cfde821",
                    "LayerId": "fbf4ea68-8699-481a-b0d5-9349a301c4dd"
                }
            ]
        },
        {
            "id": "6557fce5-69e8-4dcc-bfc0-f1d7f2931884",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5f3bb3b-eb92-4e02-817c-00ef1b1b9032",
            "compositeImage": {
                "id": "d3347495-9fac-44e3-b870-18c8a38f6cb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6557fce5-69e8-4dcc-bfc0-f1d7f2931884",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc413aa4-a2d9-4a18-af15-ceee41a199e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6557fce5-69e8-4dcc-bfc0-f1d7f2931884",
                    "LayerId": "fbf4ea68-8699-481a-b0d5-9349a301c4dd"
                }
            ]
        },
        {
            "id": "efc8cd73-592c-4469-aa3c-f30d7fea2c49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5f3bb3b-eb92-4e02-817c-00ef1b1b9032",
            "compositeImage": {
                "id": "17c5d266-ae74-421e-b12c-50ddd9160c17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efc8cd73-592c-4469-aa3c-f30d7fea2c49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3c8e3f3-5fec-4013-b9de-fdf8b5001c27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efc8cd73-592c-4469-aa3c-f30d7fea2c49",
                    "LayerId": "fbf4ea68-8699-481a-b0d5-9349a301c4dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "fbf4ea68-8699-481a-b0d5-9349a301c4dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a5f3bb3b-eb92-4e02-817c-00ef1b1b9032",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 63
}