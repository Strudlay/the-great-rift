{
    "id": "1e9e160d-5b73-42c7-9f7e-794bf730e77f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRiftShip",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "874f6609-9111-4b57-8539-ddc17ee30da1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e9e160d-5b73-42c7-9f7e-794bf730e77f",
            "compositeImage": {
                "id": "cfa374af-8bed-460e-9645-704e7f041268",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "874f6609-9111-4b57-8539-ddc17ee30da1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f359fe8d-026b-4c79-adef-2744afaabd27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "874f6609-9111-4b57-8539-ddc17ee30da1",
                    "LayerId": "26035b1f-aa41-4033-a490-6a9674cdf480"
                }
            ]
        },
        {
            "id": "8f02c46a-ff31-4aa8-8ab2-ff8b31b9b731",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e9e160d-5b73-42c7-9f7e-794bf730e77f",
            "compositeImage": {
                "id": "235b8551-fdc6-445d-9bbb-5c4ef4e20bf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f02c46a-ff31-4aa8-8ab2-ff8b31b9b731",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b841dbe6-3924-4d05-9945-c21c20dae819",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f02c46a-ff31-4aa8-8ab2-ff8b31b9b731",
                    "LayerId": "26035b1f-aa41-4033-a490-6a9674cdf480"
                }
            ]
        },
        {
            "id": "29c73975-fdd4-4435-9e48-a3b267a9d43a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e9e160d-5b73-42c7-9f7e-794bf730e77f",
            "compositeImage": {
                "id": "a2726acf-9bc8-4669-88c0-5e4211f555a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29c73975-fdd4-4435-9e48-a3b267a9d43a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7b80fbe-937b-4270-ad55-152fe48bb2d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29c73975-fdd4-4435-9e48-a3b267a9d43a",
                    "LayerId": "26035b1f-aa41-4033-a490-6a9674cdf480"
                }
            ]
        },
        {
            "id": "9a19749a-89aa-40f8-9be3-00ca2fbf91c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e9e160d-5b73-42c7-9f7e-794bf730e77f",
            "compositeImage": {
                "id": "e0abda47-6d89-4b0b-bd08-9c531c10cf2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a19749a-89aa-40f8-9be3-00ca2fbf91c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f6cc403-2b56-41a3-b856-edd14223193f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a19749a-89aa-40f8-9be3-00ca2fbf91c2",
                    "LayerId": "26035b1f-aa41-4033-a490-6a9674cdf480"
                }
            ]
        },
        {
            "id": "0be3d63f-9d9c-4b39-8fe9-92bd331b73ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e9e160d-5b73-42c7-9f7e-794bf730e77f",
            "compositeImage": {
                "id": "fe896107-bd71-457a-b505-ab43cc92ed71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0be3d63f-9d9c-4b39-8fe9-92bd331b73ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "902ea183-8d54-4593-a87d-2d0743ef118b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0be3d63f-9d9c-4b39-8fe9-92bd331b73ae",
                    "LayerId": "26035b1f-aa41-4033-a490-6a9674cdf480"
                }
            ]
        },
        {
            "id": "3d8c0d29-2b3b-4191-a203-5051d479d3b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e9e160d-5b73-42c7-9f7e-794bf730e77f",
            "compositeImage": {
                "id": "048cb3ec-d43c-4b7f-9572-2b30b25a2e1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d8c0d29-2b3b-4191-a203-5051d479d3b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b14d9404-894a-4891-8793-1aa7b127dc27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d8c0d29-2b3b-4191-a203-5051d479d3b7",
                    "LayerId": "26035b1f-aa41-4033-a490-6a9674cdf480"
                }
            ]
        },
        {
            "id": "20b68431-448c-4730-aa95-f56b23391d58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e9e160d-5b73-42c7-9f7e-794bf730e77f",
            "compositeImage": {
                "id": "a576943c-1423-46d3-9a0f-fb267bee3865",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20b68431-448c-4730-aa95-f56b23391d58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef116c48-6a2b-4e46-95cd-7a3e636f82d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20b68431-448c-4730-aa95-f56b23391d58",
                    "LayerId": "26035b1f-aa41-4033-a490-6a9674cdf480"
                }
            ]
        },
        {
            "id": "aa0ff87e-5a2a-48ee-85a5-1cab77dfa898",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e9e160d-5b73-42c7-9f7e-794bf730e77f",
            "compositeImage": {
                "id": "74349800-b552-4950-b180-1306c4e86b98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa0ff87e-5a2a-48ee-85a5-1cab77dfa898",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b4e5d32-539f-45d0-8972-3ee4d84e856a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa0ff87e-5a2a-48ee-85a5-1cab77dfa898",
                    "LayerId": "26035b1f-aa41-4033-a490-6a9674cdf480"
                }
            ]
        },
        {
            "id": "26ae702a-4e1e-4682-a6e2-2b77600d55cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e9e160d-5b73-42c7-9f7e-794bf730e77f",
            "compositeImage": {
                "id": "b95918af-e8d2-4156-83c2-f276676e1372",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26ae702a-4e1e-4682-a6e2-2b77600d55cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c66e259e-c849-466b-8218-6844139f7a87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26ae702a-4e1e-4682-a6e2-2b77600d55cc",
                    "LayerId": "26035b1f-aa41-4033-a490-6a9674cdf480"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "26035b1f-aa41-4033-a490-6a9674cdf480",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e9e160d-5b73-42c7-9f7e-794bf730e77f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 45,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}