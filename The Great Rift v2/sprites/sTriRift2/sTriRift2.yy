{
    "id": "e0d4aec3-76cc-4ac9-bc9b-aa51636bcd28",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTriRift2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 27,
    "bbox_right": 63,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "061c7ab2-b630-4fa5-bf12-5a870a7dea7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0d4aec3-76cc-4ac9-bc9b-aa51636bcd28",
            "compositeImage": {
                "id": "4c266d6c-6541-447d-88fa-909c03de5818",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "061c7ab2-b630-4fa5-bf12-5a870a7dea7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b636eb6-5ba5-4dfd-bf64-269a0a07f36d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "061c7ab2-b630-4fa5-bf12-5a870a7dea7b",
                    "LayerId": "a75c6b55-508f-45a4-909c-ea30d91a2c49"
                }
            ]
        },
        {
            "id": "a89aacf6-87e2-4c35-adc7-9ca36ee9e78c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0d4aec3-76cc-4ac9-bc9b-aa51636bcd28",
            "compositeImage": {
                "id": "a440be39-5b9b-4d53-9a08-848a8968c7d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a89aacf6-87e2-4c35-adc7-9ca36ee9e78c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "709a57ba-d20f-4390-95ba-c19e9d6ff365",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a89aacf6-87e2-4c35-adc7-9ca36ee9e78c",
                    "LayerId": "a75c6b55-508f-45a4-909c-ea30d91a2c49"
                }
            ]
        },
        {
            "id": "618a1e72-6bbe-4e6c-85ff-f70fa93f4815",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0d4aec3-76cc-4ac9-bc9b-aa51636bcd28",
            "compositeImage": {
                "id": "377d2acb-410e-400e-ad91-86b0d5430d80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "618a1e72-6bbe-4e6c-85ff-f70fa93f4815",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fefb2c4c-98db-4063-887b-a5e1a4a77cb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "618a1e72-6bbe-4e6c-85ff-f70fa93f4815",
                    "LayerId": "a75c6b55-508f-45a4-909c-ea30d91a2c49"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a75c6b55-508f-45a4-909c-ea30d91a2c49",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0d4aec3-76cc-4ac9-bc9b-aa51636bcd28",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 48,
    "yorig": 48
}