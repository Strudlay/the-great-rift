{
    "id": "87fe82ce-ae61-4bcb-be58-ba759f72aadb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTriRift3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 27,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "13d9312b-f866-48f1-a663-b742f2c3800d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87fe82ce-ae61-4bcb-be58-ba759f72aadb",
            "compositeImage": {
                "id": "ce20d65c-af51-43db-98d5-d7a2ac3db71d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13d9312b-f866-48f1-a663-b742f2c3800d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9cda587-2224-475c-887a-0fb4aa032879",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13d9312b-f866-48f1-a663-b742f2c3800d",
                    "LayerId": "f5d13d03-c323-49ff-80fa-44a791db38ff"
                }
            ]
        },
        {
            "id": "c26c67a0-4570-4d15-9c1f-402dbd0f6976",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87fe82ce-ae61-4bcb-be58-ba759f72aadb",
            "compositeImage": {
                "id": "1606bb20-e512-4b59-9160-bd4f6bbad6c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c26c67a0-4570-4d15-9c1f-402dbd0f6976",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9f06fce-1304-4db5-965c-e65770c7f73b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c26c67a0-4570-4d15-9c1f-402dbd0f6976",
                    "LayerId": "f5d13d03-c323-49ff-80fa-44a791db38ff"
                }
            ]
        },
        {
            "id": "68021901-a366-4ee1-8c3f-4d090305bf56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87fe82ce-ae61-4bcb-be58-ba759f72aadb",
            "compositeImage": {
                "id": "23822093-531d-4035-9d78-7b95c3d4478a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68021901-a366-4ee1-8c3f-4d090305bf56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "691141fa-9c1d-4146-8213-575ee69268b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68021901-a366-4ee1-8c3f-4d090305bf56",
                    "LayerId": "f5d13d03-c323-49ff-80fa-44a791db38ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f5d13d03-c323-49ff-80fa-44a791db38ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "87fe82ce-ae61-4bcb-be58-ba759f72aadb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 48,
    "yorig": 16
}