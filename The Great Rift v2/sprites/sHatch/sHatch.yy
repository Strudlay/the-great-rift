{
    "id": "c3d462cd-aec8-4d45-9d7d-0b8c26ccf11c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHatch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 11,
    "bbox_right": 52,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2754b66-9ed5-4f22-821f-20dfd5fbbfdc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3d462cd-aec8-4d45-9d7d-0b8c26ccf11c",
            "compositeImage": {
                "id": "7dbc694b-33cf-483f-b543-c3a830ac433b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2754b66-9ed5-4f22-821f-20dfd5fbbfdc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9646380f-4cfb-4ea9-abfe-58b68af92ac8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2754b66-9ed5-4f22-821f-20dfd5fbbfdc",
                    "LayerId": "7bbdd2f5-f3cf-4218-b889-224194cdef3c"
                }
            ]
        },
        {
            "id": "71e69f10-e52b-43b9-a2a5-bc9807204474",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3d462cd-aec8-4d45-9d7d-0b8c26ccf11c",
            "compositeImage": {
                "id": "605b46de-fa84-423e-b58a-35f7ad3d4990",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71e69f10-e52b-43b9-a2a5-bc9807204474",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faed8cb6-a5bc-4d60-8ee7-eb681514ad43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71e69f10-e52b-43b9-a2a5-bc9807204474",
                    "LayerId": "7bbdd2f5-f3cf-4218-b889-224194cdef3c"
                }
            ]
        },
        {
            "id": "c5f0a07e-d8ea-48a1-badf-08b90c7b4e01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3d462cd-aec8-4d45-9d7d-0b8c26ccf11c",
            "compositeImage": {
                "id": "7e8623f2-184d-4408-a110-e1cdfe2ed773",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5f0a07e-d8ea-48a1-badf-08b90c7b4e01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b87cee4b-77ea-429c-846f-d0ec769b7202",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5f0a07e-d8ea-48a1-badf-08b90c7b4e01",
                    "LayerId": "7bbdd2f5-f3cf-4218-b889-224194cdef3c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7bbdd2f5-f3cf-4218-b889-224194cdef3c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c3d462cd-aec8-4d45-9d7d-0b8c26ccf11c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 44
}