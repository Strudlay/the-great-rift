{
    "id": "c5d89b89-bd54-49fe-97bb-d0ab4e7edd90",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDualE",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d7f18f7a-ce4c-40c9-95e4-17ab78380457",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5d89b89-bd54-49fe-97bb-d0ab4e7edd90",
            "compositeImage": {
                "id": "973f05c7-3220-476b-94e4-d1fccd0186dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7f18f7a-ce4c-40c9-95e4-17ab78380457",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41a0dcdc-d768-4c7f-afa7-056b90136e24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7f18f7a-ce4c-40c9-95e4-17ab78380457",
                    "LayerId": "0df99358-f9ae-4185-8b3a-4625ada4f27f"
                }
            ]
        },
        {
            "id": "9a5898ad-9c61-4b19-addf-7c8a8b924399",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5d89b89-bd54-49fe-97bb-d0ab4e7edd90",
            "compositeImage": {
                "id": "b6200a77-537f-4634-9924-e0d6e4557f8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a5898ad-9c61-4b19-addf-7c8a8b924399",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f265aa00-e472-4ae8-ac5c-3853c5d835f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a5898ad-9c61-4b19-addf-7c8a8b924399",
                    "LayerId": "0df99358-f9ae-4185-8b3a-4625ada4f27f"
                }
            ]
        },
        {
            "id": "11e3ae86-3338-4d1b-b547-ca64cc68b711",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5d89b89-bd54-49fe-97bb-d0ab4e7edd90",
            "compositeImage": {
                "id": "2053c413-242a-4d90-a728-8f36e419a275",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11e3ae86-3338-4d1b-b547-ca64cc68b711",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c28ee375-50bd-42b1-83df-54aed0d662fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11e3ae86-3338-4d1b-b547-ca64cc68b711",
                    "LayerId": "0df99358-f9ae-4185-8b3a-4625ada4f27f"
                }
            ]
        },
        {
            "id": "b2e32203-17bf-4522-8ef2-4836a846d407",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5d89b89-bd54-49fe-97bb-d0ab4e7edd90",
            "compositeImage": {
                "id": "dedd5e0b-3355-442a-92f7-10e9bc7b3bc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2e32203-17bf-4522-8ef2-4836a846d407",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f40ed26-65ef-487d-a8e1-4ddf996697fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2e32203-17bf-4522-8ef2-4836a846d407",
                    "LayerId": "0df99358-f9ae-4185-8b3a-4625ada4f27f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0df99358-f9ae-4185-8b3a-4625ada4f27f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c5d89b89-bd54-49fe-97bb-d0ab4e7edd90",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}