{
    "id": "65d0e80c-4535-48b3-8137-7044eec67f11",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlant3_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b152457d-2a60-40e5-a061-e486481d41e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d0e80c-4535-48b3-8137-7044eec67f11",
            "compositeImage": {
                "id": "d959b427-24a9-4974-9efb-8824e3eb9e77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b152457d-2a60-40e5-a061-e486481d41e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9230be1-77a5-465d-ab0c-5d946c6e1117",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b152457d-2a60-40e5-a061-e486481d41e2",
                    "LayerId": "e7fca668-278b-40a7-9232-256a0c54dd72"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e7fca668-278b-40a7-9232-256a0c54dd72",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "65d0e80c-4535-48b3-8137-7044eec67f11",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}