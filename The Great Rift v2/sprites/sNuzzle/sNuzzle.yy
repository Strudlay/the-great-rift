{
    "id": "51615423-e6db-408f-9c12-df1be7e33bfa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sNuzzle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f4c7bfa6-3b2c-4bfa-adc6-bb08a60dd8e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51615423-e6db-408f-9c12-df1be7e33bfa",
            "compositeImage": {
                "id": "8a2be7c2-99e6-4c58-948a-4deb85981e14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4c7bfa6-3b2c-4bfa-adc6-bb08a60dd8e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9279efd-45e2-4939-8f97-85c08cc72b83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4c7bfa6-3b2c-4bfa-adc6-bb08a60dd8e0",
                    "LayerId": "675fe138-7474-4a1c-b706-0b3edbfd2980"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "675fe138-7474-4a1c-b706-0b3edbfd2980",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51615423-e6db-408f-9c12-df1be7e33bfa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}