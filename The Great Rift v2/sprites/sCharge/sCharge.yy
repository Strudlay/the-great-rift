{
    "id": "f470a155-b075-4428-9e56-36a15ec15256",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCharge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 119,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2cf35f79-cac3-421a-9d33-26fca7cbcccc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f470a155-b075-4428-9e56-36a15ec15256",
            "compositeImage": {
                "id": "99d789a9-8fc9-4f9c-a7de-fd02715f1c1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cf35f79-cac3-421a-9d33-26fca7cbcccc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "495c86e6-d035-4258-8de2-d79b8a64f03d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cf35f79-cac3-421a-9d33-26fca7cbcccc",
                    "LayerId": "42708d9e-7f93-4564-969f-5264f4d70747"
                }
            ]
        },
        {
            "id": "a665f56f-50b4-4305-9b10-894ffcaff858",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f470a155-b075-4428-9e56-36a15ec15256",
            "compositeImage": {
                "id": "f298e64e-62f8-4f5b-8600-514d331e63c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a665f56f-50b4-4305-9b10-894ffcaff858",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bd1f8df-94e1-4cc0-bc12-fa13efced0bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a665f56f-50b4-4305-9b10-894ffcaff858",
                    "LayerId": "42708d9e-7f93-4564-969f-5264f4d70747"
                }
            ]
        },
        {
            "id": "7a1aa7fc-22a7-4a01-8a15-07fad3fb9368",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f470a155-b075-4428-9e56-36a15ec15256",
            "compositeImage": {
                "id": "a20ad8a8-1693-4bbf-b193-e4c34b02317f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a1aa7fc-22a7-4a01-8a15-07fad3fb9368",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bacf3fe5-9f3c-4d35-a866-1f4291026c6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a1aa7fc-22a7-4a01-8a15-07fad3fb9368",
                    "LayerId": "42708d9e-7f93-4564-969f-5264f4d70747"
                }
            ]
        },
        {
            "id": "8a631408-79bf-4513-a474-ead3702421ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f470a155-b075-4428-9e56-36a15ec15256",
            "compositeImage": {
                "id": "9aadcb93-7b69-44f4-820c-1ab3369c9ebc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a631408-79bf-4513-a474-ead3702421ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e182797-cae6-41d4-bf0b-4326efe0477b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a631408-79bf-4513-a474-ead3702421ed",
                    "LayerId": "42708d9e-7f93-4564-969f-5264f4d70747"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "42708d9e-7f93-4564-969f-5264f4d70747",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f470a155-b075-4428-9e56-36a15ec15256",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 2,
    "yorig": 1
}