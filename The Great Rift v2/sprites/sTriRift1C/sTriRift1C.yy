{
    "id": "9eb7fc44-5b5c-4631-bd4c-6c342426bbaf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTriRift1C",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "90027d87-6ca7-46a1-9318-82a3c5a11afe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eb7fc44-5b5c-4631-bd4c-6c342426bbaf",
            "compositeImage": {
                "id": "9fec2f9e-efea-4c13-b063-63a2945351fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90027d87-6ca7-46a1-9318-82a3c5a11afe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fa3248e-61fb-47e5-a5c1-7364cdc4597f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90027d87-6ca7-46a1-9318-82a3c5a11afe",
                    "LayerId": "96d2f860-2154-450f-868d-893c10e4b298"
                }
            ]
        },
        {
            "id": "8d2eb5ee-e093-4cf0-a871-634543d8c4e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eb7fc44-5b5c-4631-bd4c-6c342426bbaf",
            "compositeImage": {
                "id": "27ee33b5-68a9-4241-b7b8-8a0a5689635f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d2eb5ee-e093-4cf0-a871-634543d8c4e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03425592-6110-4027-b9b0-910fdb7ea704",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d2eb5ee-e093-4cf0-a871-634543d8c4e0",
                    "LayerId": "96d2f860-2154-450f-868d-893c10e4b298"
                }
            ]
        },
        {
            "id": "71b536e2-07bf-499a-9671-567fdfce4876",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eb7fc44-5b5c-4631-bd4c-6c342426bbaf",
            "compositeImage": {
                "id": "858fea36-dc20-45ac-99ee-29d352e1ecb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71b536e2-07bf-499a-9671-567fdfce4876",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "771c290f-c042-414c-865b-44025bf0e730",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71b536e2-07bf-499a-9671-567fdfce4876",
                    "LayerId": "96d2f860-2154-450f-868d-893c10e4b298"
                }
            ]
        },
        {
            "id": "3e24fd1f-1a39-4471-8b35-0306cba3be13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eb7fc44-5b5c-4631-bd4c-6c342426bbaf",
            "compositeImage": {
                "id": "5a66b638-7c17-4d52-b3c8-0dcad33ecaab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e24fd1f-1a39-4471-8b35-0306cba3be13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "683d5c7e-8a7a-4023-96e0-614239eb6f48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e24fd1f-1a39-4471-8b35-0306cba3be13",
                    "LayerId": "96d2f860-2154-450f-868d-893c10e4b298"
                }
            ]
        },
        {
            "id": "f313fb7d-f024-4280-b9de-8a5db859d7e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eb7fc44-5b5c-4631-bd4c-6c342426bbaf",
            "compositeImage": {
                "id": "cd42912a-36f4-4e0c-bb6c-30a34ed5ec12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f313fb7d-f024-4280-b9de-8a5db859d7e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08098407-7c0a-4a3e-8698-a513ad83330a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f313fb7d-f024-4280-b9de-8a5db859d7e8",
                    "LayerId": "96d2f860-2154-450f-868d-893c10e4b298"
                }
            ]
        },
        {
            "id": "c0218b20-f5e7-4bd6-a736-62ff601663cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eb7fc44-5b5c-4631-bd4c-6c342426bbaf",
            "compositeImage": {
                "id": "87ec3e16-322b-4e23-a903-65602f42c50d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0218b20-f5e7-4bd6-a736-62ff601663cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e96c09c0-1fca-42e4-a1fb-676b0fc9f9e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0218b20-f5e7-4bd6-a736-62ff601663cc",
                    "LayerId": "96d2f860-2154-450f-868d-893c10e4b298"
                }
            ]
        },
        {
            "id": "bc79af47-1751-43c5-9266-b99dcda588e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eb7fc44-5b5c-4631-bd4c-6c342426bbaf",
            "compositeImage": {
                "id": "05b96104-5820-47b2-8091-b8249ee52096",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc79af47-1751-43c5-9266-b99dcda588e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2170f57-f15a-4557-ba52-10c70222fdf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc79af47-1751-43c5-9266-b99dcda588e9",
                    "LayerId": "96d2f860-2154-450f-868d-893c10e4b298"
                }
            ]
        },
        {
            "id": "29fd4506-58b4-4c89-b779-882a5497fde6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eb7fc44-5b5c-4631-bd4c-6c342426bbaf",
            "compositeImage": {
                "id": "03f6ec48-f13e-43e5-b7a6-f24a229a4c6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29fd4506-58b4-4c89-b779-882a5497fde6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "528eb967-f7f9-4ad8-a3a4-95d7a4529201",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29fd4506-58b4-4c89-b779-882a5497fde6",
                    "LayerId": "96d2f860-2154-450f-868d-893c10e4b298"
                }
            ]
        },
        {
            "id": "841bd8f8-d0f0-4bbf-a2a2-79f7cf646045",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eb7fc44-5b5c-4631-bd4c-6c342426bbaf",
            "compositeImage": {
                "id": "6e97262e-1636-4500-96dc-00e85a954a2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "841bd8f8-d0f0-4bbf-a2a2-79f7cf646045",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f833b926-75a4-4c51-b294-127b6bcec0cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "841bd8f8-d0f0-4bbf-a2a2-79f7cf646045",
                    "LayerId": "96d2f860-2154-450f-868d-893c10e4b298"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "96d2f860-2154-450f-868d-893c10e4b298",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9eb7fc44-5b5c-4631-bd4c-6c342426bbaf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}