{
    "id": "1727cfba-cbcc-4032-b58c-98f0cab1ca5b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMount",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "efa3a4e8-7e2c-4656-bfb8-79f1a5ff5038",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1727cfba-cbcc-4032-b58c-98f0cab1ca5b",
            "compositeImage": {
                "id": "2c1e6150-af82-4e14-9241-4c2008ad6fa1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efa3a4e8-7e2c-4656-bfb8-79f1a5ff5038",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5a92acb-4ae9-4487-95fd-a85707fef37d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efa3a4e8-7e2c-4656-bfb8-79f1a5ff5038",
                    "LayerId": "6950ee42-c694-4f06-8de4-ff3d2fbc4afd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6950ee42-c694-4f06-8de4-ff3d2fbc4afd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1727cfba-cbcc-4032-b58c-98f0cab1ca5b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}