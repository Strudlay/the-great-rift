{
    "id": "a08168de-56b8-4882-a916-c99e36a85805",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTriRift3C",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 27,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a3af51a-a611-4fb5-8003-5b49e8d334ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a08168de-56b8-4882-a916-c99e36a85805",
            "compositeImage": {
                "id": "3e136a87-40f2-4617-bffa-b02e02a44d76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a3af51a-a611-4fb5-8003-5b49e8d334ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19fdadbb-162e-4650-82c0-fd3bab44a908",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a3af51a-a611-4fb5-8003-5b49e8d334ac",
                    "LayerId": "1875090d-d48e-4803-8da9-256ebc58f7ff"
                }
            ]
        },
        {
            "id": "d4410250-e0ed-4427-a214-e5c6a93496f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a08168de-56b8-4882-a916-c99e36a85805",
            "compositeImage": {
                "id": "bc72b38b-3aa6-49ed-a7d8-7db661531ec9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4410250-e0ed-4427-a214-e5c6a93496f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d384569-e95b-4313-8d7f-b6300b6af6eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4410250-e0ed-4427-a214-e5c6a93496f1",
                    "LayerId": "1875090d-d48e-4803-8da9-256ebc58f7ff"
                }
            ]
        },
        {
            "id": "d655fb89-33ec-4c22-9b67-7a12adc0aa90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a08168de-56b8-4882-a916-c99e36a85805",
            "compositeImage": {
                "id": "2fbb827d-d9ce-4c7e-8802-a9d4c0102811",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d655fb89-33ec-4c22-9b67-7a12adc0aa90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df55202e-1d4d-42d1-b1b9-7844349dca84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d655fb89-33ec-4c22-9b67-7a12adc0aa90",
                    "LayerId": "1875090d-d48e-4803-8da9-256ebc58f7ff"
                }
            ]
        },
        {
            "id": "13584778-f971-420c-846c-db3e6109dfe5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a08168de-56b8-4882-a916-c99e36a85805",
            "compositeImage": {
                "id": "d11a129a-06cb-4264-9f31-6f71f49995df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13584778-f971-420c-846c-db3e6109dfe5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "911ebb1e-c339-420e-8768-306c232088c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13584778-f971-420c-846c-db3e6109dfe5",
                    "LayerId": "1875090d-d48e-4803-8da9-256ebc58f7ff"
                }
            ]
        },
        {
            "id": "30f61db8-58cd-4c8e-8d0e-e9e58fdb1b45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a08168de-56b8-4882-a916-c99e36a85805",
            "compositeImage": {
                "id": "5dd80f57-4b04-44e1-b629-117929119735",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30f61db8-58cd-4c8e-8d0e-e9e58fdb1b45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55c23c24-c0a7-47e4-8f41-ee5bf666a591",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30f61db8-58cd-4c8e-8d0e-e9e58fdb1b45",
                    "LayerId": "1875090d-d48e-4803-8da9-256ebc58f7ff"
                }
            ]
        },
        {
            "id": "8ad08b93-e934-4fc1-94be-4651d1e4bc60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a08168de-56b8-4882-a916-c99e36a85805",
            "compositeImage": {
                "id": "34ea212e-b3a5-407a-ba7b-4874cecb2d67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ad08b93-e934-4fc1-94be-4651d1e4bc60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc3ab0de-9f42-4dd1-9ef0-79c6824a373f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ad08b93-e934-4fc1-94be-4651d1e4bc60",
                    "LayerId": "1875090d-d48e-4803-8da9-256ebc58f7ff"
                }
            ]
        },
        {
            "id": "241c4fe4-1baf-4f0a-87b5-eecaf3b24cb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a08168de-56b8-4882-a916-c99e36a85805",
            "compositeImage": {
                "id": "237a7350-ebaa-4d46-be99-8c9f825d52af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "241c4fe4-1baf-4f0a-87b5-eecaf3b24cb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a2422e4-f038-4e7f-8af6-2d0cabd36d57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "241c4fe4-1baf-4f0a-87b5-eecaf3b24cb2",
                    "LayerId": "1875090d-d48e-4803-8da9-256ebc58f7ff"
                }
            ]
        },
        {
            "id": "aab4f40b-adf4-4e61-a667-ee93f384f4a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a08168de-56b8-4882-a916-c99e36a85805",
            "compositeImage": {
                "id": "e50d1670-11de-4fbf-bbe0-868788b9a94f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aab4f40b-adf4-4e61-a667-ee93f384f4a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c78a3684-adda-4052-9003-c49c92b44371",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aab4f40b-adf4-4e61-a667-ee93f384f4a6",
                    "LayerId": "1875090d-d48e-4803-8da9-256ebc58f7ff"
                }
            ]
        },
        {
            "id": "bbd99cae-2503-4c7e-981c-4d9a5d88d4f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a08168de-56b8-4882-a916-c99e36a85805",
            "compositeImage": {
                "id": "bddad7ec-49e1-4468-82f3-c0aa8e2ce347",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbd99cae-2503-4c7e-981c-4d9a5d88d4f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a68e99d-5eed-4f70-8b51-c5b5e74b6871",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbd99cae-2503-4c7e-981c-4d9a5d88d4f4",
                    "LayerId": "1875090d-d48e-4803-8da9-256ebc58f7ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1875090d-d48e-4803-8da9-256ebc58f7ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a08168de-56b8-4882-a916-c99e36a85805",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 56,
    "yorig": 18
}