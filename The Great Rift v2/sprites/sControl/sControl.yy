{
    "id": "5bd53578-a81a-4bc5-9023-62d2ca1b551d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sControl",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 685,
    "bbox_left": 339,
    "bbox_right": 916,
    "bbox_top": 643,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a2ea179-972c-472c-afc1-62dc1bb70768",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bd53578-a81a-4bc5-9023-62d2ca1b551d",
            "compositeImage": {
                "id": "e9b1e991-7eb9-40df-962d-8c3ab69bc56b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a2ea179-972c-472c-afc1-62dc1bb70768",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "719f1c0b-e1e6-4106-9d91-82faabc4b675",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a2ea179-972c-472c-afc1-62dc1bb70768",
                    "LayerId": "5199db1e-8de5-4a87-8f41-cec66b717484"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "5199db1e-8de5-4a87-8f41-cec66b717484",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5bd53578-a81a-4bc5-9023-62d2ca1b551d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 0,
    "yorig": 0
}