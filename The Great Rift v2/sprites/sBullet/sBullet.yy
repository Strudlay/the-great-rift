{
    "id": "bb2871f4-fa81-40c9-99cf-939f930762b8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 2,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e487434-74c6-43f1-ad01-a85fd8dc82e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb2871f4-fa81-40c9-99cf-939f930762b8",
            "compositeImage": {
                "id": "34292c79-9b97-4ba0-8986-161aa9f5097c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e487434-74c6-43f1-ad01-a85fd8dc82e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "418498cd-b24b-47e2-9a72-cf6be4ebbbe0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e487434-74c6-43f1-ad01-a85fd8dc82e0",
                    "LayerId": "83a513ec-9d0f-4b2a-af2c-cf8f53fdffec"
                }
            ]
        },
        {
            "id": "3577a94e-0bbc-4bf0-9ecf-c05c5d2a2131",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb2871f4-fa81-40c9-99cf-939f930762b8",
            "compositeImage": {
                "id": "a77ef43a-de6d-4d43-b628-bde3a1988ba4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3577a94e-0bbc-4bf0-9ecf-c05c5d2a2131",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daf7e847-ae03-400f-a11b-b000d40600b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3577a94e-0bbc-4bf0-9ecf-c05c5d2a2131",
                    "LayerId": "83a513ec-9d0f-4b2a-af2c-cf8f53fdffec"
                }
            ]
        },
        {
            "id": "eb5822cb-5723-4d72-8d90-5036205b18cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb2871f4-fa81-40c9-99cf-939f930762b8",
            "compositeImage": {
                "id": "ff7f2021-3af0-49bf-9c36-d497cc01c657",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb5822cb-5723-4d72-8d90-5036205b18cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b844dbfa-8d75-4920-abf4-3270f81fe02c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb5822cb-5723-4d72-8d90-5036205b18cf",
                    "LayerId": "83a513ec-9d0f-4b2a-af2c-cf8f53fdffec"
                }
            ]
        },
        {
            "id": "8e33e79b-ea74-4d80-acda-fc38a1393d16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb2871f4-fa81-40c9-99cf-939f930762b8",
            "compositeImage": {
                "id": "bf20ef87-462a-48cd-a42b-2284877bdbe3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e33e79b-ea74-4d80-acda-fc38a1393d16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aac54a1a-e209-42a7-b5f1-23849c2b2bca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e33e79b-ea74-4d80-acda-fc38a1393d16",
                    "LayerId": "83a513ec-9d0f-4b2a-af2c-cf8f53fdffec"
                }
            ]
        },
        {
            "id": "aa808113-7220-4351-9937-9cb81c081470",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb2871f4-fa81-40c9-99cf-939f930762b8",
            "compositeImage": {
                "id": "c87ef8ce-8961-4a92-86d9-df54880f3a16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa808113-7220-4351-9937-9cb81c081470",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2609ff1-6c2a-458c-9b02-dc7dfea0dce5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa808113-7220-4351-9937-9cb81c081470",
                    "LayerId": "83a513ec-9d0f-4b2a-af2c-cf8f53fdffec"
                }
            ]
        },
        {
            "id": "97996a5a-5d2d-425b-a931-6dd23e7f521c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb2871f4-fa81-40c9-99cf-939f930762b8",
            "compositeImage": {
                "id": "810493cb-2e50-489a-9f9f-e8b1df895e8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97996a5a-5d2d-425b-a931-6dd23e7f521c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "475f7e22-faf5-4b95-bbf0-06ed957a7d4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97996a5a-5d2d-425b-a931-6dd23e7f521c",
                    "LayerId": "83a513ec-9d0f-4b2a-af2c-cf8f53fdffec"
                }
            ]
        },
        {
            "id": "190701da-a449-4e00-8456-5e887fc390b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb2871f4-fa81-40c9-99cf-939f930762b8",
            "compositeImage": {
                "id": "de5d0f7e-161f-441a-94ff-e121297f421c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "190701da-a449-4e00-8456-5e887fc390b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef179b24-0cd7-4307-acd0-d301f492b997",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "190701da-a449-4e00-8456-5e887fc390b7",
                    "LayerId": "83a513ec-9d0f-4b2a-af2c-cf8f53fdffec"
                }
            ]
        },
        {
            "id": "f948d5be-a212-4d9b-9ce3-d78624b06c10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb2871f4-fa81-40c9-99cf-939f930762b8",
            "compositeImage": {
                "id": "c7abe3ca-959b-4d22-88b9-242c228d5455",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f948d5be-a212-4d9b-9ce3-d78624b06c10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d456923c-0a02-4cef-93aa-a4c73376e277",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f948d5be-a212-4d9b-9ce3-d78624b06c10",
                    "LayerId": "83a513ec-9d0f-4b2a-af2c-cf8f53fdffec"
                }
            ]
        },
        {
            "id": "64fc54de-4699-4f71-9183-bb7c7c309abe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb2871f4-fa81-40c9-99cf-939f930762b8",
            "compositeImage": {
                "id": "bfc8acc1-db06-4b81-bb8e-d8beed3b4182",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64fc54de-4699-4f71-9183-bb7c7c309abe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba4f2d8a-140f-4c8e-ac1d-b5fd7159d8a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64fc54de-4699-4f71-9183-bb7c7c309abe",
                    "LayerId": "83a513ec-9d0f-4b2a-af2c-cf8f53fdffec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "83a513ec-9d0f-4b2a-af2c-cf8f53fdffec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bb2871f4-fa81-40c9-99cf-939f930762b8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 2,
    "yorig": 2
}