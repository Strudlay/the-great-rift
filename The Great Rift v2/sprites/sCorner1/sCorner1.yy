{
    "id": "30f1b58a-72c9-4687-be2b-0429ed32d484",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCorner1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c50b633e-53e7-48d1-8d28-6b0f71e3f138",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30f1b58a-72c9-4687-be2b-0429ed32d484",
            "compositeImage": {
                "id": "4382562f-3e81-4e95-8583-eda7ca4f136d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c50b633e-53e7-48d1-8d28-6b0f71e3f138",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf725cc5-9e43-4e34-b046-f529741588c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c50b633e-53e7-48d1-8d28-6b0f71e3f138",
                    "LayerId": "ce9d687f-86ea-46e6-911b-ef0d03db26ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ce9d687f-86ea-46e6-911b-ef0d03db26ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30f1b58a-72c9-4687-be2b-0429ed32d484",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}