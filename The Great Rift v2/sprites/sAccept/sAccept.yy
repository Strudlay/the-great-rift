{
    "id": "a9626f9a-ea23-4d58-ac2e-3b5865cf5c9b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAccept",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 686,
    "bbox_left": 489,
    "bbox_right": 757,
    "bbox_top": 645,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a5a81d38-e829-49e6-bfcb-2eb405bb01ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9626f9a-ea23-4d58-ac2e-3b5865cf5c9b",
            "compositeImage": {
                "id": "834a42d2-1375-411e-8301-f2e4cef01f07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5a81d38-e829-49e6-bfcb-2eb405bb01ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0041b053-610e-4eec-bba5-e5c4f48ee5a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5a81d38-e829-49e6-bfcb-2eb405bb01ad",
                    "LayerId": "b46333d9-dd01-44f5-a0a4-4e9bf0bed1a3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "b46333d9-dd01-44f5-a0a4-4e9bf0bed1a3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a9626f9a-ea23-4d58-ac2e-3b5865cf5c9b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 0,
    "yorig": 0
}