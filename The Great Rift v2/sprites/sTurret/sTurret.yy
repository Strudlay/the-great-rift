{
    "id": "c2998e38-7a09-491e-ac6f-720962bed541",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTurret",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a9954c5b-1896-4495-b43a-3d796a137e38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2998e38-7a09-491e-ac6f-720962bed541",
            "compositeImage": {
                "id": "f77ee606-61e6-4911-b5f5-8388a75d1fe4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9954c5b-1896-4495-b43a-3d796a137e38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7ba802d-c073-4ce4-bd64-c271865e1fea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9954c5b-1896-4495-b43a-3d796a137e38",
                    "LayerId": "d8db2b6c-5f41-4986-8c12-536e38417ad1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d8db2b6c-5f41-4986-8c12-536e38417ad1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2998e38-7a09-491e-ac6f-720962bed541",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}