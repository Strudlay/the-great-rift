{
    "id": "8134a9cf-44c2-4b16-8db9-504f8e1bc2e5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLaserC",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 111,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b9054d5b-aaaf-4761-8013-e1def7597e40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8134a9cf-44c2-4b16-8db9-504f8e1bc2e5",
            "compositeImage": {
                "id": "d385aa7b-9337-42fe-8e56-5f3202a339c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9054d5b-aaaf-4761-8013-e1def7597e40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0531b4b5-cb86-43c2-bed5-926a9375a5d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9054d5b-aaaf-4761-8013-e1def7597e40",
                    "LayerId": "da295645-84b8-4f49-88f5-f25eab1faf89"
                }
            ]
        },
        {
            "id": "7257ba0d-33c1-4d50-bcb9-9c307bd82df8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8134a9cf-44c2-4b16-8db9-504f8e1bc2e5",
            "compositeImage": {
                "id": "0ffbef71-214a-4004-ab81-08f09795aa2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7257ba0d-33c1-4d50-bcb9-9c307bd82df8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62baba43-34ac-4780-a177-44cc9d65b965",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7257ba0d-33c1-4d50-bcb9-9c307bd82df8",
                    "LayerId": "da295645-84b8-4f49-88f5-f25eab1faf89"
                }
            ]
        },
        {
            "id": "5695f82d-e256-403f-88d9-d726bed521ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8134a9cf-44c2-4b16-8db9-504f8e1bc2e5",
            "compositeImage": {
                "id": "5bb8c10d-5b9a-413d-b39a-f62a1584383b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5695f82d-e256-403f-88d9-d726bed521ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00f64dc1-ab0b-410a-9b7a-64dc48182ae7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5695f82d-e256-403f-88d9-d726bed521ae",
                    "LayerId": "da295645-84b8-4f49-88f5-f25eab1faf89"
                }
            ]
        },
        {
            "id": "c59215d7-7ed1-4175-8379-07bef3f314dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8134a9cf-44c2-4b16-8db9-504f8e1bc2e5",
            "compositeImage": {
                "id": "c1310b45-ddc1-467c-9924-17f84d00f82c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c59215d7-7ed1-4175-8379-07bef3f314dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92bbe89d-4cda-4269-89f5-30b609b36586",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c59215d7-7ed1-4175-8379-07bef3f314dc",
                    "LayerId": "da295645-84b8-4f49-88f5-f25eab1faf89"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "da295645-84b8-4f49-88f5-f25eab1faf89",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8134a9cf-44c2-4b16-8db9-504f8e1bc2e5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}