{
    "id": "04761bf6-5944-4a9f-800a-7c2b2b7acf4d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLaser",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fbd9d175-72f3-45c7-a0b8-41e31806496b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04761bf6-5944-4a9f-800a-7c2b2b7acf4d",
            "compositeImage": {
                "id": "729502f3-48f7-42ed-9827-0fa12c32f9e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbd9d175-72f3-45c7-a0b8-41e31806496b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8233453-f6fa-47ef-bf6c-dd4020217d6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbd9d175-72f3-45c7-a0b8-41e31806496b",
                    "LayerId": "90d4b852-f9fd-4988-860f-6e2f24cbe4fd"
                }
            ]
        },
        {
            "id": "2cbd151b-ccb5-4056-9fd1-cfc728a2578a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04761bf6-5944-4a9f-800a-7c2b2b7acf4d",
            "compositeImage": {
                "id": "d71007bf-33c4-4bea-be8b-eda29b478484",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cbd151b-ccb5-4056-9fd1-cfc728a2578a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d429d98-68e6-445c-ad33-82ec64181325",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cbd151b-ccb5-4056-9fd1-cfc728a2578a",
                    "LayerId": "90d4b852-f9fd-4988-860f-6e2f24cbe4fd"
                }
            ]
        },
        {
            "id": "84286021-04be-42c7-abbc-ef06fe70318b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04761bf6-5944-4a9f-800a-7c2b2b7acf4d",
            "compositeImage": {
                "id": "7e5207de-e391-4137-b252-6cafe58706af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84286021-04be-42c7-abbc-ef06fe70318b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "699e8fb1-b320-431a-868d-9d47da0a2d13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84286021-04be-42c7-abbc-ef06fe70318b",
                    "LayerId": "90d4b852-f9fd-4988-860f-6e2f24cbe4fd"
                }
            ]
        },
        {
            "id": "56ee453c-91a5-4f04-a4cf-b32ba64f5e30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04761bf6-5944-4a9f-800a-7c2b2b7acf4d",
            "compositeImage": {
                "id": "28acf912-290a-45aa-9c48-324f8e6b9448",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56ee453c-91a5-4f04-a4cf-b32ba64f5e30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11eccea2-bec5-4fa3-975b-490851f8f644",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56ee453c-91a5-4f04-a4cf-b32ba64f5e30",
                    "LayerId": "90d4b852-f9fd-4988-860f-6e2f24cbe4fd"
                }
            ]
        },
        {
            "id": "52227af6-464f-4f6f-a7a8-02c1b50974a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04761bf6-5944-4a9f-800a-7c2b2b7acf4d",
            "compositeImage": {
                "id": "fde5310d-2d0c-4c3e-a5a6-91d7e0b52761",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52227af6-464f-4f6f-a7a8-02c1b50974a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ff7574a-e49f-4ece-9b67-6c9ef1ea3e0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52227af6-464f-4f6f-a7a8-02c1b50974a5",
                    "LayerId": "90d4b852-f9fd-4988-860f-6e2f24cbe4fd"
                }
            ]
        },
        {
            "id": "9b3adc86-5342-44bf-8023-e4b6dc227b24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04761bf6-5944-4a9f-800a-7c2b2b7acf4d",
            "compositeImage": {
                "id": "d4b4c177-e6d2-4021-9321-31a7e96b4022",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b3adc86-5342-44bf-8023-e4b6dc227b24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4389402-ff91-4c0d-a338-4f953dd1c789",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b3adc86-5342-44bf-8023-e4b6dc227b24",
                    "LayerId": "90d4b852-f9fd-4988-860f-6e2f24cbe4fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "90d4b852-f9fd-4988-860f-6e2f24cbe4fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "04761bf6-5944-4a9f-800a-7c2b2b7acf4d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 12,
    "yorig": 2
}