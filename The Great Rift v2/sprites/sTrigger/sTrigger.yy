{
    "id": "1d22fa8e-6ceb-4917-931a-20e0d0d52b6e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTrigger",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bc9c43bc-35ed-4cfa-8870-68cdb06c59ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d22fa8e-6ceb-4917-931a-20e0d0d52b6e",
            "compositeImage": {
                "id": "e9a0f4c0-45ee-42a4-a5d9-b459ea066e36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc9c43bc-35ed-4cfa-8870-68cdb06c59ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93b6162f-5c6d-4abd-9944-1f7f9ce86b47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc9c43bc-35ed-4cfa-8870-68cdb06c59ce",
                    "LayerId": "220dc639-c021-4adc-95fb-820f212d7626"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "220dc639-c021-4adc-95fb-820f212d7626",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d22fa8e-6ceb-4917-931a-20e0d0d52b6e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}