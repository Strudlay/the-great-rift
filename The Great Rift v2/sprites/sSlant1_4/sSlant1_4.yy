{
    "id": "7571be38-241a-4ddc-b78b-bccf68cd33f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlant1_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "317266ab-e46f-4d6c-9bc1-4c70c56ea2d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7571be38-241a-4ddc-b78b-bccf68cd33f8",
            "compositeImage": {
                "id": "98308a1f-2fe0-4e02-8899-0fe3cce447d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "317266ab-e46f-4d6c-9bc1-4c70c56ea2d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8042683d-be8c-44e6-bb16-cd4d10b49828",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "317266ab-e46f-4d6c-9bc1-4c70c56ea2d1",
                    "LayerId": "73fc4d7f-6618-4cb4-8f7f-7b5302fa65a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "73fc4d7f-6618-4cb4-8f7f-7b5302fa65a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7571be38-241a-4ddc-b78b-bccf68cd33f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}