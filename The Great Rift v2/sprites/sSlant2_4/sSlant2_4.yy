{
    "id": "35286e51-22b7-4f95-956e-92be617842ed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlant2_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4870dd3a-e280-46c7-922a-af76dc363232",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35286e51-22b7-4f95-956e-92be617842ed",
            "compositeImage": {
                "id": "cc91d25b-9633-4727-8af1-b2d431abb633",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4870dd3a-e280-46c7-922a-af76dc363232",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7288f6ea-220f-475e-92b8-1af156480c0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4870dd3a-e280-46c7-922a-af76dc363232",
                    "LayerId": "6b41a3b9-d657-4434-8473-668da1ec07f0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6b41a3b9-d657-4434-8473-668da1ec07f0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35286e51-22b7-4f95-956e-92be617842ed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}