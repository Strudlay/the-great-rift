{
    "id": "e6ee84fb-3dea-4e98-8f04-f57dc9ca5622",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlant3_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7846a50f-78b2-4f3c-bbaf-2949d55c6e60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6ee84fb-3dea-4e98-8f04-f57dc9ca5622",
            "compositeImage": {
                "id": "18ec7a12-a2fd-4b37-bed2-112691aa9d7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7846a50f-78b2-4f3c-bbaf-2949d55c6e60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "292c042f-80ea-422e-a26a-8dab10500ef0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7846a50f-78b2-4f3c-bbaf-2949d55c6e60",
                    "LayerId": "f809a744-48a4-4973-9e46-4adfe1c3c5f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f809a744-48a4-4973-9e46-4adfe1c3c5f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6ee84fb-3dea-4e98-8f04-f57dc9ca5622",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}