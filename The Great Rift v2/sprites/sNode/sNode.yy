{
    "id": "f1e70b5b-ea5b-4af2-acb3-263d4d1def62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sNode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 1,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3445c8c7-c7fa-4fca-8091-1a6ca66e213c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1e70b5b-ea5b-4af2-acb3-263d4d1def62",
            "compositeImage": {
                "id": "082c224f-8182-471b-a5ba-402b56ee728a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3445c8c7-c7fa-4fca-8091-1a6ca66e213c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8eb8d80-5626-4316-9997-13f2b8bf86c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3445c8c7-c7fa-4fca-8091-1a6ca66e213c",
                    "LayerId": "2b1ef4c5-4f00-4dd2-8f71-f3f44b4d5d09"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2b1ef4c5-4f00-4dd2-8f71-f3f44b4d5d09",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1e70b5b-ea5b-4af2-acb3-263d4d1def62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 44,
    "yorig": 32
}