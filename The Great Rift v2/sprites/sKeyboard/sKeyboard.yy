{
    "id": "4a9c77ba-a668-47e2-abe0-2cfd8030cd29",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sKeyboard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 683,
    "bbox_left": 158,
    "bbox_right": 1132,
    "bbox_top": 41,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "50c250c9-0040-4104-9cde-2fe34fe11197",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a9c77ba-a668-47e2-abe0-2cfd8030cd29",
            "compositeImage": {
                "id": "e2d0a8d6-b0a4-4234-af36-251cfefdcced",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50c250c9-0040-4104-9cde-2fe34fe11197",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "179a06ec-b3f4-48b8-9253-912f0ebc5bb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50c250c9-0040-4104-9cde-2fe34fe11197",
                    "LayerId": "726b3a1c-fe56-4be0-a70c-7995235e6451"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "726b3a1c-fe56-4be0-a70c-7995235e6451",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4a9c77ba-a668-47e2-abe0-2cfd8030cd29",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 0,
    "yorig": 0
}