{
    "id": "1fdf447a-ca97-4007-9e5e-d6e0e8978df2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTitle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 420,
    "bbox_left": 311,
    "bbox_right": 913,
    "bbox_top": 217,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f56458d0-5c08-4169-b4c7-6a8d4739c264",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fdf447a-ca97-4007-9e5e-d6e0e8978df2",
            "compositeImage": {
                "id": "0df8bccd-7da8-4da4-97d3-a2de94c9f6c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f56458d0-5c08-4169-b4c7-6a8d4739c264",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a418a99f-77bc-4883-b5a4-cf0f80b0ba59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f56458d0-5c08-4169-b4c7-6a8d4739c264",
                    "LayerId": "00c9026d-f7c9-4a83-9899-b4ad65caacf7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "00c9026d-f7c9-4a83-9899-b4ad65caacf7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1fdf447a-ca97-4007-9e5e-d6e0e8978df2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 640,
    "yorig": 360
}