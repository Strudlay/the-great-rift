{
    "id": "c1377dce-5fe3-4a3f-a03d-3fc00f31b2eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGamepad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 684,
    "bbox_left": 104,
    "bbox_right": 1147,
    "bbox_top": 38,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "07b0d44d-9c09-4f2e-8145-0ddb8c0c5939",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1377dce-5fe3-4a3f-a03d-3fc00f31b2eb",
            "compositeImage": {
                "id": "bf6cdd07-4f88-477e-aedc-780904002149",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07b0d44d-9c09-4f2e-8145-0ddb8c0c5939",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "535244ac-7da9-4760-b5d0-1c8a579c84f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07b0d44d-9c09-4f2e-8145-0ddb8c0c5939",
                    "LayerId": "86ebedc3-9730-4b2b-adcc-397e8010c26a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "86ebedc3-9730-4b2b-adcc-397e8010c26a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c1377dce-5fe3-4a3f-a03d-3fc00f31b2eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 0,
    "yorig": 0
}