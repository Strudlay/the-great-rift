{
    "id": "ff71b15e-9a61-4c48-aff7-625fca208c82",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBarrier",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b1cc6465-6da9-414e-9f76-09b628615f2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff71b15e-9a61-4c48-aff7-625fca208c82",
            "compositeImage": {
                "id": "345bc4e0-1801-4663-be67-76c054bb1d18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1cc6465-6da9-414e-9f76-09b628615f2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3055a4b1-6285-4164-8ede-1f9a2ea48175",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1cc6465-6da9-414e-9f76-09b628615f2c",
                    "LayerId": "8270d28a-4a3d-4211-8a9b-a9ba99dbf315"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "8270d28a-4a3d-4211-8a9b-a9ba99dbf315",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff71b15e-9a61-4c48-aff7-625fca208c82",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 640,
    "yorig": 360
}