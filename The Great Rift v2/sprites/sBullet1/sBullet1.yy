{
    "id": "8918034e-74ab-47a4-8635-9443a90580ed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBullet1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5592b8dc-1965-4a63-95f2-20ec2e1177d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8918034e-74ab-47a4-8635-9443a90580ed",
            "compositeImage": {
                "id": "67688e1f-b3c0-44d7-b02e-61c890aca506",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5592b8dc-1965-4a63-95f2-20ec2e1177d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0e1adfc-2688-4460-b7a2-133d78615a2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5592b8dc-1965-4a63-95f2-20ec2e1177d1",
                    "LayerId": "8b77a01b-2193-4171-8c36-e7ebd5b6c34e"
                }
            ]
        },
        {
            "id": "b50e708f-e5b0-4afd-bda1-d45f4d0748f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8918034e-74ab-47a4-8635-9443a90580ed",
            "compositeImage": {
                "id": "93e7ba6f-fb4c-417a-b165-6132f4a9f2f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b50e708f-e5b0-4afd-bda1-d45f4d0748f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cf73477-9fd9-4434-b9f3-9d32f2868219",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b50e708f-e5b0-4afd-bda1-d45f4d0748f6",
                    "LayerId": "8b77a01b-2193-4171-8c36-e7ebd5b6c34e"
                }
            ]
        },
        {
            "id": "bd9b4a34-da9e-4c05-bfdc-491161299de3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8918034e-74ab-47a4-8635-9443a90580ed",
            "compositeImage": {
                "id": "a93935c4-df75-4fd5-a8dd-d31b9f92248a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd9b4a34-da9e-4c05-bfdc-491161299de3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dbfa78c-289f-4777-845f-c9e0fac682ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd9b4a34-da9e-4c05-bfdc-491161299de3",
                    "LayerId": "8b77a01b-2193-4171-8c36-e7ebd5b6c34e"
                }
            ]
        },
        {
            "id": "8eb2afc1-d29a-4f41-be0c-9042bbedb160",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8918034e-74ab-47a4-8635-9443a90580ed",
            "compositeImage": {
                "id": "a02f6d58-8192-45f7-87da-be70ac3dd0a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8eb2afc1-d29a-4f41-be0c-9042bbedb160",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "849b0a58-59ff-414f-ab88-9284e0d1ec2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8eb2afc1-d29a-4f41-be0c-9042bbedb160",
                    "LayerId": "8b77a01b-2193-4171-8c36-e7ebd5b6c34e"
                }
            ]
        },
        {
            "id": "2fbb1a77-a186-4630-bb49-6b369168d8d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8918034e-74ab-47a4-8635-9443a90580ed",
            "compositeImage": {
                "id": "b8155cf0-4a9c-4040-9e64-08b75bd11a5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fbb1a77-a186-4630-bb49-6b369168d8d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95d43b9a-07bf-4616-a7d9-4ec1845a9943",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fbb1a77-a186-4630-bb49-6b369168d8d5",
                    "LayerId": "8b77a01b-2193-4171-8c36-e7ebd5b6c34e"
                }
            ]
        },
        {
            "id": "a2839add-b694-4b75-ace3-bfbbd130a963",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8918034e-74ab-47a4-8635-9443a90580ed",
            "compositeImage": {
                "id": "811eb0b1-379f-4481-8809-833f81ea8202",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2839add-b694-4b75-ace3-bfbbd130a963",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d01c665-1d3d-4be6-be90-8dbe5032a845",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2839add-b694-4b75-ace3-bfbbd130a963",
                    "LayerId": "8b77a01b-2193-4171-8c36-e7ebd5b6c34e"
                }
            ]
        },
        {
            "id": "6a05ab88-59cc-4cac-bf1f-37823ec9b427",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8918034e-74ab-47a4-8635-9443a90580ed",
            "compositeImage": {
                "id": "86b54c0a-6644-42d5-8c36-a12ef1b3916c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a05ab88-59cc-4cac-bf1f-37823ec9b427",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "942432b5-667f-4d4d-ac79-03adcc952425",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a05ab88-59cc-4cac-bf1f-37823ec9b427",
                    "LayerId": "8b77a01b-2193-4171-8c36-e7ebd5b6c34e"
                }
            ]
        },
        {
            "id": "6ff0dbf7-e303-42ed-8f78-a46afdca0eac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8918034e-74ab-47a4-8635-9443a90580ed",
            "compositeImage": {
                "id": "a0f2703f-7c95-4400-80f9-5ca7e3597dd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ff0dbf7-e303-42ed-8f78-a46afdca0eac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6670d799-72e6-4672-ad5c-e93099228cc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ff0dbf7-e303-42ed-8f78-a46afdca0eac",
                    "LayerId": "8b77a01b-2193-4171-8c36-e7ebd5b6c34e"
                }
            ]
        },
        {
            "id": "08a324c7-8b82-439d-8687-3e7b58057d3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8918034e-74ab-47a4-8635-9443a90580ed",
            "compositeImage": {
                "id": "e7461e36-b5f2-4965-8876-e895bd038e33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08a324c7-8b82-439d-8687-3e7b58057d3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02c46af6-a596-4310-9a91-d6e04213f707",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08a324c7-8b82-439d-8687-3e7b58057d3a",
                    "LayerId": "8b77a01b-2193-4171-8c36-e7ebd5b6c34e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "8b77a01b-2193-4171-8c36-e7ebd5b6c34e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8918034e-74ab-47a4-8635-9443a90580ed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}