{
    "id": "8d67e5c9-a382-40bb-8e02-aaef2169ad6f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTriE",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5305b147-f471-4a9b-98dd-937b53f7deef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d67e5c9-a382-40bb-8e02-aaef2169ad6f",
            "compositeImage": {
                "id": "a9ed4800-b447-4b9a-a17d-bfa5efbc3682",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5305b147-f471-4a9b-98dd-937b53f7deef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a887975-4005-4792-aa43-25aee92bd86e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5305b147-f471-4a9b-98dd-937b53f7deef",
                    "LayerId": "62de22ee-e34d-4a1c-ae5c-d2aff3a51c1f"
                }
            ]
        },
        {
            "id": "4df2c495-3a72-42a6-bbb7-43cae8156e1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d67e5c9-a382-40bb-8e02-aaef2169ad6f",
            "compositeImage": {
                "id": "d72ea137-92db-4d87-9923-05a7815df494",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4df2c495-3a72-42a6-bbb7-43cae8156e1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8605e359-c47e-46ab-a98f-bb3b34de3032",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4df2c495-3a72-42a6-bbb7-43cae8156e1a",
                    "LayerId": "62de22ee-e34d-4a1c-ae5c-d2aff3a51c1f"
                }
            ]
        },
        {
            "id": "f36c747f-f777-4c44-8305-0f61593d9664",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d67e5c9-a382-40bb-8e02-aaef2169ad6f",
            "compositeImage": {
                "id": "cc952c59-cd6e-45d6-8e35-4d5eb782aeac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f36c747f-f777-4c44-8305-0f61593d9664",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "755081cf-1b99-4072-97c2-bf06d413793a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f36c747f-f777-4c44-8305-0f61593d9664",
                    "LayerId": "62de22ee-e34d-4a1c-ae5c-d2aff3a51c1f"
                }
            ]
        },
        {
            "id": "7d6370f6-bba6-4deb-9b28-8d2ab002fb87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d67e5c9-a382-40bb-8e02-aaef2169ad6f",
            "compositeImage": {
                "id": "b2c5f3ce-5bc8-4477-9be3-f0b60897c815",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d6370f6-bba6-4deb-9b28-8d2ab002fb87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11ee467c-10ec-4b00-bab8-c0b27a59a0b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d6370f6-bba6-4deb-9b28-8d2ab002fb87",
                    "LayerId": "62de22ee-e34d-4a1c-ae5c-d2aff3a51c1f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "62de22ee-e34d-4a1c-ae5c-d2aff3a51c1f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d67e5c9-a382-40bb-8e02-aaef2169ad6f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        1258225664,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}