{
    "id": "7e27890f-8c8d-4934-876a-1bc8f35e4e3f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTiles1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7642e18d-893c-4e5a-bf34-0ffe9d586f95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e27890f-8c8d-4934-876a-1bc8f35e4e3f",
            "compositeImage": {
                "id": "0d478267-48d9-44c5-977e-8a128b9c37fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7642e18d-893c-4e5a-bf34-0ffe9d586f95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ffa1ac5-3b93-45ec-8c4b-2d65d1d90a69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7642e18d-893c-4e5a-bf34-0ffe9d586f95",
                    "LayerId": "e1e60d5a-4a0c-43df-a05a-37c4bcafc797"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "e1e60d5a-4a0c-43df-a05a-37c4bcafc797",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7e27890f-8c8d-4934-876a-1bc8f35e4e3f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}