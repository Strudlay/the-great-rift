{
    "id": "8275ed57-92b7-40df-aee0-3b17e78f0d26",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStars3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 561,
    "bbox_left": 119,
    "bbox_right": 1062,
    "bbox_top": 46,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b18f6cda-2be6-4dc0-855b-231938b7337c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8275ed57-92b7-40df-aee0-3b17e78f0d26",
            "compositeImage": {
                "id": "2704a2cc-2ce5-478a-a115-2372e55b3933",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b18f6cda-2be6-4dc0-855b-231938b7337c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c1a78b3-63db-49a4-87b7-a1f6c5476673",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b18f6cda-2be6-4dc0-855b-231938b7337c",
                    "LayerId": "fa9786fd-376c-41ae-b3e0-2e8d2a7d85c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "fa9786fd-376c-41ae-b3e0-2e8d2a7d85c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8275ed57-92b7-40df-aee0-3b17e78f0d26",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 0,
    "yorig": 0
}