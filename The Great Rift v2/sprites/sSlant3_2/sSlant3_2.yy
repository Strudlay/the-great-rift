{
    "id": "00c53f80-2d11-4850-9821-79ae3d47c652",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlant3_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1432514c-fb70-460d-bd4a-36b0cbfca2e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00c53f80-2d11-4850-9821-79ae3d47c652",
            "compositeImage": {
                "id": "287edfed-38ee-4ff4-8778-ef6e3c816200",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1432514c-fb70-460d-bd4a-36b0cbfca2e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cf683c0-50f6-4a60-a7f8-2c35020a49e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1432514c-fb70-460d-bd4a-36b0cbfca2e5",
                    "LayerId": "1d97c379-277f-4467-9095-86c9e27b05e5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1d97c379-277f-4467-9095-86c9e27b05e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "00c53f80-2d11-4850-9821-79ae3d47c652",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}