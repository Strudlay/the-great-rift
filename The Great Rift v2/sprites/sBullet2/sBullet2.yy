{
    "id": "e00c498e-22d2-40c2-a327-7f19de1040f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBullet2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "54c25490-c019-4e08-9e1d-d7304005a438",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e00c498e-22d2-40c2-a327-7f19de1040f5",
            "compositeImage": {
                "id": "9cc1275e-f54a-45a2-a996-cc7cf60d7d6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54c25490-c019-4e08-9e1d-d7304005a438",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8decfda-09dc-471c-96ae-41af7b8f8dad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54c25490-c019-4e08-9e1d-d7304005a438",
                    "LayerId": "8c8d6207-e74d-47c1-9bb9-fc399ca32710"
                }
            ]
        },
        {
            "id": "fa181298-8023-439d-9a57-8f36e0e3736f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e00c498e-22d2-40c2-a327-7f19de1040f5",
            "compositeImage": {
                "id": "9a4325e0-d513-44f2-88a6-74907fc6e15b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa181298-8023-439d-9a57-8f36e0e3736f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d1b542a-f37f-407e-9409-65a717d226f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa181298-8023-439d-9a57-8f36e0e3736f",
                    "LayerId": "8c8d6207-e74d-47c1-9bb9-fc399ca32710"
                }
            ]
        },
        {
            "id": "48201836-476e-4939-a61b-f5a741abac32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e00c498e-22d2-40c2-a327-7f19de1040f5",
            "compositeImage": {
                "id": "eb5242a9-090d-49f9-9ffe-f85eb24ac454",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48201836-476e-4939-a61b-f5a741abac32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60a0de57-37fc-468d-9418-efb01f2661bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48201836-476e-4939-a61b-f5a741abac32",
                    "LayerId": "8c8d6207-e74d-47c1-9bb9-fc399ca32710"
                }
            ]
        },
        {
            "id": "77b83630-a4d9-4a38-9529-63b8329e3d50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e00c498e-22d2-40c2-a327-7f19de1040f5",
            "compositeImage": {
                "id": "1fd3be97-91c7-4c54-8f02-219b2ff3da9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77b83630-a4d9-4a38-9529-63b8329e3d50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a5a8b0d-19ce-49df-a9d9-49210081983e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77b83630-a4d9-4a38-9529-63b8329e3d50",
                    "LayerId": "8c8d6207-e74d-47c1-9bb9-fc399ca32710"
                }
            ]
        },
        {
            "id": "b661eb96-1317-4639-ad47-014b46a2d83a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e00c498e-22d2-40c2-a327-7f19de1040f5",
            "compositeImage": {
                "id": "18145e0e-5d6e-4a86-be28-54cade8ae72d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b661eb96-1317-4639-ad47-014b46a2d83a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3da7e443-ed6e-4b6c-9b24-f4396a5e7a43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b661eb96-1317-4639-ad47-014b46a2d83a",
                    "LayerId": "8c8d6207-e74d-47c1-9bb9-fc399ca32710"
                }
            ]
        },
        {
            "id": "926fee52-9d54-43df-8a8a-421b9dc5728a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e00c498e-22d2-40c2-a327-7f19de1040f5",
            "compositeImage": {
                "id": "81bdc114-3f5b-4329-8d07-d3a952e5fe8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "926fee52-9d54-43df-8a8a-421b9dc5728a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fb45afa-13c3-4480-9502-6e37f81be0f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "926fee52-9d54-43df-8a8a-421b9dc5728a",
                    "LayerId": "8c8d6207-e74d-47c1-9bb9-fc399ca32710"
                }
            ]
        },
        {
            "id": "72387e0a-7834-46e5-96d9-1cdea4724946",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e00c498e-22d2-40c2-a327-7f19de1040f5",
            "compositeImage": {
                "id": "023f3e33-247f-4b89-88d6-8bcfe88c2f58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72387e0a-7834-46e5-96d9-1cdea4724946",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ae30900-2857-4e9e-b09d-c4d82b35162f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72387e0a-7834-46e5-96d9-1cdea4724946",
                    "LayerId": "8c8d6207-e74d-47c1-9bb9-fc399ca32710"
                }
            ]
        },
        {
            "id": "f517ccad-dd59-407c-b07d-b7d0348101d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e00c498e-22d2-40c2-a327-7f19de1040f5",
            "compositeImage": {
                "id": "db57594a-cbfd-4c33-b664-c7e3a2b68cf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f517ccad-dd59-407c-b07d-b7d0348101d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a9ccbe5-cd74-448b-b424-db7b9985dfd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f517ccad-dd59-407c-b07d-b7d0348101d9",
                    "LayerId": "8c8d6207-e74d-47c1-9bb9-fc399ca32710"
                }
            ]
        },
        {
            "id": "60339122-ca8a-4a72-aef6-174ad2d62510",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e00c498e-22d2-40c2-a327-7f19de1040f5",
            "compositeImage": {
                "id": "fd68e106-f9e2-4a6c-9cae-354232daf409",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60339122-ca8a-4a72-aef6-174ad2d62510",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d08e836-39d3-4bdf-9d3c-2f4097f26aff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60339122-ca8a-4a72-aef6-174ad2d62510",
                    "LayerId": "8c8d6207-e74d-47c1-9bb9-fc399ca32710"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "8c8d6207-e74d-47c1-9bb9-fc399ca32710",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e00c498e-22d2-40c2-a327-7f19de1040f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 6
}