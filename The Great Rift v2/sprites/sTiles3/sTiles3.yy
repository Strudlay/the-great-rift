{
    "id": "70ff2084-2ea4-4026-9611-85b73c63bd12",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTiles3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 32,
    "bbox_right": 159,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "26845157-5bb8-49ea-a5d4-72e9e3b44b77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70ff2084-2ea4-4026-9611-85b73c63bd12",
            "compositeImage": {
                "id": "c2e7d61f-5716-405e-a86f-4b0344bf7582",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26845157-5bb8-49ea-a5d4-72e9e3b44b77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82cb63e8-14db-4856-a5e3-9d0c6bcd9894",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26845157-5bb8-49ea-a5d4-72e9e3b44b77",
                    "LayerId": "57edf6fc-d18b-4728-9395-426d733b5d04"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "57edf6fc-d18b-4728-9395-426d733b5d04",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70ff2084-2ea4-4026-9611-85b73c63bd12",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}