{
    "id": "d1eddba3-91c0-4915-94b3-a8613c1c0ffa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCorner2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "913680ea-4bba-49eb-8034-d8337b095742",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1eddba3-91c0-4915-94b3-a8613c1c0ffa",
            "compositeImage": {
                "id": "6c2ee87d-b9c3-4995-9a74-c65ec5db79c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "913680ea-4bba-49eb-8034-d8337b095742",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "469b6db1-d630-41b3-ac3f-b1b62eac521b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "913680ea-4bba-49eb-8034-d8337b095742",
                    "LayerId": "00c0456a-e278-4eb4-a416-7e4079dca4e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "00c0456a-e278-4eb4-a416-7e4079dca4e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1eddba3-91c0-4915-94b3-a8613c1c0ffa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}