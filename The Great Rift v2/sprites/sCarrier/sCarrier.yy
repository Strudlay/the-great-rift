{
    "id": "bad61e2d-02c2-4c80-a3c9-37452c12cc86",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCarrier",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 2,
    "bbox_right": 62,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ec5e2966-2eec-4e20-9902-02522352b408",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bad61e2d-02c2-4c80-a3c9-37452c12cc86",
            "compositeImage": {
                "id": "9d696913-5bdb-491a-a788-f609e18cdc69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec5e2966-2eec-4e20-9902-02522352b408",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cd446eb-f3b9-41dc-9ed1-43e36b7ebeed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec5e2966-2eec-4e20-9902-02522352b408",
                    "LayerId": "3142ae3e-9a96-42f2-9978-01b0d6cadb0e"
                }
            ]
        },
        {
            "id": "3d65c649-1f6c-4b2b-9286-1ba116747c04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bad61e2d-02c2-4c80-a3c9-37452c12cc86",
            "compositeImage": {
                "id": "efc7a5e9-8a2c-4fb4-81a9-5eaed1ab235b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d65c649-1f6c-4b2b-9286-1ba116747c04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de99e2d6-5787-4e04-8a8b-0fa5f9eb3622",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d65c649-1f6c-4b2b-9286-1ba116747c04",
                    "LayerId": "3142ae3e-9a96-42f2-9978-01b0d6cadb0e"
                }
            ]
        },
        {
            "id": "27b7d06f-30ca-475f-9aaa-24483b36c28b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bad61e2d-02c2-4c80-a3c9-37452c12cc86",
            "compositeImage": {
                "id": "5d78fa93-9e0f-4169-9c1b-1f51d1bc6d10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27b7d06f-30ca-475f-9aaa-24483b36c28b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9523bf7-6077-4a40-ac59-b36c8132f599",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27b7d06f-30ca-475f-9aaa-24483b36c28b",
                    "LayerId": "3142ae3e-9a96-42f2-9978-01b0d6cadb0e"
                }
            ]
        },
        {
            "id": "bd6007ee-b191-4e8c-9de1-ec539515fe13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bad61e2d-02c2-4c80-a3c9-37452c12cc86",
            "compositeImage": {
                "id": "4d4182e4-7871-42fe-bfb4-e4cad0f8237e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd6007ee-b191-4e8c-9de1-ec539515fe13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23da0700-83cc-466a-919e-4c50bee507e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd6007ee-b191-4e8c-9de1-ec539515fe13",
                    "LayerId": "3142ae3e-9a96-42f2-9978-01b0d6cadb0e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3142ae3e-9a96-42f2-9978-01b0d6cadb0e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bad61e2d-02c2-4c80-a3c9-37452c12cc86",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 18,
    "yorig": 32
}