{
    "id": "c4d4760a-a856-4407-bca9-abed7a26ea49",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlant2_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7455c824-eae6-4f97-85bd-d5ace664eb8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d4760a-a856-4407-bca9-abed7a26ea49",
            "compositeImage": {
                "id": "797b593a-4c17-41f5-bf83-8ea62a3d2163",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7455c824-eae6-4f97-85bd-d5ace664eb8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bd12177-fe93-4af3-90b5-067a3056144a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7455c824-eae6-4f97-85bd-d5ace664eb8f",
                    "LayerId": "eb2b152e-6bd5-4f98-899d-4bb736aecf5d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "eb2b152e-6bd5-4f98-899d-4bb736aecf5d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4d4760a-a856-4407-bca9-abed7a26ea49",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}