{
    "id": "882ee182-d4be-40af-a240-6e948aebbebe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTest",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "95fc6ace-b53d-41d0-8898-0bdff06596a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "882ee182-d4be-40af-a240-6e948aebbebe",
            "compositeImage": {
                "id": "453fa957-b6eb-4e4c-a52a-2c5e95cf4a1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95fc6ace-b53d-41d0-8898-0bdff06596a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df47bccc-192c-4d27-a79f-d3a650212700",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95fc6ace-b53d-41d0-8898-0bdff06596a2",
                    "LayerId": "8be962af-7380-48d7-b3b2-4f2dc5f2a478"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8be962af-7380-48d7-b3b2-4f2dc5f2a478",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "882ee182-d4be-40af-a240-6e948aebbebe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}