{
    "id": "db278b0e-7511-4c86-bd88-ec70743fffb9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCorner4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f4c65efe-277c-4b16-8f2b-19e6e71c77af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db278b0e-7511-4c86-bd88-ec70743fffb9",
            "compositeImage": {
                "id": "0e496883-e95a-42c1-bcd4-9a9650bbbf23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4c65efe-277c-4b16-8f2b-19e6e71c77af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fdd486d-9757-4ead-9782-ddf19c7c986f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4c65efe-277c-4b16-8f2b-19e6e71c77af",
                    "LayerId": "5071b464-17e3-45b5-98f7-c6a22341db1c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5071b464-17e3-45b5-98f7-c6a22341db1c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db278b0e-7511-4c86-bd88-ec70743fffb9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}