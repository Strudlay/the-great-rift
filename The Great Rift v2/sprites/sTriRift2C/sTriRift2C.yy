{
    "id": "3f7ec198-0cba-41af-af71-a2529dcd1b46",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTriRift2C",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 27,
    "bbox_right": 63,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4838ee5c-3714-42ac-b60e-8359f8bd27d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f7ec198-0cba-41af-af71-a2529dcd1b46",
            "compositeImage": {
                "id": "5d733740-bfaa-4f76-8c4a-c64dc7d06af9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4838ee5c-3714-42ac-b60e-8359f8bd27d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e38e55d-4cdc-4e2b-8fee-4d83c0f2080c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4838ee5c-3714-42ac-b60e-8359f8bd27d1",
                    "LayerId": "0ee1b45b-8552-4009-9852-a1420ab2195a"
                }
            ]
        },
        {
            "id": "83fb4ff9-235b-4758-86d4-dc82a04f7ad0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f7ec198-0cba-41af-af71-a2529dcd1b46",
            "compositeImage": {
                "id": "d1f08bd8-0c25-4439-9ac8-0fa438d9be46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83fb4ff9-235b-4758-86d4-dc82a04f7ad0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d6a6cfd-7d39-4b0d-88ab-7a72eb7c9f2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83fb4ff9-235b-4758-86d4-dc82a04f7ad0",
                    "LayerId": "0ee1b45b-8552-4009-9852-a1420ab2195a"
                }
            ]
        },
        {
            "id": "3613287a-757d-4be5-8907-93e7847099c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f7ec198-0cba-41af-af71-a2529dcd1b46",
            "compositeImage": {
                "id": "a4302501-74fc-43a8-9923-d643d460c41d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3613287a-757d-4be5-8907-93e7847099c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8ccede9-db4e-45de-8940-fe0669aa0f35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3613287a-757d-4be5-8907-93e7847099c3",
                    "LayerId": "0ee1b45b-8552-4009-9852-a1420ab2195a"
                }
            ]
        },
        {
            "id": "edf72d7a-6865-4cee-9289-70d1cbeb8aa2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f7ec198-0cba-41af-af71-a2529dcd1b46",
            "compositeImage": {
                "id": "4051073b-857f-4a7d-9b1e-2219aac0d576",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edf72d7a-6865-4cee-9289-70d1cbeb8aa2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13285cd6-1941-4561-b9cf-acd5e43341c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edf72d7a-6865-4cee-9289-70d1cbeb8aa2",
                    "LayerId": "0ee1b45b-8552-4009-9852-a1420ab2195a"
                }
            ]
        },
        {
            "id": "e0cf758e-7f20-4a84-88e0-52a102bd2f5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f7ec198-0cba-41af-af71-a2529dcd1b46",
            "compositeImage": {
                "id": "1b41592f-8074-47e5-ac24-675a5f897151",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0cf758e-7f20-4a84-88e0-52a102bd2f5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "459b1a49-3bf5-48e9-b567-18ca91897cad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0cf758e-7f20-4a84-88e0-52a102bd2f5f",
                    "LayerId": "0ee1b45b-8552-4009-9852-a1420ab2195a"
                }
            ]
        },
        {
            "id": "b21cbf39-85c7-45d4-a43d-7228db6ea60d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f7ec198-0cba-41af-af71-a2529dcd1b46",
            "compositeImage": {
                "id": "8e51e827-580d-4983-a3b0-0aef33c067a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b21cbf39-85c7-45d4-a43d-7228db6ea60d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1955e91-5299-4652-84b4-1e49ce6a3162",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b21cbf39-85c7-45d4-a43d-7228db6ea60d",
                    "LayerId": "0ee1b45b-8552-4009-9852-a1420ab2195a"
                }
            ]
        },
        {
            "id": "0281831c-d973-4029-9c4e-8ff5e23e479c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f7ec198-0cba-41af-af71-a2529dcd1b46",
            "compositeImage": {
                "id": "152daf40-a5ff-4fc4-9941-9d499c8cc141",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0281831c-d973-4029-9c4e-8ff5e23e479c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "121a7577-929f-4601-8304-0cf14cae40ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0281831c-d973-4029-9c4e-8ff5e23e479c",
                    "LayerId": "0ee1b45b-8552-4009-9852-a1420ab2195a"
                }
            ]
        },
        {
            "id": "5e5bd446-9ec8-4b20-8734-dc2252bc5413",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f7ec198-0cba-41af-af71-a2529dcd1b46",
            "compositeImage": {
                "id": "29d567c7-ddc1-4a27-97c4-5fdcc4ad7fab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e5bd446-9ec8-4b20-8734-dc2252bc5413",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fb370de-8d8a-4fbd-aaf9-cd3d4f251812",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e5bd446-9ec8-4b20-8734-dc2252bc5413",
                    "LayerId": "0ee1b45b-8552-4009-9852-a1420ab2195a"
                }
            ]
        },
        {
            "id": "0f14d009-7252-4089-ad34-9d3073eaee93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f7ec198-0cba-41af-af71-a2529dcd1b46",
            "compositeImage": {
                "id": "5e6a51e8-97b2-42f2-944b-97d99afea8a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f14d009-7252-4089-ad34-9d3073eaee93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7f7fdd2-547b-4958-b57b-c5c69c48fc9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f14d009-7252-4089-ad34-9d3073eaee93",
                    "LayerId": "0ee1b45b-8552-4009-9852-a1420ab2195a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0ee1b45b-8552-4009-9852-a1420ab2195a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f7ec198-0cba-41af-af71-a2529dcd1b46",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 56,
    "yorig": 46
}