{
    "id": "fbdebcd4-8b86-439e-aa18-9fa5686a7cbb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBackground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 606,
    "bbox_left": 57,
    "bbox_right": 1128,
    "bbox_top": 123,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a5142517-d1c9-4e0c-83dc-12f9090edca0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbdebcd4-8b86-439e-aa18-9fa5686a7cbb",
            "compositeImage": {
                "id": "0d164f48-3aa3-478a-95d3-5a404c6f0870",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5142517-d1c9-4e0c-83dc-12f9090edca0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b8810cd-626e-411d-b735-cdccf28779aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5142517-d1c9-4e0c-83dc-12f9090edca0",
                    "LayerId": "2fe709f7-48a4-407d-9356-f782e52b7601"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "2fe709f7-48a4-407d-9356-f782e52b7601",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fbdebcd4-8b86-439e-aa18-9fa5686a7cbb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 640,
    "yorig": 360
}