{
    "id": "c6142de7-dca8-40e2-8942-85935181cb18",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemy2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "625db629-aa77-4de9-a11a-2e8a415ea60f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6142de7-dca8-40e2-8942-85935181cb18",
            "compositeImage": {
                "id": "604d49b7-a199-41ba-ac41-f0d825bd747a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "625db629-aa77-4de9-a11a-2e8a415ea60f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33bb6677-6210-4a43-8888-e33883702131",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "625db629-aa77-4de9-a11a-2e8a415ea60f",
                    "LayerId": "2abf0fd8-9649-4104-b6ff-da7ed805062d"
                }
            ]
        },
        {
            "id": "3de220c0-7025-455d-a7d8-e550eaef19bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6142de7-dca8-40e2-8942-85935181cb18",
            "compositeImage": {
                "id": "1adf79dc-2c11-4813-8cb1-26829d5e8a9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3de220c0-7025-455d-a7d8-e550eaef19bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e31b8725-c447-49c2-82ba-5b691d06314e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3de220c0-7025-455d-a7d8-e550eaef19bf",
                    "LayerId": "2abf0fd8-9649-4104-b6ff-da7ed805062d"
                }
            ]
        },
        {
            "id": "e6611f3d-eae0-4325-8221-e2d5d2284383",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6142de7-dca8-40e2-8942-85935181cb18",
            "compositeImage": {
                "id": "0c5bb84d-ea43-4ddd-8e48-5c493d8e633b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6611f3d-eae0-4325-8221-e2d5d2284383",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a40d58ea-3378-4685-80e9-b4e22779007d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6611f3d-eae0-4325-8221-e2d5d2284383",
                    "LayerId": "2abf0fd8-9649-4104-b6ff-da7ed805062d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "2abf0fd8-9649-4104-b6ff-da7ed805062d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c6142de7-dca8-40e2-8942-85935181cb18",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 96
}