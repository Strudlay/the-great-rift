{
    "id": "fbb2fb38-d2d8-4785-b9bf-230262848623",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTri",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 3,
    "bbox_right": 48,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "015d36ce-baab-4e1e-9f9c-4f19312623e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbb2fb38-d2d8-4785-b9bf-230262848623",
            "compositeImage": {
                "id": "263408b0-9ab0-4c0a-b85e-d42a02cfcec3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "015d36ce-baab-4e1e-9f9c-4f19312623e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "432b1f38-b571-410d-aa21-acc5f73a22d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "015d36ce-baab-4e1e-9f9c-4f19312623e4",
                    "LayerId": "10d72e40-f2c1-4886-bab6-9d285fe6d82d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 53,
    "layers": [
        {
            "id": "10d72e40-f2c1-4886-bab6-9d285fe6d82d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fbb2fb38-d2d8-4785-b9bf-230262848623",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 52,
    "xorig": 26,
    "yorig": 26
}