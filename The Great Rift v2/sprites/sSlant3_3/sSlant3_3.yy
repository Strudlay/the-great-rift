{
    "id": "849a6fae-4af6-4935-af3a-272e030a2423",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlant3_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bfdd9774-6f73-4a31-8b5a-006d1b82002c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849a6fae-4af6-4935-af3a-272e030a2423",
            "compositeImage": {
                "id": "a13d3889-e49d-4f04-8cf9-9521412cddaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfdd9774-6f73-4a31-8b5a-006d1b82002c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1cc0423-3f01-40e6-b45b-be150fd3747c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfdd9774-6f73-4a31-8b5a-006d1b82002c",
                    "LayerId": "7dac8ece-cf3d-4873-a927-a9c93a5b9f0f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7dac8ece-cf3d-4873-a927-a9c93a5b9f0f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "849a6fae-4af6-4935-af3a-272e030a2423",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}