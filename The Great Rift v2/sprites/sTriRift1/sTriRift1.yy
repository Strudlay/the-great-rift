{
    "id": "46076e12-9dc6-403f-8827-5c0f58a89a14",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTriRift1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f888f4c5-5b68-49e9-accc-c054c43ed1a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46076e12-9dc6-403f-8827-5c0f58a89a14",
            "compositeImage": {
                "id": "503c3836-e939-4d00-a087-20c1a58176ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f888f4c5-5b68-49e9-accc-c054c43ed1a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c81efd02-5160-4044-96c4-7059a3625523",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f888f4c5-5b68-49e9-accc-c054c43ed1a5",
                    "LayerId": "5a8c10ce-d4d6-48c7-8c63-9323ba2bb6c1"
                }
            ]
        },
        {
            "id": "41912661-0b87-4a2f-8731-92d146e01199",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46076e12-9dc6-403f-8827-5c0f58a89a14",
            "compositeImage": {
                "id": "7e3e6a29-f690-4215-b655-25ea6f5d44ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41912661-0b87-4a2f-8731-92d146e01199",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd3305df-5656-48cf-95d7-ca4f384583d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41912661-0b87-4a2f-8731-92d146e01199",
                    "LayerId": "5a8c10ce-d4d6-48c7-8c63-9323ba2bb6c1"
                }
            ]
        },
        {
            "id": "7f1ecd69-294c-475f-9155-4f5a262bfe52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46076e12-9dc6-403f-8827-5c0f58a89a14",
            "compositeImage": {
                "id": "038c289c-78ee-4dbf-9711-f8e7d6bbd85f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f1ecd69-294c-475f-9155-4f5a262bfe52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4342d23-36d5-49a8-992e-56b4065525e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f1ecd69-294c-475f-9155-4f5a262bfe52",
                    "LayerId": "5a8c10ce-d4d6-48c7-8c63-9323ba2bb6c1"
                }
            ]
        },
        {
            "id": "b7b28da7-f338-4986-800a-fbcb3806b367",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46076e12-9dc6-403f-8827-5c0f58a89a14",
            "compositeImage": {
                "id": "3d0e9ccf-2b4e-4a74-bc02-c40bdb805490",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7b28da7-f338-4986-800a-fbcb3806b367",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "522e4489-e6e3-4cd6-b7f9-94f076af3652",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7b28da7-f338-4986-800a-fbcb3806b367",
                    "LayerId": "5a8c10ce-d4d6-48c7-8c63-9323ba2bb6c1"
                }
            ]
        },
        {
            "id": "bfa82bc0-74e5-4f68-a6e9-c4a533039867",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46076e12-9dc6-403f-8827-5c0f58a89a14",
            "compositeImage": {
                "id": "9241a4eb-7eee-4c20-ba68-113207beab62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfa82bc0-74e5-4f68-a6e9-c4a533039867",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3075604a-e4fb-47d2-83d1-c563d2d9dbb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfa82bc0-74e5-4f68-a6e9-c4a533039867",
                    "LayerId": "5a8c10ce-d4d6-48c7-8c63-9323ba2bb6c1"
                }
            ]
        },
        {
            "id": "aad90718-6ebc-42a9-96c5-ecc572e044b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46076e12-9dc6-403f-8827-5c0f58a89a14",
            "compositeImage": {
                "id": "219f98bf-c3a0-4d7e-b3bc-2203c49e5ed4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aad90718-6ebc-42a9-96c5-ecc572e044b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d95763c-f999-4f2e-8798-ef8589185426",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aad90718-6ebc-42a9-96c5-ecc572e044b1",
                    "LayerId": "5a8c10ce-d4d6-48c7-8c63-9323ba2bb6c1"
                }
            ]
        },
        {
            "id": "ab893630-12c3-47cf-bb57-8e840575f933",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46076e12-9dc6-403f-8827-5c0f58a89a14",
            "compositeImage": {
                "id": "85f20584-c4f8-4595-b82a-01a93b3daea2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab893630-12c3-47cf-bb57-8e840575f933",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de0b8af0-a233-4427-9764-9c9d1f00101c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab893630-12c3-47cf-bb57-8e840575f933",
                    "LayerId": "5a8c10ce-d4d6-48c7-8c63-9323ba2bb6c1"
                }
            ]
        },
        {
            "id": "0273ff5c-77ac-41b6-9e9b-c4771b619a37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46076e12-9dc6-403f-8827-5c0f58a89a14",
            "compositeImage": {
                "id": "0fba4b24-1033-44a7-b673-35c1a4d5dd27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0273ff5c-77ac-41b6-9e9b-c4771b619a37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ad913bd-cb21-4ab8-8a03-b039355ddebd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0273ff5c-77ac-41b6-9e9b-c4771b619a37",
                    "LayerId": "5a8c10ce-d4d6-48c7-8c63-9323ba2bb6c1"
                }
            ]
        },
        {
            "id": "583caea7-96ff-4dd1-81c1-73b0f6c9def1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46076e12-9dc6-403f-8827-5c0f58a89a14",
            "compositeImage": {
                "id": "6ac95873-6d33-4478-9a09-5eea4c6629c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "583caea7-96ff-4dd1-81c1-73b0f6c9def1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "311b3510-f4b9-4618-ba89-6754717515cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "583caea7-96ff-4dd1-81c1-73b0f6c9def1",
                    "LayerId": "5a8c10ce-d4d6-48c7-8c63-9323ba2bb6c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5a8c10ce-d4d6-48c7-8c63-9323ba2bb6c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "46076e12-9dc6-403f-8827-5c0f58a89a14",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}