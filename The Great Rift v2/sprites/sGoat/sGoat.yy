{
    "id": "8309806c-b0da-4b74-85f7-0b0c8fc9befa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGoat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3497cf1f-3e07-4064-9b2a-4051477f03d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8309806c-b0da-4b74-85f7-0b0c8fc9befa",
            "compositeImage": {
                "id": "a52895e2-bd55-4269-ae40-eabc7bf045da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3497cf1f-3e07-4064-9b2a-4051477f03d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a444c4a5-61a5-4571-a23c-ca5241302a5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3497cf1f-3e07-4064-9b2a-4051477f03d0",
                    "LayerId": "fd808e63-5d6f-43a1-950a-bbb69f8cf2d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "fd808e63-5d6f-43a1-950a-bbb69f8cf2d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8309806c-b0da-4b74-85f7-0b0c8fc9befa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}