{
    "id": "e3784e78-a698-4594-b9d7-d6c4ea9ade6b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 10,
    "bbox_right": 42,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2d9ac06-004c-447c-8446-b27a07f4f569",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3784e78-a698-4594-b9d7-d6c4ea9ade6b",
            "compositeImage": {
                "id": "063ab35c-5a30-42c4-80bc-7d5ac835529e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2d9ac06-004c-447c-8446-b27a07f4f569",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2aee90e3-3afa-4afd-981d-ad052e6303f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2d9ac06-004c-447c-8446-b27a07f4f569",
                    "LayerId": "b43de1e9-00f7-4ef9-b867-901607a8a852"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b43de1e9-00f7-4ef9-b867-901607a8a852",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3784e78-a698-4594-b9d7-d6c4ea9ade6b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}