{
    "id": "b9151ca0-6b65-4f9d-ae93-f5c2a6a06a92",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlant2_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e37bc036-b49a-4950-804c-b53c293819e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9151ca0-6b65-4f9d-ae93-f5c2a6a06a92",
            "compositeImage": {
                "id": "a70e932e-4aef-475b-b47b-cf70b793dd33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e37bc036-b49a-4950-804c-b53c293819e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d22047a-c960-45a0-b0ca-5977d5a82c95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e37bc036-b49a-4950-804c-b53c293819e1",
                    "LayerId": "7f79e12b-883a-476f-9853-e75728ddd22a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7f79e12b-883a-476f-9853-e75728ddd22a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9151ca0-6b65-4f9d-ae93-f5c2a6a06a92",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}