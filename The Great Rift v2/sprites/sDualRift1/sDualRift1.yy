{
    "id": "01ad4e1f-d2ec-4f02-9dfe-9990e6ceccb5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDualRift1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "62023683-84ad-4895-b2e5-90345c5bc8a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01ad4e1f-d2ec-4f02-9dfe-9990e6ceccb5",
            "compositeImage": {
                "id": "6604891a-0d4d-4122-b148-612e4050fc02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62023683-84ad-4895-b2e5-90345c5bc8a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdec9dff-36b0-426e-8866-660a2b4a10cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62023683-84ad-4895-b2e5-90345c5bc8a4",
                    "LayerId": "fdeed753-9302-4a5b-9a9e-070dfc7430f8"
                }
            ]
        },
        {
            "id": "4e3152c0-408e-4507-b512-75c1d1e1f9b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01ad4e1f-d2ec-4f02-9dfe-9990e6ceccb5",
            "compositeImage": {
                "id": "f8a787f2-84f8-4e4f-b4f4-92ec966b1d4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e3152c0-408e-4507-b512-75c1d1e1f9b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1eb90b78-533e-4b4b-b6da-3f7b69a71ee6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e3152c0-408e-4507-b512-75c1d1e1f9b3",
                    "LayerId": "fdeed753-9302-4a5b-9a9e-070dfc7430f8"
                }
            ]
        },
        {
            "id": "cc3b7d91-de17-4a88-9849-5ddafffd2a28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01ad4e1f-d2ec-4f02-9dfe-9990e6ceccb5",
            "compositeImage": {
                "id": "886401a9-6017-4e87-afc7-e7b5eb14d770",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc3b7d91-de17-4a88-9849-5ddafffd2a28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc99db35-1ef2-4396-b290-bf640f2338c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc3b7d91-de17-4a88-9849-5ddafffd2a28",
                    "LayerId": "fdeed753-9302-4a5b-9a9e-070dfc7430f8"
                }
            ]
        },
        {
            "id": "3b7a3017-08ab-4682-9c46-295600ed2288",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01ad4e1f-d2ec-4f02-9dfe-9990e6ceccb5",
            "compositeImage": {
                "id": "1e118fe0-24e7-446c-b461-d9eb1246d669",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b7a3017-08ab-4682-9c46-295600ed2288",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b64f46db-a0a5-4133-abcd-64b2beab021e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b7a3017-08ab-4682-9c46-295600ed2288",
                    "LayerId": "fdeed753-9302-4a5b-9a9e-070dfc7430f8"
                }
            ]
        },
        {
            "id": "920217b7-9584-4b9d-b1a8-9a75b5546ef1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01ad4e1f-d2ec-4f02-9dfe-9990e6ceccb5",
            "compositeImage": {
                "id": "e54e81a1-3ceb-4f39-abfa-3d37d7a1debd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "920217b7-9584-4b9d-b1a8-9a75b5546ef1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d6f0402-0f60-449b-b514-9ff3f8c3f68f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "920217b7-9584-4b9d-b1a8-9a75b5546ef1",
                    "LayerId": "fdeed753-9302-4a5b-9a9e-070dfc7430f8"
                }
            ]
        },
        {
            "id": "1361b04f-e94e-4b1c-8efd-c24f70e4546d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01ad4e1f-d2ec-4f02-9dfe-9990e6ceccb5",
            "compositeImage": {
                "id": "2c05b292-229e-4763-9c4a-328965de158e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1361b04f-e94e-4b1c-8efd-c24f70e4546d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "165524c2-a2e8-4326-b972-28b2aee5869a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1361b04f-e94e-4b1c-8efd-c24f70e4546d",
                    "LayerId": "fdeed753-9302-4a5b-9a9e-070dfc7430f8"
                }
            ]
        },
        {
            "id": "8509d69f-8573-4dbc-b044-539734d235ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01ad4e1f-d2ec-4f02-9dfe-9990e6ceccb5",
            "compositeImage": {
                "id": "12e0daa5-f4f1-4e39-a6f5-c2a9244d1150",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8509d69f-8573-4dbc-b044-539734d235ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "618ecba0-8678-4e23-8913-decb1bcbbe2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8509d69f-8573-4dbc-b044-539734d235ed",
                    "LayerId": "fdeed753-9302-4a5b-9a9e-070dfc7430f8"
                }
            ]
        },
        {
            "id": "e5cd4f78-a4ef-4b03-a2c1-02f0894e1775",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01ad4e1f-d2ec-4f02-9dfe-9990e6ceccb5",
            "compositeImage": {
                "id": "5929dd7c-fb5d-40ca-98d9-7ae0cdb94d5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5cd4f78-a4ef-4b03-a2c1-02f0894e1775",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35a3aef6-4100-4824-b7ff-058c19f83b7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5cd4f78-a4ef-4b03-a2c1-02f0894e1775",
                    "LayerId": "fdeed753-9302-4a5b-9a9e-070dfc7430f8"
                }
            ]
        },
        {
            "id": "d693c7f7-67b1-46f9-83a7-bd359cad98e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01ad4e1f-d2ec-4f02-9dfe-9990e6ceccb5",
            "compositeImage": {
                "id": "596f9789-6110-4867-9db4-48492220e52e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d693c7f7-67b1-46f9-83a7-bd359cad98e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70ad6fe4-1ffb-4a3b-ad15-1a3b311dab10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d693c7f7-67b1-46f9-83a7-bd359cad98e0",
                    "LayerId": "fdeed753-9302-4a5b-9a9e-070dfc7430f8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "fdeed753-9302-4a5b-9a9e-070dfc7430f8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "01ad4e1f-d2ec-4f02-9dfe-9990e6ceccb5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}