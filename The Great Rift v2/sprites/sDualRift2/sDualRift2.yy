{
    "id": "0f9bea5c-47ef-4d75-a487-5140c225776c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDualRift2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 27,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "beef618f-9c9b-4295-9964-589840619025",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f9bea5c-47ef-4d75-a487-5140c225776c",
            "compositeImage": {
                "id": "df4a0c4b-5834-4565-a31e-63fac00bd699",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "beef618f-9c9b-4295-9964-589840619025",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7340cb95-b361-4159-a87f-0965759f2c85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "beef618f-9c9b-4295-9964-589840619025",
                    "LayerId": "3749309a-bf43-4bfa-abec-ddf0843115b6"
                }
            ]
        },
        {
            "id": "55afecc2-6a92-4ef1-978a-f7b4db080b4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f9bea5c-47ef-4d75-a487-5140c225776c",
            "compositeImage": {
                "id": "1e331770-9849-4648-8e1d-7272996f573f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55afecc2-6a92-4ef1-978a-f7b4db080b4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "508adcbe-61d2-4cc6-b2bf-d12ce37880fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55afecc2-6a92-4ef1-978a-f7b4db080b4a",
                    "LayerId": "3749309a-bf43-4bfa-abec-ddf0843115b6"
                }
            ]
        },
        {
            "id": "bd0f7fda-b923-42c1-be22-2206511539fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f9bea5c-47ef-4d75-a487-5140c225776c",
            "compositeImage": {
                "id": "dd61fc34-8d19-438e-b08a-17de8c68d2c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd0f7fda-b923-42c1-be22-2206511539fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c65773a-e8ee-4ffc-bb48-a5d9d88c91aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd0f7fda-b923-42c1-be22-2206511539fd",
                    "LayerId": "3749309a-bf43-4bfa-abec-ddf0843115b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3749309a-bf43-4bfa-abec-ddf0843115b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f9bea5c-47ef-4d75-a487-5140c225776c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}