{
    "id": "e97f8f11-9cff-4183-a6fe-3204357ac13c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlant1_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9eb5b508-fbf7-429f-a4c8-1927934210a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e97f8f11-9cff-4183-a6fe-3204357ac13c",
            "compositeImage": {
                "id": "05ec80fb-d14f-456f-a6e9-7a6a81efe80a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9eb5b508-fbf7-429f-a4c8-1927934210a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d57f2253-b69f-48e6-bdd1-68b137a69c82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9eb5b508-fbf7-429f-a4c8-1927934210a4",
                    "LayerId": "7aad9517-3f31-4be6-a04d-446eb562107e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7aad9517-3f31-4be6-a04d-446eb562107e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e97f8f11-9cff-4183-a6fe-3204357ac13c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}