{
    "id": "8404c9bc-74de-408a-9606-e3cafdf3cc9a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlant2_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6be1047d-76b8-43fd-8f4c-78b3de91ef0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8404c9bc-74de-408a-9606-e3cafdf3cc9a",
            "compositeImage": {
                "id": "6aade3c2-3d4e-4e75-a6c9-fba1831eaee6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6be1047d-76b8-43fd-8f4c-78b3de91ef0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcf47522-6a02-44e6-96cf-6b472da7fda5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6be1047d-76b8-43fd-8f4c-78b3de91ef0b",
                    "LayerId": "0357fc79-81ca-4c83-9bcd-1cc53da36c2d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0357fc79-81ca-4c83-9bcd-1cc53da36c2d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8404c9bc-74de-408a-9606-e3cafdf3cc9a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}