{
    "id": "dc819419-4f63-4cad-8e36-3e878757404e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlant1_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1da7f240-4a18-4c3d-896f-0447ac0f8804",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc819419-4f63-4cad-8e36-3e878757404e",
            "compositeImage": {
                "id": "ac6d98e3-9165-4167-b85e-7e59de5c621e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1da7f240-4a18-4c3d-896f-0447ac0f8804",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c05f483c-a2ed-4e81-908b-bc4965f41b89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1da7f240-4a18-4c3d-896f-0447ac0f8804",
                    "LayerId": "5f9c6c54-59b3-4d92-a7cb-75065841fb76"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5f9c6c54-59b3-4d92-a7cb-75065841fb76",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc819419-4f63-4cad-8e36-3e878757404e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}