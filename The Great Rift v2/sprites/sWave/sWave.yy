{
    "id": "1b653792-2275-45fd-bcee-92bef311f213",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWave",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 119,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d11f315-2c4a-4d80-886b-1591126f232e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b653792-2275-45fd-bcee-92bef311f213",
            "compositeImage": {
                "id": "73ea7b4e-228b-4b2f-9281-62b900205874",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d11f315-2c4a-4d80-886b-1591126f232e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c90c88a2-0fe4-4ab1-8ddd-45ded45939ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d11f315-2c4a-4d80-886b-1591126f232e",
                    "LayerId": "352c6fa8-5c3c-4673-a561-913bb419baad"
                }
            ]
        },
        {
            "id": "1ba1028a-a9ca-4bfc-9ef3-96c020af616b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b653792-2275-45fd-bcee-92bef311f213",
            "compositeImage": {
                "id": "3a9c7054-db4b-4dce-baf8-53d7122c9571",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ba1028a-a9ca-4bfc-9ef3-96c020af616b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7350467b-83f1-4a76-baae-c4b4d65644a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ba1028a-a9ca-4bfc-9ef3-96c020af616b",
                    "LayerId": "352c6fa8-5c3c-4673-a561-913bb419baad"
                }
            ]
        },
        {
            "id": "4525c7b4-40b6-4f08-82e3-66fd06d82633",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b653792-2275-45fd-bcee-92bef311f213",
            "compositeImage": {
                "id": "028d8aff-ef9d-4079-9ed3-1bf4360a9a1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4525c7b4-40b6-4f08-82e3-66fd06d82633",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9285c49a-2583-47f4-b9fe-c3c3843ad58b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4525c7b4-40b6-4f08-82e3-66fd06d82633",
                    "LayerId": "352c6fa8-5c3c-4673-a561-913bb419baad"
                }
            ]
        },
        {
            "id": "34947b7b-c61d-4744-a545-9d59975e3515",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b653792-2275-45fd-bcee-92bef311f213",
            "compositeImage": {
                "id": "814f7e40-cc2b-4745-8705-410301c0da64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34947b7b-c61d-4744-a545-9d59975e3515",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0c254a4-823c-416e-bdad-f861a37dd17d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34947b7b-c61d-4744-a545-9d59975e3515",
                    "LayerId": "352c6fa8-5c3c-4673-a561-913bb419baad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "352c6fa8-5c3c-4673-a561-913bb419baad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b653792-2275-45fd-bcee-92bef311f213",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 0,
    "yorig": 1
}