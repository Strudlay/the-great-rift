{
    "id": "0f851766-0418-491d-8a0a-a126aab2bfc8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRift1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 4,
    "bbox_right": 60,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e7e3718e-ec53-40b5-b338-7b4d653c8353",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f851766-0418-491d-8a0a-a126aab2bfc8",
            "compositeImage": {
                "id": "e53d2afb-942c-4fc9-8ddb-32cd09381739",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7e3718e-ec53-40b5-b338-7b4d653c8353",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ee532d2-42c5-47b3-bb93-7d92ba922398",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7e3718e-ec53-40b5-b338-7b4d653c8353",
                    "LayerId": "276d795d-0705-4335-86f4-fb81f8523872"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "276d795d-0705-4335-86f4-fb81f8523872",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f851766-0418-491d-8a0a-a126aab2bfc8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}