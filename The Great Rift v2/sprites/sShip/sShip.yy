{
    "id": "17527f5c-a403-4a17-a719-fbbc29679ed1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShip",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 4,
    "bbox_right": 60,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d2b57304-8075-4edb-b35f-56b8222ac92d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17527f5c-a403-4a17-a719-fbbc29679ed1",
            "compositeImage": {
                "id": "00ef2f71-aaa5-4a49-8391-e28c4cc85bde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2b57304-8075-4edb-b35f-56b8222ac92d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcf168aa-019e-49e1-a9ad-b8f5a775e343",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2b57304-8075-4edb-b35f-56b8222ac92d",
                    "LayerId": "fd7c9561-2e89-4862-af78-41f447153c0a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "fd7c9561-2e89-4862-af78-41f447153c0a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "17527f5c-a403-4a17-a719-fbbc29679ed1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}