{
    "id": "b6361763-a251-4add-8c58-44d62a3201ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTiles2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 64,
    "bbox_right": 575,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5041fb28-6a24-4d1f-9aa4-715da9b302ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6361763-a251-4add-8c58-44d62a3201ea",
            "compositeImage": {
                "id": "63246a24-2df5-4622-ad79-07dc7c2b1dc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5041fb28-6a24-4d1f-9aa4-715da9b302ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34b8a823-9be4-4017-b001-7bc550fa0095",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5041fb28-6a24-4d1f-9aa4-715da9b302ab",
                    "LayerId": "be43db65-de8e-4485-a27e-40d392ae0e04"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 640,
    "layers": [
        {
            "id": "be43db65-de8e-4485-a27e-40d392ae0e04",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6361763-a251-4add-8c58-44d62a3201ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}