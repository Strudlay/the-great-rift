{
    "id": "b92a8c93-8bce-4bca-9268-7dad408ad42e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCooldown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 52,
    "bbox_left": 0,
    "bbox_right": 51,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "75e0cef7-1ec5-4d95-bc48-0d349f117377",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b92a8c93-8bce-4bca-9268-7dad408ad42e",
            "compositeImage": {
                "id": "3b900fcd-09d4-4bd8-92bb-67e1aab1e884",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75e0cef7-1ec5-4d95-bc48-0d349f117377",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "819c44ed-1502-4a27-8d66-85fb1edaf177",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75e0cef7-1ec5-4d95-bc48-0d349f117377",
                    "LayerId": "dfcab610-955f-41c7-b69f-a6965f509512"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 53,
    "layers": [
        {
            "id": "dfcab610-955f-41c7-b69f-a6965f509512",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b92a8c93-8bce-4bca-9268-7dad408ad42e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 52,
    "xorig": 26,
    "yorig": 26
}