{
    "id": "d93d5055-4811-43c6-8b7f-c226b7b9335c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMachine",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 5,
    "bbox_right": 52,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "234121ba-1baa-41e9-8785-4c2fbd42c309",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d93d5055-4811-43c6-8b7f-c226b7b9335c",
            "compositeImage": {
                "id": "b13960c4-71c7-493c-8ce3-bb6802a1a1f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "234121ba-1baa-41e9-8785-4c2fbd42c309",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf003bf9-2260-487a-b7d9-12e11ce5ad45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "234121ba-1baa-41e9-8785-4c2fbd42c309",
                    "LayerId": "2852c321-038e-46c2-8767-7d63551b17bf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2852c321-038e-46c2-8767-7d63551b17bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d93d5055-4811-43c6-8b7f-c226b7b9335c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 20,
    "yorig": 32
}