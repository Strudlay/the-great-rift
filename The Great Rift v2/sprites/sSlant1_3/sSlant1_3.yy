{
    "id": "460229d0-160a-43b8-8776-2fdcafaccedd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlant1_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19825933-98df-4ce4-bee6-b21e48cb5968",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460229d0-160a-43b8-8776-2fdcafaccedd",
            "compositeImage": {
                "id": "01b60574-ae04-42d3-8123-775feb6ef6f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19825933-98df-4ce4-bee6-b21e48cb5968",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6aa7f77b-e319-4259-b7b7-fa970cff655e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19825933-98df-4ce4-bee6-b21e48cb5968",
                    "LayerId": "5786787e-e5f1-45e3-bb5c-21e66eb60b2f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5786787e-e5f1-45e3-bb5c-21e66eb60b2f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "460229d0-160a-43b8-8776-2fdcafaccedd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}