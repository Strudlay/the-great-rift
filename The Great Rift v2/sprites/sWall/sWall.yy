{
    "id": "66fbb27e-3c33-4944-9402-e1504908dd35",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "10d3717e-4738-42d0-a3e1-54566a8b74ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66fbb27e-3c33-4944-9402-e1504908dd35",
            "compositeImage": {
                "id": "408e094c-0d6a-41d2-9755-b6d6bf104846",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10d3717e-4738-42d0-a3e1-54566a8b74ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad5436ec-fa7e-4d2a-8e0f-d53547753149",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10d3717e-4738-42d0-a3e1-54566a8b74ad",
                    "LayerId": "32267eb0-ead5-4cca-9085-8f3b51f8d930"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "32267eb0-ead5-4cca-9085-8f3b51f8d930",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66fbb27e-3c33-4944-9402-e1504908dd35",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}