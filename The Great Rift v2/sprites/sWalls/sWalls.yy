{
    "id": "9503f4b3-8d4e-4581-89ee-a4874f27e14e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWalls",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b762491-d85c-4639-bc86-5925ec23d363",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9503f4b3-8d4e-4581-89ee-a4874f27e14e",
            "compositeImage": {
                "id": "59e3e314-4fe6-447a-9e53-4532cce7fa39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b762491-d85c-4639-bc86-5925ec23d363",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06cd0c86-a4e0-4984-b83f-db67dab87e08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b762491-d85c-4639-bc86-5925ec23d363",
                    "LayerId": "c0d21bdf-1c6f-469b-8cd2-ef3cd373fcbd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c0d21bdf-1c6f-469b-8cd2-ef3cd373fcbd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9503f4b3-8d4e-4581-89ee-a4874f27e14e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}