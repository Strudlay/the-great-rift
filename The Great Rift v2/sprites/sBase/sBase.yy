{
    "id": "8252ec30-2c28-4c3a-a0f9-47143035474b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBase",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0d45d75d-a52e-4636-a131-012e34abade1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8252ec30-2c28-4c3a-a0f9-47143035474b",
            "compositeImage": {
                "id": "579bde2c-40d5-4785-8ef2-e28570adf07f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d45d75d-a52e-4636-a131-012e34abade1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f91fb777-4e7c-4095-912b-81486ec7b7c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d45d75d-a52e-4636-a131-012e34abade1",
                    "LayerId": "cdafd45c-a98c-4a42-a617-44e3d621de9a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "cdafd45c-a98c-4a42-a617-44e3d621de9a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8252ec30-2c28-4c3a-a0f9-47143035474b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 44
}