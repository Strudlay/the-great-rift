{
    "id": "0f273c80-e182-4ee0-84f6-493bff5d2524",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStars1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 696,
    "bbox_left": 57,
    "bbox_right": 1216,
    "bbox_top": 37,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2a5b66bf-1dcc-4020-a2da-b55ef2a26d1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f273c80-e182-4ee0-84f6-493bff5d2524",
            "compositeImage": {
                "id": "2e033f86-deaf-47bf-b7cb-2775197d7fb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a5b66bf-1dcc-4020-a2da-b55ef2a26d1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86b0c4cf-a4e3-4551-b64b-d4e3d2db0d72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a5b66bf-1dcc-4020-a2da-b55ef2a26d1b",
                    "LayerId": "816b648d-2aad-4a33-a977-5c921554330a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "816b648d-2aad-4a33-a977-5c921554330a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f273c80-e182-4ee0-84f6-493bff5d2524",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 0,
    "yorig": 0
}