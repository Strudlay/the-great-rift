{
    "id": "db181e63-4640-48cb-872a-e550627952f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sNodeA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 1,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0db12beb-1787-4c2d-87d1-80b3c52fad31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db181e63-4640-48cb-872a-e550627952f5",
            "compositeImage": {
                "id": "b5426b79-3b28-4bdd-bf36-4ab74e0b8d3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0db12beb-1787-4c2d-87d1-80b3c52fad31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ff75224-0fca-423e-9298-c21c2ecf613c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0db12beb-1787-4c2d-87d1-80b3c52fad31",
                    "LayerId": "26cdffa7-9e95-4b23-a9f6-b24f65b6f36d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "26cdffa7-9e95-4b23-a9f6-b24f65b6f36d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db181e63-4640-48cb-872a-e550627952f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 44,
    "yorig": 32
}