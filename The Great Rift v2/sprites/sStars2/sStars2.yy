{
    "id": "ed50b4a8-2ab4-4d58-94dc-a82f467f1468",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStars2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 692,
    "bbox_left": 81,
    "bbox_right": 1214,
    "bbox_top": 65,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d1df3443-e768-49a3-8c33-8782d489770d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed50b4a8-2ab4-4d58-94dc-a82f467f1468",
            "compositeImage": {
                "id": "a473541b-2f2e-439e-a6d1-9e09286c9d31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1df3443-e768-49a3-8c33-8782d489770d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1401aa7b-403c-4c46-9504-39ba10bce693",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1df3443-e768-49a3-8c33-8782d489770d",
                    "LayerId": "b8fe6164-cc68-4f6a-a641-38e8f46fb8b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "b8fe6164-cc68-4f6a-a641-38e8f46fb8b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed50b4a8-2ab4-4d58-94dc-a82f467f1468",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 0,
    "yorig": 0
}