{
    "id": "7a4a30d4-c56e-47a0-afce-0272016a3e3f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDual",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 3,
    "bbox_right": 48,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a1c2176-d7dc-4af0-b223-e6961b37b3b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a4a30d4-c56e-47a0-afce-0272016a3e3f",
            "compositeImage": {
                "id": "0a230853-daab-4966-8531-75788230883f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a1c2176-d7dc-4af0-b223-e6961b37b3b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13013b6a-205b-441f-aeda-5395746ab8f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a1c2176-d7dc-4af0-b223-e6961b37b3b0",
                    "LayerId": "f7d569b1-930d-493e-8493-09f81a934a55"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 53,
    "layers": [
        {
            "id": "f7d569b1-930d-493e-8493-09f81a934a55",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a4a30d4-c56e-47a0-afce-0272016a3e3f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 52,
    "xorig": 26,
    "yorig": 26
}