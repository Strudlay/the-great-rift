/// @desc Setup

/// Move towards ship if it is in range
if (distance_to_object(oGoat) < 500)
{
	// Sets the energy towards ship at a speed of 5 if x is higher than the ship
	if (x > oGoat.x)
	{
		move_towards_point(oGoat.x, oGoat.y, 6);
	}
	
	// // Sets the energy towards ship at a speed of 5 if x is higher than the ship.(To compensate for camera speed)
	else if (x < oGoat.x)
	{
		move_towards_point(oGoat.x, oGoat.y, 10);	
	}
	
	// Once within range energy gets absorbed and added to cooldown bar
	if (point_distance(x, y, oGoat.x, oGoat.y) < 5)
	{
		instance_destroy();
		// Checks to see if energy is low
		if (global.dual_energy != 1) && (sprite_index = sDualE)
		{
			// Adds energy
			global.dual_energy += global.energyvalue;
		}
		if (global.tri_energy != 1) && (sprite_index = sTriE)
		{
			// Adds energy
			global.tri_energy += global.energyvalue;
		}
		
	}
}