{
    "id": "87e35eca-69c5-451f-9f7e-29e1501df131",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnergy",
    "eventList": [
        {
            "id": "b46a70e3-63e4-47c4-b30b-52b2c45f039d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "87e35eca-69c5-451f-9f7e-29e1501df131"
        },
        {
            "id": "3fb5e2a8-3832-44ca-8dc2-1f72b2ed5bbd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "87e35eca-69c5-451f-9f7e-29e1501df131"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c5d89b89-bd54-49fe-97bb-d0ab4e7edd90",
    "visible": true
}