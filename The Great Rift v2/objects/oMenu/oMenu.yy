{
    "id": "adcae881-7c0b-40b5-9c52-4dd84ea21367",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMenu",
    "eventList": [
        {
            "id": "f7c6a7a5-a632-48ef-a33e-badcb78510ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "adcae881-7c0b-40b5-9c52-4dd84ea21367"
        },
        {
            "id": "410300cf-75e8-4f07-a835-d469d612f041",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "adcae881-7c0b-40b5-9c52-4dd84ea21367"
        },
        {
            "id": "b71ac9a8-7a1a-4ebb-b25f-66d93f465351",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "adcae881-7c0b-40b5-9c52-4dd84ea21367"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}