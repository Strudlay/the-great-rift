{
    "id": "0d545b51-afec-4d5e-9222-bac5f1fbf311",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSlant3_2",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5bc5cc86-acb2-4799-8f2e-256bb9793da6",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00c53f80-2d11-4850-9821-79ae3d47c652",
    "visible": true
}