/// @desc Draw Setup ------------------------------------------------------------------------------

// Draws self
draw_self();

// Creates flash animation when hit by bullets
if (flash > 0)
{
	flash--;
	shader_set(shWhite);
	draw_self();
	shader_reset();
}

/// Charge animation
if (instance_exists(oNodeA))
{
	alpha = instance_nearest(x, y, oNodeA);
	if (point_distance(x, y, alpha.x, alpha.y) < 500)
	{
		draw_line(x, y, alpha.x, alpha.y);
		if (collision_line(x, y, alpha.x, alpha.y, oGoat, false, false))
		{
			effect_create_above(ef_explosion, oRiftShip.x, oRiftShip.y, 0.6, c_red);
			effect_create_above(ef_explosion, oRiftShip.x, oRiftShip.y, 0.3, c_orange);
			effect_create_above(ef_explosion, oRiftShip.x, oRiftShip.y, 0.1, c_yellow);
			instance_destroy(oRiftShip);
		}
	}
}