/// @desc Setup

/// Set variables for ship
hsp = -2.5;									// Horizontal Speed
vsp = 0;									// Vertical Speed
firingdelay = 0;							// Firing delay for shooting
hp = 2;										// Health
flash = 0;									// Flash when hit
can_move = false;							// Sets movement to false
alpha = 0;									// Sets alpha_node to false

posy = y;
posx = x;
// Zig Zag motion
zigzag = 3;
alarm[0] = room_speed;