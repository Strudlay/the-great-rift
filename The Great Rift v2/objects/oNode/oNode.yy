{
    "id": "ec891f2a-d9c6-4dfe-8c4e-30733131a0d3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oNode",
    "eventList": [
        {
            "id": "0e2b8ab1-4595-439f-8b30-ad8d44449839",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ec891f2a-d9c6-4dfe-8c4e-30733131a0d3"
        },
        {
            "id": "e482d3a7-503d-4ec8-8916-8cca284d4c21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ec891f2a-d9c6-4dfe-8c4e-30733131a0d3"
        },
        {
            "id": "ead313f9-59f3-49cf-9d7a-b4cbb6f99b60",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ec891f2a-d9c6-4dfe-8c4e-30733131a0d3"
        },
        {
            "id": "626e6bad-1885-478c-9efc-c8a25ace6c78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "1b37dcc4-68bc-40b7-988a-695e8daa5c4f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ec891f2a-d9c6-4dfe-8c4e-30733131a0d3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f1e70b5b-ea5b-4af2-acb3-263d4d1def62",
    "visible": true
}