{
    "id": "95a17f87-4940-490b-bb7c-0d4f0ee5d060",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDualRift2",
    "eventList": [
        {
            "id": "d1ae2e32-9324-474b-9a1d-95dd487c04e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "95a17f87-4940-490b-bb7c-0d4f0ee5d060"
        },
        {
            "id": "05664035-c072-4b7a-8363-77402ad677ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "95a17f87-4940-490b-bb7c-0d4f0ee5d060"
        },
        {
            "id": "f10abab5-0ad9-470f-b6e3-e0d2413d973a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "95a17f87-4940-490b-bb7c-0d4f0ee5d060"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0f9bea5c-47ef-4d75-a487-5140c225776c",
    "visible": true
}