/// @desc Sets variables and parameters------------------------------------------------------------

// Variables
thruster_delay = 0;				// sets delay for thruster
barrier_trigger = false;		// sets barrier to false
controller = 1;					// sets game controller to true
controllerangle = 0;			// sets angle for controller aiming
firing_delay = 0;				// sets sets delay for guns
trigger =  false;
hp = 2;

// Creates scapegoat to follow ship for targeting, etc.
instance_create_layer(x, y, "Bullet", oGoat);