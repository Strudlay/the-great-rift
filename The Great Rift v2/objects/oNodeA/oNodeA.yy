{
    "id": "b2878631-8c1d-4bea-8a2d-56c62332c340",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oNodeA",
    "eventList": [
        {
            "id": "723e00e4-91cd-4bd0-a997-ec90cd2209c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b2878631-8c1d-4bea-8a2d-56c62332c340"
        },
        {
            "id": "85a6d88e-15e4-4f86-8dc2-8d05635ea100",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b2878631-8c1d-4bea-8a2d-56c62332c340"
        },
        {
            "id": "8f1c4f59-cae5-49d4-9361-89e04dd4f12d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b2878631-8c1d-4bea-8a2d-56c62332c340"
        },
        {
            "id": "39e58c3a-611d-4abb-a3a3-4e8eb9ee6842",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "1b37dcc4-68bc-40b7-988a-695e8daa5c4f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b2878631-8c1d-4bea-8a2d-56c62332c340"
        },
        {
            "id": "6c5cfb4a-a67a-4e2f-8861-ed6cdd8e4d70",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b2878631-8c1d-4bea-8a2d-56c62332c340"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "db181e63-4640-48cb-872a-e550627952f5",
    "visible": true
}