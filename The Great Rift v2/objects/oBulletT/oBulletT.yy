{
    "id": "709dfc9a-a0d7-4695-ad15-3c2b63013de1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBulletT",
    "eventList": [
        {
            "id": "917725db-3770-4f8f-a512-7ae582ab0ad1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5bc5cc86-acb2-4799-8f2e-256bb9793da6",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "709dfc9a-a0d7-4695-ad15-3c2b63013de1"
        },
        {
            "id": "2711e186-859a-4e47-8ef4-92f1e964e8f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "709dfc9a-a0d7-4695-ad15-3c2b63013de1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bb2871f4-fa81-40c9-99cf-939f930762b8",
    "visible": true
}