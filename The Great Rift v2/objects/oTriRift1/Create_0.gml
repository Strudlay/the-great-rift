/// @desc Sets variables and parameters -----------------------------------------------------------

/// Variables -------------------------------------------------------------------------------------

// Movement
hsp = 0;						// sets horizontal speed
vsp = 0;						// sets vertical speed
grav = 0.6;						// sets friction for vertical speed
spd = 8;						// sets speed to multiply by direction
thruster_delay = 0;				// sets delay for thruster
barrier_trigger = false;		// sets barrier to false

// Control
hascontrol = true;				// sets player input to true
controller = 0;					// sets game controller to true

// State
sDefault = sTriRift1;			// Sets default sprite
state = states.tri;				// default state set to tri rift
single = false;					// sets single to false
dual = false;					// sets dual to false
transform = false;				// sets transform to false

// Shooting
firing_delay = 0;				// sets sets delay for guns
can_shoot = true;				// allows player to shoot
laser_delay = 0;				// sets laser delay to 0
charge_count = 0;				// sets laser charge to 0

// Creates scapegoat to follow ship for targeting, etc.
instance_create_layer(x, y, "Bullet", oGoat);