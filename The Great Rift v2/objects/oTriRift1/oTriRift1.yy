{
    "id": "8f5f8686-e78e-483b-b99e-a9c4903b789d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTriRift1",
    "eventList": [
        {
            "id": "eaad229e-0e93-4541-8d58-dcfafb923b24",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8f5f8686-e78e-483b-b99e-a9c4903b789d"
        },
        {
            "id": "7f5751ea-53af-4c98-b7ac-830f48648249",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8f5f8686-e78e-483b-b99e-a9c4903b789d"
        },
        {
            "id": "e60dcbfd-b5bf-41ae-b64c-a8a110e88c69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "8f5f8686-e78e-483b-b99e-a9c4903b789d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "46076e12-9dc6-403f-8827-5c0f58a89a14",
    "visible": true
}