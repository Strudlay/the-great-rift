/// @desc Controls

// Allows player to go back to previous menu
if (keyboard_check_pressed(vk_escape))
{
	room_goto(rOptions);
}