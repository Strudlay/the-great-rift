{
    "id": "a3199e4e-4dad-4d47-9a92-92b72d807b3e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBarrier",
    "eventList": [
        {
            "id": "40c382c4-0ece-4c34-b8f9-e9241f97707d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a3199e4e-4dad-4d47-9a92-92b72d807b3e"
        },
        {
            "id": "b683c9cf-6c8b-47ac-ba05-2412f6a8f728",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a3199e4e-4dad-4d47-9a92-92b72d807b3e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ff71b15e-9a61-4c48-aff7-625fca208c82",
    "visible": false
}