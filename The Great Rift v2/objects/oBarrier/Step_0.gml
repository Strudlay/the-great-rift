/// @desc Update camera

// Update Destination
if (instance_exists(follow))
{
	xTo = follow.x;
	yTo = follow.y;
}

// Update object position
x += (xTo - x) - 4;
y += (yTo - y);

if(place_meeting(x, y, oTrigger))
{
	oCamera.hsp = 0;	
}