{
    "id": "1b37dcc4-68bc-40b7-988a-695e8daa5c4f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnemyTrig",
    "eventList": [
        {
            "id": "683ed19a-ac5e-44a9-b8b7-0d0b3a46bd29",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1b37dcc4-68bc-40b7-988a-695e8daa5c4f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1d22fa8e-6ceb-4917-931a-20e0d0d52b6e",
    "visible": true
}