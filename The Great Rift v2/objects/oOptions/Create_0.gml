/// @desc Menu Setup

// GUI Setup
gui_width = display_get_gui_width();
gui_height = display_get_gui_height();
gui_margin = 0;

// Variables Setup
menu_trigger = false;
menu_x = gui_width / 2; 
menu_y = gui_height / 2;
menu_speed = 30; // lower is faster
menu_font = fMenu;
menu_itemheight = font_get_size(fFont);
menu_committed = -1;
menu_control = false;

// Menu Layout
//menu[2] = "Gamepad Controls";
menu[1] = "Keyboard Controls";
menu[0] = "Back";

menu_items = array_length_1d(menu);
menu_cursor = 1;
