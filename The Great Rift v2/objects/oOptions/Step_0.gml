// Allows cursor to move up
if (keyboard_check_pressed(ord("W")))
{
	menu_cursor++;
	if (menu_cursor >= menu_items) menu_cursor = 0;
}
// Allows cursor to move up
if (keyboard_check_pressed(ord("S")))
{
	menu_cursor--;
	if (menu_cursor >= menu_items) menu_cursor = 0;
}

// Allows player to go back to previous menu
if (keyboard_check_pressed(vk_escape))
{
	room_goto(rMenu);
}

// Allows player to select highlighted option
if (keyboard_check_pressed(vk_enter))
{
	menu_committed = menu_cursor;
	menu_control = false;
}

if (menu_committed != -1)
{
	audio_stop_all();
	switch (menu_committed)
	{
		
		//case 2: default: room_goto(rGamepad); break;
		case 1: room_goto(rKeyboard); break;
		case 0: room_goto(rMenu); break;
	}
}
