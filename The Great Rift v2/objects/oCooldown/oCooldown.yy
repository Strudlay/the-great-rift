{
    "id": "aca3a5b7-c2a6-41f9-8e52-48adcc4a28b7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCooldown",
    "eventList": [
        {
            "id": "d332777d-81fb-4815-917b-1ba6eb87a8c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "aca3a5b7-c2a6-41f9-8e52-48adcc4a28b7"
        },
        {
            "id": "6dd851bb-8b86-4d88-9ff2-d0feb33b48a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "aca3a5b7-c2a6-41f9-8e52-48adcc4a28b7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b92a8c93-8bce-4bca-9268-7dad408ad42e",
    "visible": true
}