{
    "id": "e937d130-c05c-44c3-8f65-5f86ebe8970b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMachine",
    "eventList": [
        {
            "id": "c3782a23-52d2-4150-93da-5a1d564609ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e937d130-c05c-44c3-8f65-5f86ebe8970b"
        },
        {
            "id": "1ac54d60-5cba-45f7-85d5-fe9a4b1dbafb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e937d130-c05c-44c3-8f65-5f86ebe8970b"
        },
        {
            "id": "18b5cf09-b947-485d-ba20-69f9c1264c38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e937d130-c05c-44c3-8f65-5f86ebe8970b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d93d5055-4811-43c6-8b7f-c226b7b9335c",
    "visible": true
}