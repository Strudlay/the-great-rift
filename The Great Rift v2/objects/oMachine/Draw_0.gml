/// @desc Draw Setup ------------------------------------------------------------------------------

// Draws self
draw_self();

// Creates flash animation when hit by bullets\
var inst;
inst = instance_nearest(x, y, oMount);

if (inst.flash > 0)
{
	inst.flash--;
	shader_set(shWhite);
	draw_self();
	shader_reset();
}