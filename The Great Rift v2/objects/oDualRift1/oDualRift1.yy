{
    "id": "dcbe133b-1f11-4c46-b92c-ce023e3853bf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDualRift1",
    "eventList": [
        {
            "id": "e5473c91-9047-440f-b376-9412cc336941",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dcbe133b-1f11-4c46-b92c-ce023e3853bf"
        },
        {
            "id": "94b79847-0ea8-4284-b2c1-e0ef74d8a6f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dcbe133b-1f11-4c46-b92c-ce023e3853bf"
        },
        {
            "id": "637d1647-e918-4cdb-8db6-a95ca84a5bef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "dcbe133b-1f11-4c46-b92c-ce023e3853bf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "01ad4e1f-d2ec-4f02-9dfe-9990e6ceccb5",
    "visible": true
}