/// @desc Dual Rift Setup--------------------------------------------------------------------------

// Runs script for player controls
scrControls();

// Sets states for player's rift modes
switch (state)
{
	case states.single: scrSingle(); break;
	case states.dual: scrDual(); break;
	case states.tri: scrTri(); break;
	//case states.quad: scrQuad(); break;
	//case states.pent: scrPent(); break;
	//case states.hex: scrHex(); break
}

// Restricts ship from leaving screen
if (instance_exists(oDualRift2) && (oDualRift2.trigger))
{
	if (y > oDualRift2.y)
	{
		x = clamp(x, (oCamera.x - oCamera.view_w_half) + 32, (oCamera.x + oCamera.view_w_half) - 32);
		y = clamp(y, (oCamera.y - oCamera.view_h_half) + 282, camera_get_view_height(oCamera.cam) - 32);
	}
	if (y < oDualRift2.y)
	{
		x = clamp(x, (oCamera.x - oCamera.view_w_half) + 32, (oCamera.x + oCamera.view_w_half) - 32);
		y = clamp(y, (oCamera.y - oCamera.view_h_half) + 32, camera_get_view_height(oCamera.cam) - 282);
	}
}
else if (instance_exists(oDualRift2))
{
	if (y > oDualRift2.y)
	{
		x = clamp(x, (oCamera.x - oCamera.view_w_half) + 32, (oCamera.x + oCamera.view_w_half) - 32);
		y = clamp(y, (oCamera.y - oCamera.view_h_half) + 32, camera_get_view_height(oCamera.cam) - 32);
	}
	if (y < oDualRift2.y)
	{
		x = clamp(x, (oCamera.x - oCamera.view_w_half) + 32, (oCamera.x + oCamera.view_w_half) - 32);
		y = clamp(y, (oCamera.y - oCamera.view_h_half) + 32, camera_get_view_height(oCamera.cam) - 32);
	}
}

// Sets scapegoat to follow ship
if instance_exists(oGoat)
{
	var inst;
	inst = instance_nearest(x, y, oGoat);

	inst.x = x;
	inst.y = y;
}