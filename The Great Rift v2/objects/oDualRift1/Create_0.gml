/// @desc Sets variables and parameters -----------------------------------------------------------

/// Variables -------------------------------------------------------------------------------------

// Movement
hsp = 0;						// sets horizontal speed
vsp = 0;						// sets vertical speed
grav = 0.6;						// sets friction for vertical speed
spd = 8;						// sets speed to multiply by direction
thruster_delay = 0;				// sets delay for thruster
barrier_trigger = false;		// sets barrier to false

// Shooting
can_shoot = true;				// allows player to shoot
firing_delay = 0;				// sets sets delay for guns

// Control
hascontrol = true;				// sets player input to true
controller = 0;					// sets game controller to true

// State
state = states.dual;			// default state set to dual rift
transform = false;				// sets transforming animation to false;
single = false;					// sets single rift to false
tri = false;					// sets tri rift to false

// Creates scapegoat to follow ship for targeting, etc.
instance_create_layer(x, y, "Bullet", oGoat);