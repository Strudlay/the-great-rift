{
    "id": "4f776702-264d-4c4d-80f9-a7dd1e0ff55c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCarrier",
    "eventList": [
        {
            "id": "ff0f7617-1c04-4a10-994d-2849fb9fddb3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4f776702-264d-4c4d-80f9-a7dd1e0ff55c"
        },
        {
            "id": "39e6fde0-82ec-43b4-b320-d01cfca70fd7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4f776702-264d-4c4d-80f9-a7dd1e0ff55c"
        },
        {
            "id": "8d790176-93c2-40a2-abf1-223604300f71",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "4f776702-264d-4c4d-80f9-a7dd1e0ff55c"
        },
        {
            "id": "0d6ef901-d133-43d4-b3b5-52eda1378b86",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4f776702-264d-4c4d-80f9-a7dd1e0ff55c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bad61e2d-02c2-4c80-a3c9-37452c12cc86",
    "visible": true
}