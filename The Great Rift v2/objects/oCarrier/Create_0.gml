/// @desc Carrier Setup ---------------------------------------------------------------------------

/// @desc Setup

/// Set variables for ship
hsp = -4;									// Horizontal Speed
hp = 2;										// Health
flash = 0;									// Flash when hit
can_move = false;							// Sets movement to false

// Zig Zag motion
zigzag = random(3);
alarm[0] = random_range(50, 60);