{
    "id": "6ed60df6-6fc0-4a53-9355-a9d1de7697a5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCharge",
    "eventList": [
        {
            "id": "c4229192-9f3c-4cdf-9ac4-e989a080e9ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6ed60df6-6fc0-4a53-9355-a9d1de7697a5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f470a155-b075-4428-9e56-36a15ec15256",
    "visible": true
}