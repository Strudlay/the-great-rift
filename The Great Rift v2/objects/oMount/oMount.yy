{
    "id": "7d45d229-eafb-4ae5-95a3-cd347e96af33",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMount",
    "eventList": [
        {
            "id": "b1b2b99a-3883-4d6c-9169-7045ae170f2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "7d45d229-eafb-4ae5-95a3-cd347e96af33"
        },
        {
            "id": "b1f2867b-1fe5-4fee-8b1a-4f8d0fc5cc7a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7d45d229-eafb-4ae5-95a3-cd347e96af33"
        },
        {
            "id": "28c2626e-4a87-4d1e-952d-73112bda994f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7d45d229-eafb-4ae5-95a3-cd347e96af33"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1727cfba-cbcc-4032-b58c-98f0cab1ca5b",
    "visible": true
}