/// @desc Draw Setup ------------------------------------------------------------------------------

// Draws self
draw_self();

// Creates flash animation when hit by bullets
if (flash > 0)
{
	flash--;
	shader_set(shWhite);
	draw_self();
	shader_reset();
}