{
    "id": "441eb00d-b509-4328-be1e-dbdd924dfef7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oController",
    "eventList": [
        {
            "id": "3d762a5f-9df3-4f2f-adeb-833fb02a4903",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "441eb00d-b509-4328-be1e-dbdd924dfef7"
        },
        {
            "id": "30cf60d5-3f6d-4421-8a1b-e01f62c48f03",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "441eb00d-b509-4328-be1e-dbdd924dfef7"
        },
        {
            "id": "77d6ddbf-f5d8-4090-b1b6-08038ba9bc92",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "441eb00d-b509-4328-be1e-dbdd924dfef7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}