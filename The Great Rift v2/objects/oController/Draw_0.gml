/// @desc Draw score and lives

// Display score and lives
draw_set_color(c_white);
draw_set_font(fFont);
draw_text(oCamera.x - 764, oCamera.y - 532, "Score: " + string(score) + " / Lives: " + string(lives));
draw_text(oCamera.x, oCamera.y, "Energy Count: " + string(global.dual_energy));
draw_text(oCamera.x, oCamera.y + 50, "Energy Count: " + string(global.tri_energy));
// Testing coordinates
/*
if (instance_exists(oRiftShip))
{
draw_text(oCamera.x - 200, oCamera.y - 100, "y: " + string(oRiftShip.y));
}
if (instance_exists(oDualRift2))
{
draw_text(oCamera.x - 200, oCamera.y - 100, "y: " + string(oDualRift1.y));
draw_text(oCamera.x - 200, oCamera.y - 200, "y: " + string(oDualRift2.y));
draw_text(oCamera.x - 200, oCamera.y - 300, "y: " + string(oTest.y));
}

if (instance_exists(oTriRift1))
{
draw_text(oCamera.x - 200, oCamera.y - 100, "y Tri 1: " + string(oTriRift1.y));
draw_text(oCamera.x - 200, oCamera.y - 200, "y Tri 2: " + string(oTriRift2.y));
draw_text(oCamera.x - 200, oCamera.y - 300, "y Tri 3: " + string(oTriRift3.y));
draw_text(oCamera.x - 200, oCamera.y - 150, "x Tri 1: " + string(oTriRift1.x));
draw_text(oCamera.x - 200, oCamera.y - 250, "x Tri 2: " + string(oTriRift2.x));
draw_text(oCamera.x - 200, oCamera.y - 350, "x Tri 3: " + string(oTriRift3.x));
}