{
    "id": "1fcea3c6-5179-4f4c-978b-dfb6a682efe2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSlant2_4",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5bc5cc86-acb2-4799-8f2e-256bb9793da6",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "35286e51-22b7-4f95-956e-92be617842ed",
    "visible": true
}