{
    "id": "c763f505-0e81-49ea-8b06-87fe8a73d05a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oWave",
    "eventList": [
        {
            "id": "9bad6dfa-3fa1-426f-a01c-5a5cc8ed7267",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c763f505-0e81-49ea-8b06-87fe8a73d05a"
        },
        {
            "id": "b273e8a3-a5e8-44dc-b7c1-5ac3e452e32f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c763f505-0e81-49ea-8b06-87fe8a73d05a"
        },
        {
            "id": "ae741fb1-b8d3-4e50-82b5-2fdbf1f6afc0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "1b37dcc4-68bc-40b7-988a-695e8daa5c4f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c763f505-0e81-49ea-8b06-87fe8a73d05a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1b653792-2275-45fd-bcee-92bef311f213",
    "visible": true
}