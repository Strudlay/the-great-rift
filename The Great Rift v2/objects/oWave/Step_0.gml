/// @desc Tri Rift Setup -------------------------------------------------------------------------

// Collision with barrier
if(place_meeting(x, y, oBarrier))
{
	can_move = true;	
}

// If collision happens movement is true
if (can_move)
{
	// Horizontal Collision
	x += hsp;
}