{
    "id": "8eb3c7ef-9a74-41d2-adc2-953071f6b27a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLaserC",
    "eventList": [
        {
            "id": "e02890d3-d4bb-49e3-bca4-0564fdeedcdc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8eb3c7ef-9a74-41d2-adc2-953071f6b27a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8134a9cf-44c2-4b16-8db9-504f8e1bc2e5",
    "visible": true
}