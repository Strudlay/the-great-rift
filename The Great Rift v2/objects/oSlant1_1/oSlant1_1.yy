{
    "id": "39b30653-28d6-4c02-93a9-7e25cce6c789",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSlant1_1",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5bc5cc86-acb2-4799-8f2e-256bb9793da6",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e97f8f11-9cff-4183-a6fe-3204357ac13c",
    "visible": true
}