{
    "id": "47eafff6-42cd-4d64-ab81-3ff9ea3a7e52",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTriRift3",
    "eventList": [
        {
            "id": "630cdc4c-9d5b-45dd-83fc-c64c9b52f739",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "47eafff6-42cd-4d64-ab81-3ff9ea3a7e52"
        },
        {
            "id": "8c28b550-369d-4926-8b07-ef19458444be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "47eafff6-42cd-4d64-ab81-3ff9ea3a7e52"
        },
        {
            "id": "e3ff01ba-391a-41e6-b526-79823f74bf50",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "47eafff6-42cd-4d64-ab81-3ff9ea3a7e52"
        },
        {
            "id": "944ecb0c-ec09-43ff-b9ab-6f095936696b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "47eafff6-42cd-4d64-ab81-3ff9ea3a7e52"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "87fe82ce-ae61-4bcb-be58-ba759f72aadb",
    "visible": true
}