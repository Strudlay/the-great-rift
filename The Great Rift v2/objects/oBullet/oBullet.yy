{
    "id": "a318c9f0-5666-4635-bea0-c2ca948b4eac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBullet",
    "eventList": [
        {
            "id": "277aba6b-dcb8-44fa-8a16-56a2d0eacf6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "a318c9f0-5666-4635-bea0-c2ca948b4eac"
        },
        {
            "id": "a19bba26-af8d-4cd9-ae16-9cc980e08a7c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a318c9f0-5666-4635-bea0-c2ca948b4eac"
        },
        {
            "id": "32b4765b-3285-4f65-a5d5-3d43cd144dc0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ec891f2a-d9c6-4dfe-8c4e-30733131a0d3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a318c9f0-5666-4635-bea0-c2ca948b4eac"
        },
        {
            "id": "485c8e83-ef62-4b65-99f7-205b1d890f89",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b2878631-8c1d-4bea-8a2d-56c62332c340",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a318c9f0-5666-4635-bea0-c2ca948b4eac"
        },
        {
            "id": "5b4081c8-71cb-473d-a656-1fd3213b0229",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4f776702-264d-4c4d-80f9-a7dd1e0ff55c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a318c9f0-5666-4635-bea0-c2ca948b4eac"
        },
        {
            "id": "7393a960-57ce-4583-b6b1-026e6f237ce2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7d45d229-eafb-4ae5-95a3-cd347e96af33",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a318c9f0-5666-4635-bea0-c2ca948b4eac"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bb2871f4-fa81-40c9-99cf-939f930762b8",
    "visible": true
}