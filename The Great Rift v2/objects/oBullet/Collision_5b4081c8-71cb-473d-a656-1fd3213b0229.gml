/// @desc Carrier Collision

// With Node
with (other)
{
	// Decrease life and add flash effect
	hp--;	
	flash = 3;
}

// Destroy bullets
instance_destroy();

