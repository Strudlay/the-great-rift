{
    "id": "1f12a6a7-287c-4808-a9f6-9872b617d217",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTri",
    "eventList": [
        {
            "id": "63f0bbb2-c6f5-4531-a684-aed85a872c5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1f12a6a7-287c-4808-a9f6-9872b617d217"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fbb2fb38-d2d8-4785-b9bf-230262848623",
    "visible": true
}