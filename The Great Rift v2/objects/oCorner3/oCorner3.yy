{
    "id": "08d1953d-11ee-4f58-8251-a8c69a732f48",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCorner3",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5bc5cc86-acb2-4799-8f2e-256bb9793da6",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "aec4b71e-d79e-435b-887b-e040feb8adbf",
    "visible": true
}