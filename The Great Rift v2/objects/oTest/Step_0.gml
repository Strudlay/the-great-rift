/// Sets oTest properties for each rift

/// Dual Rift Setup -------------------------------------------------------------------------------
if (instance_exists(oDualRift2))
{
	// Sets coordinates to be with primary ship
	if instance_exists(oDualRift1)
	{
		if (oDualRift1.transform)
		{
			// Horizontal Speed and Position
			x = oDualRift1.x;

			// Vertical Speed and Position
			y = oDualRift1.y;
		
			// Restricts ship from leaving screen
			if (oDualRift1.y > y)
			{
				x = clamp(x, (oCamera.x - oCamera.view_w_half) + 32, (oCamera.x + oCamera.view_w_half) - 32);
				y = clamp(y, (oCamera.y - oCamera.view_h_half) + 32, camera_get_view_height(oCamera.cam) - 32);
			}
			if (oDualRift1.y < y)
			{
				x = clamp(x, (oCamera.x - oCamera.view_w_half) + 32, (oCamera.x + oCamera.view_w_half) - 32);
				y = clamp(y, (oCamera.y - oCamera.view_h_half) + 32, camera_get_view_height(oCamera.cam) - 32);
			}
		}
		else
		{
			// Horizontal Speed and Position
			x = x + (oDualRift1.hsp + oCamera.hsp);

			// Vertical Speed and Position
			y = y + oDualRift1.vsp;
		
			// Restricts ship from leaving screen
			if (oDualRift1.y > y)
			{
				x = clamp(x, (oCamera.x - oCamera.view_w_half) + 32, (oCamera.x + oCamera.view_w_half) - 32);
				y = clamp(y, (oCamera.y - oCamera.view_h_half) + 32, camera_get_view_height(oCamera.cam) - 282);
			}
			if (oDualRift1.y < y)
			{
				x = clamp(x, (oCamera.x - oCamera.view_w_half) + 32, (oCamera.x + oCamera.view_w_half) - 32);
				y = clamp(y, (oCamera.y - oCamera.view_h_half) + 282, camera_get_view_height(oCamera.cam) - 32);
			}
		}
	}
}
/// Tri-Rift Setup --------------------------------------------------------------------------------
if (instance_exists(oTriRift1))
{
	if (y > oTriRift1.y)
	{
		/// oTest Setup ---------------------------------------------------------------------------
		
		// Horizontal Speed and Position
		x = x + (oTriRift1.hsp + oCamera.hsp);

		// Vertical Speed and Position
		y = y + oTriRift1.vsp;	
	
		x = clamp(x, (oCamera.x - oCamera.view_w_half) + 16, (oCamera.x + oCamera.view_w_half) - 132);
		y = clamp(y, (oCamera.y - oCamera.view_h_half) + 176, camera_get_view_height(oCamera.cam) - 16);
		
		/// oTriRift2 Setup -----------------------------------------------------------------------
		
		// Horizontal Speed and Position
		oTriRift2.x += (oTriRift1.hsp + oCamera.hsp);

		// Vertical Speed and Position
		oTriRift2.y += oTriRift1.vsp;	
		
		oTriRift2.x = clamp(oTriRift2.x, (oCamera.x - oCamera.view_w_half) + 16, (oCamera.x + oCamera.view_w_half) - 132);
		oTriRift2.y = clamp(oTriRift2.y, (oCamera.y - oCamera.view_h_half) + 176, camera_get_view_height(oCamera.cam) - 16);
	}
	else if (y < oTriRift1.y)
	{
		/// oTest Setup ---------------------------------------------------------------------------
		
		// Horizontal Speed and Position
		x = x + (oTriRift1.hsp + oCamera.hsp);

		// Vertical Speed and Position
		y = y + oTriRift1.vsp;	
	
		x = clamp(x, (oCamera.x - oCamera.view_w_half) + 16, (oCamera.x + oCamera.view_w_half) - 132);
		y = clamp(y, (oCamera.y - oCamera.view_h_half) + 16, camera_get_view_height(oCamera.cam) - 176);	
		
		/// oTriRift2 Setup -----------------------------------------------------------------------
		
		// Horizontal Speed and Position
		oTriRift3.x = oTriRift3.x + (oTriRift1.hsp + oCamera.hsp);

		// Vertical Speed and Position
		oTriRift3.y = oTriRift3.y + oTriRift1.vsp;	
	
		oTriRift3.x = clamp(oTriRift3.x, (oCamera.x - oCamera.view_w_half) + 16, (oCamera.x + oCamera.view_w_half) - 132);
		oTriRift3.y = clamp(oTriRift3.y, (oCamera.y - oCamera.view_h_half) + 16, camera_get_view_height(oCamera.cam) - 176);
	}
}