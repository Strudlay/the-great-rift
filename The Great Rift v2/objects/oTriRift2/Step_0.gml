/// @desc Tri Rift Setup -------------------------------------------------------------------------

// Detaching animation
if (oTest)
{
	var inst;
	inst = instance_nearest(x, y + 200, oTest);
	if(point_distance(x, y, inst.x, inst.y) > 3)
	{
		move_towards_point(inst.x, inst.y, 5);
	}
	else
	{
		speed = 0;
		trigger = true;
	}
}
/// Shooting Setup --------------------------------------------------------------------------------

if (oTriRift1.can_shoot)
{
	// Shooting synced with main ship
	if (oTriRift1.firing_delay = 25) && (!oTriRift1.key_secondary)
	{
		with (instance_create_layer(x - 15, y, "Bullet", oLaser))
		{
			speed = 25;
		}

	}
	// Charged laser attack
	if (oTriRift1.key_secondary) && (!oTriRift1.key_primary)
	{
		image_angle = point_direction(x, y, oTriRift1.x, oTriRift1.y);
		sprite_index = sTriRift2C;
		if (instance_number(oCharge) <= 1)
		{
			with (instance_create_layer(x, y, "Bullet", oCharge))
			{
				direction = oTriRift2.image_angle;
				image_angle = direction;
			}
		}
	}
	// Reposition ships when not charging
	else
	{
		instance_destroy(oCharge);
		image_angle = point_direction(x, y, oTriRift2.x + 5, oTriRift2.y);
		sprite_index = sTriRift2;
	}
}

