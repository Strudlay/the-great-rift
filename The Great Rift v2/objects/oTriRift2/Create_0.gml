/// @desc Sets variables and parameters------------------------------------------------------------

// Variables
thruster_delay = 0;				// sets delay for thruster
barrier_trigger = false;		// sets barrier to false
controller = 1;					// sets game controller to true
controllerangle = 0;			// sets angle for controller aiming
trigger = false;				// sets trigger for transforming to false
inst = 0;