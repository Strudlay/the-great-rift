{
    "id": "e7ece6cc-74b6-4d38-891a-7c146189a0bb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTriRift2",
    "eventList": [
        {
            "id": "56ab2f57-31ae-4368-b711-41ea9a8dec4d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e7ece6cc-74b6-4d38-891a-7c146189a0bb"
        },
        {
            "id": "25f79665-8c99-4322-aeea-45546a167bc1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e7ece6cc-74b6-4d38-891a-7c146189a0bb"
        },
        {
            "id": "d03675ce-392d-43cc-8864-8835fd631546",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "e7ece6cc-74b6-4d38-891a-7c146189a0bb"
        },
        {
            "id": "fb198add-403e-40ef-becb-44031ccdcbc1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e7ece6cc-74b6-4d38-891a-7c146189a0bb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e0d4aec3-76cc-4ac9-bc9b-aa51636bcd28",
    "visible": true
}