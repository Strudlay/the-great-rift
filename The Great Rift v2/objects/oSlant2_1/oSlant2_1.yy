{
    "id": "10a319e1-d12c-420a-8ead-245cdf27be1c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSlant2_1",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5bc5cc86-acb2-4799-8f2e-256bb9793da6",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c4d4760a-a856-4407-bca9-abed7a26ea49",
    "visible": true
}