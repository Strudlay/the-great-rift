/// @desc Sets variables and parameters -----------------------------------------------------------

// Variables
hsp = 0;						// sets horizontal speed
vsp = 0;						// sets vertical speed
grav = 0.6;						// sets friction for vertical speed
spd = 8;						// sets speed to multiply by direction
firing_delay = 0;				// sets sets delay for guns
thruster_delay = 0;				// sets delay for thruster
hascontrol = true;				// sets player input to true
can_shoot = true;				// allows player to shoot
barrier_trigger = false;		// sets barrier to false
state = states.single;			// default state set to single rift
controller = 0;					// sets game controller to true
can_transform = false;			// sets transform to false

// Creates scapegoat to follow ship for targeting, etc.
instance_create_layer(x, y, "Bullet", oGoat);