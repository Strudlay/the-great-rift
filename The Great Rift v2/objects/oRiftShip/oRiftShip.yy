{
    "id": "05eb0fc3-a551-4eaf-81b4-b67a139efecc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oRiftShip",
    "eventList": [
        {
            "id": "6686907a-cb60-47be-a8f5-386afabe5356",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "05eb0fc3-a551-4eaf-81b4-b67a139efecc"
        },
        {
            "id": "0fad5b48-1ec4-45fa-95bd-0c7ad819499d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "05eb0fc3-a551-4eaf-81b4-b67a139efecc"
        },
        {
            "id": "726af7a4-1dc1-4d29-b22d-e77f3c33f05b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "05eb0fc3-a551-4eaf-81b4-b67a139efecc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1e9e160d-5b73-42c7-9f7e-794bf730e77f",
    "visible": true
}