/// @desc Dual Rift Setup -------------------------------------------------------------------------

// Detaching animation
if(point_distance(x, y, oTest.x, oTest.y) > 3)
{
	move_towards_point(oTest.x, oTest.y, 7);
}
else
{
	speed = 0;
	trigger = true;
}
 
//Re-attaching animation
if instance_exists(oDualRift1)
{
	if (!trigger) || (oDualRift1.transform)
	{	
		// Horizontal Speed and Position
		x = x + (oDualRift1.hsp + oCamera.hsp);

		// Vertical Speed and Position
		y = y + oDualRift1.vsp;
	
		// Restricts ship from leaving screen
		if (oDualRift1.y > y)
		{
			x = clamp(x, (oCamera.x - oCamera.view_w_half) + 32, (oCamera.x + oCamera.view_w_half) - 32);
			y = clamp(y, (oCamera.y - oCamera.view_h_half) + 32, camera_get_view_height(oCamera.cam) - 32);
		}
		if (oDualRift1.y < y)
		{
			x = clamp(x, (oCamera.x - oCamera.view_w_half) + 32, (oCamera.x + oCamera.view_w_half) - 32);
			y = clamp(y, (oCamera.y - oCamera.view_h_half) + 32, camera_get_view_height(oCamera.cam) - 32);
		}
	}
	else
	{
		// Horizontal Speed and Position
		x = x + (oDualRift1.hsp + oCamera.hsp);

		// Vertical Speed and Position
		y = y + oDualRift1.vsp;
		
		// Restricts ship from leaving screen
		if (oDualRift1.y > y)
		{
			x = clamp(x, (oCamera.x - oCamera.view_w_half) + 32, (oCamera.x + oCamera.view_w_half) - 32);
			y = clamp(y, (oCamera.y - oCamera.view_h_half) + 32, camera_get_view_height(oCamera.cam) - 282);
		}
		if (oDualRift1.y < y)
		{
			x = clamp(x, (oCamera.x - oCamera.view_w_half) + 32, (oCamera.x + oCamera.view_w_half) - 32);
			y = clamp(y, (oCamera.y - oCamera.view_h_half) + 282, camera_get_view_height(oCamera.cam) - 32);
		}
	}
}
/// Shooting Setup --------------------------------------------------------------------------------

if instance_exists(oDualRift1)
{
	// Setup image angle for ship
	if (oDualRift1.controller = 0)
	{
		image_angle = point_direction(x, y, mouse_x, mouse_y);
	}
	else if (oDualRift1.controller = 1)
	{
		var controllerh = gamepad_axis_value(0, gp_axisrh);
		var controllerv = gamepad_axis_value(0, gp_axisrv);
		if (abs(controllerh) > 0.2) || (abs(controllerv) > 0.2)
		{
			controllerangle = point_direction(0, 0, controllerh, controllerv);
		}
		image_angle = controllerangle;
	}

	// Shooting synced with main ship. Realigns bullets every 90 degrees
	if (oDualRift1.can_shoot)
	{
		if (oDualRift1.firing_delay = 20)
		{
			if (image_angle > 45) && (image_angle < 135)
			{
				with (instance_create_layer(x - 14, y, "Bullet", oBullet))
				{
					speed = 25;
					direction = oDualRift2.image_angle;
					image_angle = direction;
				}
				with (instance_create_layer(x + 14, y, "Bullet", oBullet))
				{
					speed = 25;
					direction = oDualRift2.image_angle;
					image_angle = direction;
				}
			}
			else if (image_angle > 225) && (image_angle < 315)
			{
				with (instance_create_layer(x - 14, y, "Bullet", oBullet))
				{
					speed = 25;
					direction = oDualRift2.image_angle;
					image_angle = direction;
				}
				with (instance_create_layer(x + 14, y, "Bullet", oBullet))
				{
					speed = 25;
					direction = oDualRift2.image_angle;
					image_angle = direction;
				}
			}
			else
			{
				with (instance_create_layer(x, y - 14, "Bullet", oBullet))
				{
					speed = 25;
					direction = oDualRift2.image_angle;
					image_angle = direction;
				}
				with (instance_create_layer(x, y + 14, "Bullet", oBullet))
				{
					speed = 25;
					direction = oDualRift2.image_angle;
					image_angle = direction;
				}
			}
		}
	}
}

// Sets scapegoat to follow ship
var inst;
inst = instance_nearest(x, y, oGoat);

inst.x = x;
inst.y = y;