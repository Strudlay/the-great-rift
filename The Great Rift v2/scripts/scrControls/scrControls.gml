/// @desc Ship Setup

/// Keyboard setup --------------------------------------------------------------------------------
if (hascontrol)
{
	// Controls
	key_left = keyboard_check(ord("A"));
	key_right = keyboard_check(ord("D"));
	key_up = keyboard_check(ord("W"));
	key_down = keyboard_check(ord("S")); 
	key_primary = mouse_check_button(mb_left);
	key_secondary = mouse_check_button(mb_right);
	key_single = keyboard_check_pressed(ord("1"));
	key_dual = keyboard_check_pressed(ord("2"));
	key_tri = keyboard_check_pressed(ord("3"));
	key_quad = keyboard_check_pressed(ord("4"));


	// If keyboard is being used, controller is inactive
	if (key_left) || (key_right) || (key_up) || (key_down)
	{
		controller = 0;	
	}

	/// Gamepad setup --------------------------------------------------------------------------------

	// Horizontal movement setup
	if (abs(gamepad_axis_value(0, gp_axislh)) > 0.2)
	{
		key_left = abs(min(gamepad_axis_value(0, gp_axislh), 0));
		key_right = max(gamepad_axis_value(0, gp_axislh), 0);
		controller = 1;
	}

	// Vertical movement setup
	if (abs(gamepad_axis_value(0, gp_axislv)) > 0.2)
	{
		key_up = abs(min(gamepad_axis_value(0, gp_axislv), 0));
		key_down = max(gamepad_axis_value(0, gp_axislv), 0);
		controller = 1;
	}

	// Primary Shooting setup
	if (gamepad_button_check(0, gp_shoulderrb))
	{
		key_primary = 1;
		controller = 1;	
	}
	
	// Secondary Shooting setup
	if (gamepad_button_check(0, gp_shoulderlb))
	{
		key_secondary = 1;
		controller = 1;	
	}

	// Single-rift setup
	if (gamepad_button_check_pressed(0, gp_face1))
	{
		key_single = 1;
		controller = 1;
	}

	// Dual-rift setup
	if (gamepad_button_check_pressed(0, gp_face2))
	{
		key_dual = 1;
		controller = 1;
	}
	
	// Tri-rift setup
	if (gamepad_button_check_pressed(0, gp_face3))
	{
		key_tri = 1;
		controller = 1;
	}
}
else
{
	key_right = 0;
	key_left = 0;
	key_jump = 0;
	key_up = 0;
	key_down = 0;
}

/// Player movement -------------------------------------------------------------------------------

// Determine movement
var hmove = key_right - key_left;
var vmove = key_down - key_up;
hsp = hmove * spd;
vsp = vmove * spd;

x = x + (hsp + oCamera.hsp);

y = y + vsp;

// Creates thruster effect behind player's ship
thruster_delay--;

if (thruster_delay < 0) && (key_right) || (key_up) || (key_down)
{
	thruster_delay = 2;
	effect_create_above(ef_spark, x - 20, y, 0.05, merge_colour(c_green, c_yellow, random(1)));
}

/// Collisions ------------------------------------------------------------------------------------

// Wall Collisions
if(place_meeting(x, y, oWall))
{
	instance_destroy();
	effect_create_above(ef_explosion, x, y, 0.6, c_red);
	effect_create_above(ef_explosion, x, y, 0.3, c_orange);
	effect_create_above(ef_explosion, x, y, 0.1, c_yellow);
}

// Node Collisions
if(place_meeting(x, y, oNode))
{
	instance_destroy();
	effect_create_above(ef_explosion, x, y, 0.6, c_red);
	effect_create_above(ef_explosion, x, y, 0.3, c_orange);
	effect_create_above(ef_explosion, x, y, 0.1, c_yellow);
}

// Carrier Collisions
if(place_meeting(x, y, oCarrier))
{
	instance_destroy();
	effect_create_above(ef_explosion, x, y, 0.6, c_red);
	effect_create_above(ef_explosion, x, y, 0.3, c_orange);
	effect_create_above(ef_explosion, x, y, 0.1, c_yellow);
}

// Turret Collisions
if(place_meeting(x, y, oMount))
{
	instance_destroy();
	effect_create_above(ef_explosion, x, y, 0.6, c_red);
	effect_create_above(ef_explosion, x, y, 0.3, c_orange);
	effect_create_above(ef_explosion, x, y, 0.1, c_yellow);
}

// Turret Bullet Collisions
if(place_meeting(x, y, oBulletT))
{
	instance_destroy();
	effect_create_above(ef_explosion, x, y, 0.6, c_red);
	effect_create_above(ef_explosion, x, y, 0.3, c_orange);
	effect_create_above(ef_explosion, x, y, 0.1, c_yellow);
}