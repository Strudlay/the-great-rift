/// Carrier Script --------------------------------------------------------------------------------


/// Movement --------------------------------------------------------------------------------------

// Collision with barrier
if(place_meeting(x, y, oBarrier))
{
	can_move = true;	
}

// If collision happens movement is true
if (can_move)
{
	y += zigzag;
	x += hsp;
}

/// Health ----------------------------------------------------------------------------------------
if (hp = 0)
{
	effect_create_above(ef_explosion, x, y, 0.6, c_red);
	effect_create_above(ef_explosion, x, y, 0.3, c_orange);
	effect_create_above(ef_explosion, x, y, 0.1, c_yellow);
	instance_destroy();
	instance_create_depth(x, y, "Player", oEnergy);
	score += 50;
	instance_create_depth(x, y, "Player", oEnergy);
}
