/// Separate Rifts --------------------------------------------------------------------------------


/// Single-Rift Setup -----------------------------------------------------------------------------

// Swaps to Single-Rift
if (key_single)
{
	// Top test block
	var inst1;
	inst1 = instance_nearest(x, y - 200, oTest);
	inst1.x = x + 16;
	inst1.y = y - 16;
	
	// Bottom test block
	var inst2;
	inst2 = instance_nearest(x, y + 200, oTest);
	inst2.x = x + 16;
	inst2.y = y + 16;
	single = true;
}

// Reattaching animation
if (single)
{
	if(point_distance(oTest.x, oTest.y, oTriRift2.x, oTriRift2.y) < 5)
	{
		instance_destroy(oGoat);
		can_shoot = false;
		instance_destroy(oCharge);
		instance_change(oRiftShip, true);
		instance_destroy(oTest);
		instance_destroy(oTriRift2);
		instance_destroy(oTriRift3);
	}	
}

/// Dual-Rift Setup -------------------------------------------------------------------------------
/*
// Swaps to Dual-Rift
if (key_dual)
{
	// Set nearest oTest to variable
	var inst1;
	inst1 = instance_nearest(x, y - 200, oTest);
	var inst2;
	inst2 = instance_nearest(x, y + 200, oTest);
	dual = true;
	
	// Spawn Dual-Rift based on Tri-Rift's location
	if (oTriRift1.y < (room_height / 2))
	{
		can_shoot = false;
		
		// Top test block

		inst1.x = x + 16;
		inst1.y = y + 234;
		
		// Bottom test block
		inst2.x = x + 16;
		inst2.y = y + 266;
	}
	if (oTriRift1.y > (room_height / 2))
	{
		can_shoot = false;
		
		// Top test block
		inst1.x = x + 16;
		inst1.y = y - 234;
		
		// Bottom test block
		inst2.x = x + 16;
		inst2.y = y - 266;
	}
}
if (dual)
{
	// Set nearest oTest to variable
	var inst1;
	inst1 = instance_nearest(oTriRift3.x, oTriRift3.y - 200, oTest);
	var inst2;
	inst2 = instance_nearest(oTriRift3.x, oTriRift3.y + 200, oTest);
	
	// Checks to see if Tri-Rift pieces collide with oTests
	if(point_distance(inst1.x, inst1.y, oTriRift3.x, oTriRift3.y) < 10) && (point_distance(inst2.x, inst2.y, oTriRift2.x, oTriRift2.y) < 10)
	{
		can_shoot = false;
		instance_destroy(oCharge);
		instance_destroy(oTest);
		instance_destroy(oTriRift2);
		instance_destroy(oTriRift3);
		
		// Spawns Dual-Rift according to location
		if (oTriRift1.y > (room_height / 2))
		{
			can_shoot = false;
			instance_change(oDualRift1, true);
			instance_create_layer(x, y - 250, "Player", oTest);
			instance_create_layer(oTest.x, oTest.y, "Player", oDualRift2);
		}
		if (oTriRift1.y < (room_height / 2))
		{
			can_shoot = false;
			instance_change(oDualRift1, true);
			instance_create_layer(x, y + 250, "Player", oTest);
			instance_create_layer(oTest.x, oTest.y, "Player", oDualRift2);
		}

	}	
}
*/
/// Shooting setup --------------------------------------------------------------------------------

// Shoot Bullets from guns on player's ship
if (can_shoot)
{
	// Primary attack
	firing_delay = firing_delay - 1;
	if (key_primary) && (firing_delay < 0) && (!key_secondary)
	{
		firing_delay = 25;
		with (instance_create_layer(x - 5, y - 1, "Bullet", oLaser))
		{
			speed = 25;	
		}
	}
	else 
	{
		key_primary = false;
	}
	// Charged laser attack
	if (key_secondary) && (!key_primary)
	{
		charge_count++;
		sprite_index = sTriRift1C;
		if (charge_count = 120)
		{
			with (instance_create_layer(x - 32, y, "Bullet", oLaserC))
			{
				speed = 25;
			}
		}
	}
	else
	{
		key_secondary = false;
		charge_count = 0;
		sprite_index = sTriRift1;
	}
}