/// Turret Setup ----------------------------------------------------------------------------------

/// Sets up turret to aim and shoot ship if within range

// Checks to see if ship is in range
var inst; 
inst = instance_nearest(x, y, oGoat);

if (distance_to_object(inst) < 600)
{
	// Aims at ship
	image_angle = point_direction(x, y, inst.x, inst.y);
	
	// Shooting
	
	firing_delay = firing_delay - 1;
	
	// Once firing_delay is less than 0, the turret will shoot
	if (image_angle > 0) && (image_angle < 180)
	{
		if (firing_delay < 0)
		{
			firing_delay = 30;
			with (instance_create_layer(x, y, "Bullet", oBulletT))
			{
				image_angle = point_direction(x, y, inst.x, inst.y);
				direction = image_angle;
				speed = 10;	
			}
		}
	}
}

/// Health ----------------------------------------------------------------------------------------
var inst = instance_nearest(x, y, oMount);

if (inst.hp = 0)
{
	effect_create_above(ef_explosion, x, y, 0.6, c_red);
	effect_create_above(ef_explosion, x, y, 0.3, c_orange);
	effect_create_above(ef_explosion, x, y, 0.1, c_yellow);
	instance_destroy();
	instance_destroy(inst);
	score += 50;
	instance_create_depth(x, y, "Player", oEnergy);
}


