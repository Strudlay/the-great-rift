/// Dual-Rift Script ------------------------------------------------------------------------------

/// Separate Rifts --------------------------------------------------------------------------------

/// Single-Rift Setup -----------------------------------------------------------------------------

// Swaps to Single Rift
if (key_single)
{
	single = true;
	transform = true;
	oTest.x = x;
	oTest.y = y;
}

// Reattaching animation
if (single)
{
	if(point_distance(oTest.x, oTest.y, oDualRift2.x, oDualRift2.y) < 10)
	{
		instance_destroy(oGoat);
		can_shoot = false;
		instance_change(oRiftShip, true);
		instance_destroy(oTest);
		instance_destroy(oDualRift2);
	}	
}
/*
/// Tri-Rift Setup --------------------------------------------------------------------------------

// Swaps to Tri-Rift
if (key_tri)
{
	// Top test block
	var inst1;
	inst1 = instance_nearest(x, y, oTest);
	inst1.x = x - 100;
	inst1.y = y;
	tri = true;
}

// Reattaching animation
if (tri)
{
	if(point_distance(oTest.x, oTest.y, oDualRift2.x, oDualRift2.y) < 5)
	{
		can_shoot = false;
		instance_change(oTriRift1, true);
		instance_destroy(oDualRift2);
		instance_destroy(oTest);
		instance_create_layer(oTriRift1.x - 100, oTriRift1.y + 80, "Player", oTest);
		instance_create_layer(oTriRift1.x - 100, oTriRift1.y - 80, "Player", oTest);
	
		instance_create_layer(oTriRift1.x - 100, oTriRift1.y + 16, "Player", oTriRift2);
		instance_create_layer(oTriRift1.x - 100, oTriRift1.y - 16, "Player", oTriRift3);
	}	
}
*/
/// Shooting Setup --------------------------------------------------------------------------------

// Shoot Bullets from guns on player's ship
if (can_shoot)
{
	firing_delay = firing_delay - 1;

	if (key_primary) && (firing_delay < 0)
	{
		firing_delay = 20;
		with (instance_create_layer(x - 15, y, "Bullet", oBullet))
		{
			speed = 25;	
		}
	
	}
}

/// Collision Setup -------------------------------------------------------------------------------
if (!instance_exists(oDualRift1))
{
	with (oDualRift2)
	{
		var inst;
		inst = instance_nearest(x, y, oGoat);
		instance_destroy(oGoat);
		instance_destroy();
		effect_create_above(ef_explosion, x, y, 0.6, c_red);
		effect_create_above(ef_explosion, x, y, 0.3, c_orange);
		effect_create_above(ef_explosion, x, y, 0.1, c_yellow);	
	}
}