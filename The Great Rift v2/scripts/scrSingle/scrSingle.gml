/// Single Rift Setup -----------------------------------------------------------------------------

/// Separate Rifts --------------------------------------------------------------------------------

// Swaps to Dual Rift if energy is full
if (key_dual) && (global.dual_energy >= 1)
{
	global.dual_energy = 0;
	instance_destroy(oGoat);
	
	if (oRiftShip.y > (room_height / 2))
	{
		can_shoot = false;
		instance_change(oDualRift1, true);
		instance_create_layer(x, y, "Player", oDualRift2);
		instance_create_layer(x, y - 250, "Bullet", oTest);
	}
	if (oRiftShip.y < (room_height / 2))
	{
		can_shoot = false;
		instance_change(oDualRift1, true);
		instance_create_layer(x, y, "Player", oDualRift2);
		instance_create_layer(x, y + 250, "Bullet", oTest);
	}
}

// Swaps to Tri-Rift
if (key_tri) && (global.tri_energy >= 1)
{
	global.tri_energy = 0;
	instance_destroy(oGoat);
	
	can_shoot = false;
	instance_change(oTriRift1, true);
	instance_create_layer(oTriRift1.x, oTriRift1.y, "Player", oTriRift2);
	instance_create_layer(oTriRift1.x, oTriRift1.y, "Player", oTriRift3);
	instance_create_layer(oTriRift1.x - 100, oTriRift1.y + 80, "Bullet", oTest);
	instance_create_layer(oTriRift1.x - 100, oTriRift1.y - 80, "Bullet", oTest);
}

/// Single Rift Setup -----------------------------------------------------------------------------

// Shoot Bullets from guns on player's ship
if (can_shoot)
{
	firing_delay = firing_delay - 1;

	if (key_primary) && (firing_delay < 0)
	{
		firing_delay = 15;
		with (instance_create_layer(x - 15, y, "Bullet", oBullet))
		{
			speed = 25;	
		}

	}
}