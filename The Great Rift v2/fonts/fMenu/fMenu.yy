{
    "id": "7551732d-bd1f-4e90-949b-49d48ba1c083",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fMenu",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Kredit",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "60a83914-de73-417c-9f6f-7f5740126aba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 41,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e539d949-ae73-4f8d-8c1b-1a4decb0044a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 41,
                "offset": 7,
                "shift": 21,
                "w": 7,
                "x": 191,
                "y": 131
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "59e9584f-a749-4d7c-bc2d-0068d137bb7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 2,
                "y": 88
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "a1eff71c-71b7-443e-bc2a-53b6c18897c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 154,
                "y": 88
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "8b74d91b-9ff0-42fb-8c7c-d299378004fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 135,
                "y": 88
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "8dc57e36-ec2e-4d8e-b246-524f6ab042da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 458,
                "y": 45
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "121aeff7-e07f-4a79-b2b0-56295bb5914a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 420,
                "y": 45
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "9519eed1-34f6-4838-9b9e-c7730e464c4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 41,
                "offset": 6,
                "shift": 21,
                "w": 9,
                "x": 149,
                "y": 131
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "0407a25d-a2a4-4c80-8d05-4fc669565213",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 41,
                "offset": 7,
                "shift": 21,
                "w": 9,
                "x": 160,
                "y": 131
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "a4b9d5b8-1948-4c5b-87fd-7f713537d1d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 41,
                "offset": 5,
                "shift": 21,
                "w": 10,
                "x": 137,
                "y": 131
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "c71d929c-86b9-4e39-afa9-b2dcea02feec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 41,
                "offset": 3,
                "shift": 21,
                "w": 16,
                "x": 172,
                "y": 88
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "dfb78fb4-6d0b-49af-abf4-09f0157af0c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 382,
                "y": 45
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "46cb5da1-9625-468c-9fc7-a9b5f4cf3ebf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 41,
                "offset": 5,
                "shift": 21,
                "w": 11,
                "x": 124,
                "y": 131
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "b8f76c87-5077-4dbf-8bf6-cc7e594bd9dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 208,
                "y": 88
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "7ce64432-2dfd-4453-963c-a951e16f7dc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 41,
                "offset": 6,
                "shift": 21,
                "w": 8,
                "x": 181,
                "y": 131
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "1185b90f-fab2-4ed5-8809-e4f513d3592f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 41,
                "offset": 3,
                "shift": 21,
                "w": 16,
                "x": 226,
                "y": 88
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "5bc5cb2d-26ee-4bbb-8e9d-a0c7f6a9a35b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 344,
                "y": 45
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "4660505a-b818-4bb1-9a1a-deade9af2529",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 244,
                "y": 88
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "b59df1f4-15eb-478e-bf57-076b2ef66df7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 325,
                "y": 45
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d6787e6c-d6ca-4672-8d79-8c5154a36edb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 306,
                "y": 45
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "032b1b80-7989-46c3-99be-9ac2038fe8ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 41,
                "offset": 3,
                "shift": 21,
                "w": 15,
                "x": 20,
                "y": 131
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "6c94686b-255a-4c3f-bedf-dc572ce02970",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 41,
                "offset": 3,
                "shift": 21,
                "w": 16,
                "x": 298,
                "y": 88
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "c5b856d8-0dba-41b0-9a63-8076bde1ded8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 439,
                "y": 45
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "9905813d-d47e-4d15-bec5-b8175e4b1143",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 287,
                "y": 45
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "838780a9-f340-41f7-bca7-e7c9edf65f01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 334,
                "y": 88
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "ae74d291-303d-4ed9-a8ac-7d7f0a682a32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 363,
                "y": 45
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "42591b9f-c593-43d8-9908-58258b5bbfaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 41,
                "offset": 6,
                "shift": 21,
                "w": 8,
                "x": 171,
                "y": 131
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "985f81a9-9c8d-4084-b799-f006fb2f1209",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 41,
                "offset": 5,
                "shift": 21,
                "w": 11,
                "x": 111,
                "y": 131
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "69922c6e-8dd3-4627-b66b-7c903a84f248",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 401,
                "y": 45
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "caa5401e-18dd-45c9-ab21-75ae1e7c558a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 2,
                "y": 131
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "a9f12239-dccc-4189-bff2-1cab19bb33f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 41,
                "offset": 3,
                "shift": 21,
                "w": 16,
                "x": 424,
                "y": 88
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "1cc29bd4-33af-448f-88d3-7649839ba559",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 41,
                "offset": 3,
                "shift": 21,
                "w": 16,
                "x": 406,
                "y": 88
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "b47d3c1d-2c30-4d13-88c9-4cb9f9572dbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 21,
                "y": 88
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "e935b459-cf28-4755-b962-c917d7973d2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 40,
                "y": 88
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "7220a25d-b611-4a70-9ea0-f25af89c0211",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 59,
                "y": 88
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "7bfd9804-16dc-46cb-b019-d90a8382c889",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 78,
                "y": 88
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "35066965-6a12-4ab9-ad6d-6c78701f5a2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 97,
                "y": 88
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "ce9e6c6a-b65a-4857-ae2a-0873e4bfd58a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 116,
                "y": 88
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "615e6398-7ae5-4d23-a3cb-8d27052981db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 316,
                "y": 88
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "9ea19bc1-63e5-47dd-ae1e-c94749bdc842",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 477,
                "y": 45
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "26058e49-3bf2-4654-acc5-032228289c5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 230,
                "y": 45
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "35bb6844-81ba-48f3-b6a9-a8c1d0cd1308",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 357,
                "y": 2
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "68528f0e-c7e7-41e1-a69d-4b4c2f1a183f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 41,
                "offset": 4,
                "shift": 21,
                "w": 14,
                "x": 53,
                "y": 131
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "e1243974-cd93-4241-b9b7-ff174b5090de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 319,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "dec20321-9701-4aa6-8b69-b4fff094f3fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 300,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "f9828035-2c92-4c64-a329-45e97d7c369e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 388,
                "y": 88
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "608d808c-a1f0-4895-b46c-6b5dc45bdcca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 281,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "30bf0019-deff-4b12-a6c6-38e15365ddf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 249,
                "y": 45
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "401b7766-2bab-47ae-a02e-a1c7ffceca74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 243,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "cbe6f588-9ee4-4bf9-8549-37473a229119",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "890940ca-450a-439f-8b49-79ed0bde1da8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 205,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d7f2255d-fd0d-4a59-b201-275680c1a9cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 442,
                "y": 88
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "8f03abe6-9319-4190-8361-3a6d57934f7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 460,
                "y": 88
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "8deadbbc-33b8-431c-a8b1-5df5ded31ee1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 338,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "3f895fcd-277d-4d5a-ade1-1e01c2da97f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 478,
                "y": 88
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "fea49c79-508b-444b-b13a-98f2fc936685",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 186,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "fad4bd00-72d5-423b-95c3-cc5e52e8e1ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 41,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 148,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "35b31839-c24b-4ca1-a61c-4ff86bc10eb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "20fd688a-fe1c-46a8-b891-c086c9702423",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "d633a10a-0e83-411e-ba16-b55e8bb5619b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 41,
                "offset": 6,
                "shift": 21,
                "w": 13,
                "x": 69,
                "y": 131
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "1eb17445-dfd4-458a-89ba-1acaee13a66c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 41,
                "offset": 3,
                "shift": 21,
                "w": 16,
                "x": 370,
                "y": 88
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "39f1f61c-1793-4534-bad1-f9a3eb431839",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 12,
                "x": 84,
                "y": 131
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "d8ed4254-13fa-4b6d-89c2-84d3e49c9b6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 41,
                "offset": 3,
                "shift": 21,
                "w": 16,
                "x": 352,
                "y": 88
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "b23de40a-135a-4e13-ae9b-41e9b7611c95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 41,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "68ed8a65-8e14-4ba5-b87d-ab833681f33c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 41,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 98,
                "y": 131
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "1cdfae09-9acc-422a-8461-c162e3bcd5ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "666a764c-e375-40dc-a749-714b33e4801a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "dbaabdac-8b42-4425-9e25-d3995291e971",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "efd0a99d-0d5c-4398-93e7-e54a98d24ce7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "3b5c4ce3-454d-46c9-a6d0-fab5252c8581",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 376,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "1116f2fc-f9c3-47b7-9f82-a86796c5c1fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 40,
                "y": 45
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "d475aa66-72c1-40e7-9321-74c8554cb832",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 395,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "5b0e79b7-433e-4498-be7a-4298e6a422b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 192,
                "y": 45
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "ca550dea-9220-4d52-b284-9ce112aa7c6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 173,
                "y": 45
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "42c5e916-494b-4441-9667-7bede5cc2a3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 41,
                "offset": 4,
                "shift": 21,
                "w": 14,
                "x": 37,
                "y": 131
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "41210056-1718-4a11-86b1-7726ec81756b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 154,
                "y": 45
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "821255f5-d39b-426a-b2fc-e3431f0f28ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 135,
                "y": 45
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "2b0715e0-3e1d-4ca1-aaa6-4a585c98d2a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 116,
                "y": 45
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "cfa21b4c-e01b-46b8-88c1-fa9e7af0a7b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 97,
                "y": 45
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "864eb76d-cadc-46dc-a49c-d6df4998e360",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 78,
                "y": 45
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "10d99a09-0adb-45ad-9e12-acc0ab46669c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 211,
                "y": 45
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "3e852676-56ee-4cae-a3d7-26b0e22ff6bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 59,
                "y": 45
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "b845fc80-72a1-4599-b2b2-5dfd7e587ca4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 21,
                "y": 45
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "dace7738-cb77-4bc2-ad9d-ca70a09ceb93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 2,
                "y": 45
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "d0d2b50c-665d-48f2-ad11-fc9d9f914a6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 280,
                "y": 88
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "eebdec87-d5f6-4d22-b65f-e81dbc0687f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 490,
                "y": 2
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "cd1394f6-45e9-484a-8749-fb5387a7c553",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 471,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "4f0daebe-543f-45a4-bde4-acf7361a4c5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 452,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "00558f56-a70d-47f7-ae34-0590bff6d823",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 41,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 433,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "5e2f8515-f94b-4993-a2d4-97cbaf39262a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 414,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "2d4e080e-66da-4cdd-ae3b-954323615d1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 190,
                "y": 88
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "bd1c2142-be2d-41e4-8ef1-7f42d15ecbe8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 262,
                "y": 88
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "f4390c56-ef7c-47b6-8112-ed27299c803d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 41,
                "offset": 8,
                "shift": 21,
                "w": 5,
                "x": 200,
                "y": 131
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e168ac81-9b5e-4bab-9fb9-0c7277ae7a43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 41,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 262,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "02d80ebd-3eae-4564-86a2-a908a641b6b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 41,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 268,
                "y": 45
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 20,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}