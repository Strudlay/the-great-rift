{
    "id": "e548453c-c643-40a6-b9a4-3250d447fb72",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fFont",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Kredit",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "5cc23336-7c6c-4dae-a308-0bf7b9fdb2e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ee8d7575-1a29-4466-b283-55f2b8f61c56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 21,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 226,
                "y": 71
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "c84a8449-e4cf-41cb-9c1c-308ec47b572c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 156,
                "y": 48
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "3f3ead2b-9c9e-4b3c-b7ec-c43bf06be235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 145,
                "y": 48
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "c9c3a76e-ff54-46d1-b63e-5e0b164d7761",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 134,
                "y": 48
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "64c99893-99e9-462a-9cb4-0b221b85a475",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 123,
                "y": 48
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "75b8c49a-7266-4809-8e0c-da2c6149f5df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 112,
                "y": 48
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "df2372af-92b0-40ba-b396-0c14efa139e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 21,
                "offset": 3,
                "shift": 10,
                "w": 5,
                "x": 213,
                "y": 71
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "22cb6809-9170-4826-8f92-91a127fc3dcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 21,
                "offset": 3,
                "shift": 10,
                "w": 5,
                "x": 206,
                "y": 71
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "8ddde871-c70c-4822-b2b5-253b43c1110f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 21,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 174,
                "y": 71
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "a90c6d54-c2a4-4b15-b7f6-256fbf23b9b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 101,
                "y": 48
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "4424279d-72a2-4c5f-8ae7-d6f9565cc287",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 167,
                "y": 48
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "6b25f9b3-e8dd-4b4f-b7fd-572c38197c09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 21,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 166,
                "y": 71
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "9b93cbf7-6e4e-45cd-abd3-446979cd82fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 90,
                "y": 48
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "cd27fdfe-8bff-4821-8af8-c5e0c0270bba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 21,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 220,
                "y": 71
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "c4e90029-0d9d-4331-80c6-bd855f6c85c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 68,
                "y": 48
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "eaaf2746-d771-4158-b482-8553f702c1ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 57,
                "y": 48
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "42bc842f-a704-455a-857f-60905549ee55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 129,
                "y": 71
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "69656cf8-7c02-4dd7-9f60-d399d0a238f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 46,
                "y": 48
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "243d7bf6-b68a-4ae0-9e5b-ebae396df259",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 35,
                "y": 48
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "89676b62-8687-41fb-bbf3-3b8cec9b8a36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 109,
                "y": 71
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "ce412a36-bdad-4066-bff7-86b4d7c3a890",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 24,
                "y": 48
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a2c97922-e615-4310-bae7-33910005fab8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "e1e66875-7a23-40dd-aacb-59ca4b7fef60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 79,
                "y": 48
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "28536cff-02d0-4510-9474-923648d86781",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 79,
                "y": 71
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "35efec94-6db6-42f7-b98d-fb583069b001",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 178,
                "y": 48
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "812dcabe-21c9-4d12-8bfb-e70162b959f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 21,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 232,
                "y": 71
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "458e1f39-ebe4-44cb-b048-dfd61040c3e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 21,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 198,
                "y": 71
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "f35f6d85-b7e9-48d3-9272-29bcab2dcd43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 57,
                "y": 71
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "28ea9af8-7c67-462a-9545-919033a82a72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 119,
                "y": 71
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "2dfc7989-bc5c-4efa-882f-eb0c49204ca6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 68,
                "y": 71
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "edef0795-712e-405d-b542-690825797e60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 35,
                "y": 71
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "1ac60639-517a-45d6-ba54-1a0eeb5cf684",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 200,
                "y": 48
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "7800f92c-fed7-4132-a4dc-6198d31aaadb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 13,
                "y": 71
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "56433eea-1bce-4b77-87a6-4305ff893e0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 71
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "00428e54-9823-48b2-9a09-6c3f2012878a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 244,
                "y": 48
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "79a779da-655e-44a8-ae1a-9563ab6260a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 89,
                "y": 71
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "49f2005e-9abc-4466-866c-d1b832ee89ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 233,
                "y": 48
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "5fc5efa3-a91e-4575-807b-d62682c17c93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 222,
                "y": 48
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "cc33028f-f6cf-4ffc-bc31-ab0199caec2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 211,
                "y": 48
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "caca1dc2-80a9-480e-ae24-bc49e2e73bcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 24,
                "y": 71
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "43fcaf65-0752-437e-b28c-29f0392a6fde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 46,
                "y": 71
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "78327a4e-bca5-4fa4-9d2c-ab9123e65489",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 157,
                "y": 71
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "1fe338e0-92ae-49a5-9c8b-5bed3ff0ac18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 13,
                "y": 48
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "1e42fa45-3a47-4c0b-8efe-2e3ff33cc4e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 233,
                "y": 25
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "babc33d5-0d87-4034-b7f2-c81f33a123ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 222,
                "y": 25
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "06b72341-47e5-4194-8905-459801876db0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "82acd059-3d01-4897-9388-5b498a0c1413",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 244,
                "y": 25
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "a01e6712-0646-4a5d-9b62-0de4efbf2598",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "004b3146-dce3-4f85-a803-ef62d32fa808",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 173,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "2e562c95-2d6f-45f0-8169-cd9db166662f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "6dd2078f-bb5c-45a1-9c57-41ffa143cf20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "5c399f97-898d-47e3-a862-f8a6a0e3c5a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "d229226f-64bb-4568-b718-4aa7d48dfd1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "adde74df-57c1-4692-bf15-a6aba5c5cdaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "178b8e56-01df-4c14-bb67-dcb14bdfbd9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "4aad9c41-e269-47e4-a095-ed337b33c8e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "c362b542-d87e-4615-b15d-b46f113158fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 85,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "786acf63-c73b-4f28-a009-4dabe46695c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "398a6be7-39f8-4d87-973e-698dac1b3bf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 21,
                "offset": 3,
                "shift": 10,
                "w": 7,
                "x": 139,
                "y": 71
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "98391914-9e03-4a80-b88d-261332e59a96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 63,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "e9638695-7842-4f8c-b44a-7966a9c7180d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 182,
                "y": 71
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "ac746134-ffed-4317-914c-6a9e7e613df7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "caa5d582-3cb8-4a6d-93d4-a79e11dc9be2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "24f23bf4-0df3-4601-9f27-27786e8e84da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 190,
                "y": 71
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a66447ec-ef84-4c7b-9931-60c6fdbb1fd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "10091ce6-e100-4c06-a1a8-f99ec4330993",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "6d4b7c5d-f6d5-450b-9e96-2ff8381c4992",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "a749abb3-3d4a-4e5f-ad97-bf7c93c61d96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 99,
                "y": 71
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "803259a2-6aa4-4b8a-bded-4724b08f223f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "847987b3-5c15-4de1-a8e8-689b92d18b08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 239,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "e34af563-9e77-4079-89d9-ab021e95d0e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 25
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "e322e38e-ad93-4c09-b00c-82fdf0bd0f5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 211,
                "y": 25
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "c8b3f6c0-252c-4f6b-b3e8-e0d36696d87a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 200,
                "y": 25
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "94cd8086-333b-4c49-bd2a-01ed22e5bc97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 21,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 148,
                "y": 71
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "1f160639-7320-41d2-a6b4-1552f21062f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 189,
                "y": 25
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "12bab881-ec01-4867-871a-2687443e895f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 178,
                "y": 25
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "8808c0d8-9f9d-4d64-b774-d4ee07ff2fa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 167,
                "y": 25
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "2a709be4-8fb5-4aa2-9ac3-fade543b3b18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 156,
                "y": 25
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "aefd5806-7917-4874-a815-d73f7133ba3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 145,
                "y": 25
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "a4142e97-1763-4727-b6dc-04d1ba2ce025",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 134,
                "y": 25
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "31eb5e5c-6202-4d76-b1b4-bb725c873741",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 123,
                "y": 25
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "6ac90f08-85d1-4501-91ed-d627fea8a0a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 112,
                "y": 25
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "302bb4de-7cf8-40f7-9e4e-a29e9a5586ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 101,
                "y": 25
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "9ef8a05a-6ffb-4c51-8248-5c677701d2bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 90,
                "y": 25
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "17fd4c36-86ca-4127-8167-d4409ab0dd9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 79,
                "y": 25
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "9c3ee123-7ae2-4db0-9137-0124157e331e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 68,
                "y": 25
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "dc9cc367-da05-441f-9e03-1e4ac96eaa3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 57,
                "y": 25
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "83db45db-16ff-441e-86a1-6620c7848df8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 46,
                "y": 25
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "161470da-f120-4eac-acea-62cff8d42bb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 35,
                "y": 25
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "2df4bce0-de47-4e82-af69-87f86396a769",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 24,
                "y": 25
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "e42664f5-16d2-4a27-95df-129181c8604b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 13,
                "y": 25
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "08e41ab0-4442-4302-a77f-bf145a66f852",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 21,
                "offset": 4,
                "shift": 10,
                "w": 3,
                "x": 238,
                "y": 71
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "26b58313-51fe-4bc2-90eb-43081b83e393",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 195,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "9632ce18-5668-415e-b714-eb68fbe341cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 189,
                "y": 48
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 10,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}